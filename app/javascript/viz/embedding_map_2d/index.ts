import Crux from "crux";
import Oviz from "crux";
//import Oviz from "oviz";
import { registerDefaultBioInfoComponents } from "crux/dist/element/global";
import { event } from "crux/dist/utils";
import { register } from "page/visualizers";
import { editorConfig } from "./editor";
import { registerEditorConfig } from "utils/editor";
//import { savedTheme } from "common/mem-theme";

import { process} from "./data";
import template from "./template.bvt";
import {HexagonPI} from "./HexagonPI";

// Crux.use.theme("embedding_map-dark", {
//     extends: "dark",
//     colors: {
//         textFill: "#fff",
//         axisColor: "#fff",
//         test: "red",
//         legendFill: "#41484D"
//     },
// });

// Crux.use.theme("embedding_map-light", {
//     extends: "light",
//     colors: {
//         textFill: "#000",
//         axisColor: "#000",
//         test: "blue",
//         legendFill: "white"
//     },
// });

registerDefaultBioInfoComponents();

var _data;//store uploded data
export function getData() {return _data};

const MODULE_NAME = "embedding_map_2d";

export function init(id ,path1 ,path2,path3,path4 , eid ,plot_name) {
    // if (!window.gon || window.gon.module_name !== MODULE_NAME)  return;
    Oviz.visualize({
        el: id,
        //template,
        renderer: "svg",
        root: new HexagonPI(0),
        //theme: savedTheme(MODULE_NAME),
        data: {},
        loadData: {
            expressionData: {
                fileKey: "expressionData",
                url: path1,
                //url:"/api/public/scatterDemo",
                type: "csv",
                //loaded: process,
            },
            metaData: {
                fileKey: "spotMetaData",
                url: path2,
                type: "csv",
                loaded: process,
            },
            markerData: {
                fileKey: "markerData",
                url: path3,
                type: "csv",
                loaded() {}

            },
            spatialImageData: {
                fileKey: "spatialImageData",
                url: path4,
                type: "image",
                loaded() {}    
            }
        },
        setup() {
            registerEditorConfig(editorConfig(this),"getVue",plot_name);//render webpage
        },
    });
    //return [visualizer, {
    //    theme: {
    //        light: "embedding_map-light",
    //        dark: "embedding_map-dark",
    //    },
    //}];
}

// export function updateEditor(){
//     // registerEditorConfig(editorConfig(this));//render webpage
// }

// register(MODULE_NAME, init);
// event.emit("bvd3-resource-loaded");

// export function registerEmbeddingMap2D(){
//     register(MODULE_NAME, init);
// }