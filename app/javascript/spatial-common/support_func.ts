export function createOnUndefined(path: any, obj: string) {
    if (!path.hasOwnProperty(obj))
        path[obj] = {};
    return path[obj];
}

export function diffStartAt(arr: string[], retType: boolean = false): [number, string] | number {
    const chars = arr[0].split('');
    for (let i = 0; i < chars.length; ++i) {
        if (!arr.some(x => isNaN(parseFloat(x)))) // Number
            // if (!isNaN(Number(arr[0].substring(i)))) // Number
            return retType ? [i, "number"] : i;
        if (arr.some(a => a.charAt(i) != chars[i])) // String
            return retType ? [i, "string"] : i;
    }
    return retType ? [chars.length, "string"] as [number, string] : chars.length as number;
}

export function sortLabels(a, b) {
    const [diffPos, type] = diffStartAt([a, b], true) as [number, string];
    const [a_, b_] = [a, b].map(x => x.substring(diffPos));
    if (type === "string")
        return a_ < b_ ? -1 : a_ === b_ ? 0 : 1;
    else return parseFloat(a_) - parseFloat(b_);
}

export function sortChrs(a, b) {
    const [diffPos, type] = diffStartAt([a, b], true) as [number, string];
    const [a_, b_] = [a, b].map(x => {
        const sub = x.substring(diffPos)
        return isNaN(sub) ? sub.charCodeAt(0) : parseFloat(sub)
    });
    return a_ - b_;
}

export function isArray(obj) {
    if (Array.isArray)
        return Array.isArray(obj);
    else
        return Object.prototype.toString.call(obj) === "[object Array]";
}

export function range(n: number) {
    const ret = [];
    for (let i = 0; i < n; i++)
        ret.push(i);
    return ret;
}

export function copyObject(obj: any) {
    const result = {};
    Object.entries(obj).forEach(([k, v]) => result[k] = v);
    return result;
}

export function arrAvg(arr, precision = 3) {
    const denominator = Math.pow(10, precision);
    return Math.round(arr.reduce((acc, curr) => acc + Number(curr), 0) / arr.length * denominator) / denominator;
}