# Welcome to STAP
Spatial transcriptomics analysis platform (STAP) is a one-stop online platform containing three major components:

- Analyses: online spatial transcriptomics (ST) analysis pipelines,
- Visualizations: comprehensive interactive visualization of ST data,
- Database: curated ST database with publication and sample details.

![logo](tutorial_imgs/Figure1.png)

# Analyses

![logo](tutorial_imgs/analyses_nar.png)

STAP currently provides two R-based ST analysis pipelines, `ST_Analysis` and `ST_SC_Analysis`, which require 10x Space Ranger expression h5 and spatial files as inputs, then analyze and visualize the spatial and single-cell (SC) integrated spatial data with one click. We have embedded state-of-art tools, including `Seurat`, `sctransform`, `Leiden`, `BayesSpace`, `SPOTlight`, *etc*. to to perform normalization, imputation, dimension reduction, clustering, differential expression, marker genes enrichment, spatial deconvolution, single-cell integration, *etc*. 

Users can go to [Analysis Pipelines](https://st.deepomics.org/submit/pipelines) to select the desired pipeline and submit the job.

![logo](tutorial_imgs/pipelines.png)

After submission, users can monitor submitted jobs using [Analysis Job Query](https://st.deepomics.org/submit/job-query).

![logo](tutorial_imgs/job_query.png)

STAP provides the finished demo pipelines for `ST_Analaysis` and `ST_SC_Analysis` at [Demo Pipelines](https://st.deepomics.org/submit/job-demo).

![logo](tutorial_imgs/demo_pipelines.png)

## ST_Analysis

[ST_Analysis](https://st.deepomics.org/submit/pipeline/48) pipeline requires four input files from `10x Space Ranger` and two parameters:

```
Input files:
 --in_h5_fn=The path of 10x ST h5 file.                 e.g. filtered_feature_bc_matrix.h5
 --in_image_fn=The path of spatial image.               e.g. tissue_lowres_image.png
 --in_coord_fn=The path of 10x spatial coordinates.     e.g. tissue_positions_list.csv
 --in_scale_fn=The path of scalefactor json.            e.g. scalefactors_json.json

Parameters:
 --image_res=Can only be "lowres" or "hires".           e.g. lowres
 --prefix=The prefix of output file.                    e.g. seurat_v1_hln

```

The output files are:
- `*_normalized_counts.csv`: the expression counts normalized by `sctransform`.
- `*_metadata.csv`: the spot meta information including scaled spatial coordinates, `PCA` and `UMAP` embedding results, `Seurat Leiden` and `BayesSpace` clustering results, *etc*.
- `*_de_markers.csv`: the differential expressed markers among detected clusters.

## ST_SC_Analysis

[ST_SC_Analysis](https://st.deepomics.org/submit/pipeline/50) pipeline requires four ST input files from `10x Space Ranger`, two SC input files, and two parameters:

```
ST Input files:
 --in_h5_fn=The path of 10x ST h5 file.                 e.g. filtered_feature_bc_matrix.h5
 --in_image_fn=The path of spatial image.               e.g. tissue_lowres_image.png
 --in_coord_fn=The path of 10x spatial coordinates.     e.g. tissue_positions_list.csv
 --in_scale_fn=The path of scalefactor json.            e.g. scalefactors_json.json

SC Input files
 --in_sc_count_fn=The path of single cell count matrix. e.g. allen_cortex_dwn_count.csv   
 --in_sc_meta_fn=The path of single cell meta file.     e.g. allen_cortex_dwn_metadata.csv

Parameters:
 --image_res=Can only be "lowres" or "hires".           e.g. lowres
 --prefix=The prefix of output file.                    e.g. stxBrain_anterior_slice1

```

The output files are:
- `*_normalized_counts.csv`: the expression counts normalized by `sctransform`.
- `*_metadata.csv`: the spot meta information including scaled spatial coordinates, `PCA` and `UMAP` embedding results, `Seurat Leiden` and `BayesSpace` clustering results, *etc*.
- `*_de_markers.csv`: the differential expressed markers among detected clusters.
- `*_deconvolutino.csv`: the spot cell type fractions deconvoluted by `SPOTlight`.
- `*_interaction.csv`: the cell type spatial interaction calculated by `SPOTlight`.
- `*_corr.csv`: the cell type Pearson correlation.
- `*_corr_p.csv`: the cell type Pearson correlation p value.

# Visualizations
![logo](tutorial_imgs/visualizations_nar.png)

STAP supports comprehensive interactive visualizations of result from ST analyse pipelines, including Spot Map 2D, Spot Map 3D, Embedding Map 2D, Spatial Deconvolution - Pie View, Spatial Deonvolution - Bar View, Relation - Marker Heatmap, Relation - Marker Comparison, Relation - Spatial Interaction, Relation - Spatial Correlation. Multiple interaction options are available, including informative tooltips, movable legends, edited labels, optional SCI color, etc. Users can adjust and drag the spatial image zoom slider to inspect the region of interest with customized size and resolution. All visualizations are downloadable in high-quality publication-ready format. 

Feel free to checkout the nine visualizations with demo datasets at [Demo Visualizations](https://st.deepomics.org/visualizer).

## Spot Map 2D

![logo](tutorial_imgs/spot_map_2d.png ':size=400x400')

## Spot Map 3D

![logo](tutorial_imgs/spot_map_3d.png ':size=400x400')

## Spatial Deconvolution - Pie View

![logo](tutorial_imgs/pie_view.png ':size=600x400')

## Spatial Deconvolution - Bar View

![logo](tutorial_imgs/bar_view.png )

## Relation - Marker Heatmap

![logo](tutorial_imgs/marker_heatmap.png )

## Relation - Marker Comparison

![logo](tutorial_imgs/marker_comparison.png )

## Relation - Spatial Interaction

![logo](tutorial_imgs/spatial_interaction.png ':size=450x400')

## Relation - Spatial Correlation

![logo](tutorial_imgs/spatial_correlation.png ':size=400x400')


# ST Database

STAP provides curated ST database with project/publication and sample details. Users can browse, search, select, and download interest data.

## Project Details
The project table stores the available ST project/publication related entries:
 - project_name
 - publication_link
 - author
 - journal
 - year
 - number_of_samples
 - description
 - study_species
 - study_type
 - study_tissue
 - cancer_name
 - cancer_subtype
 - sample_country

## Sample Details
The sample table stores the available ST sample related entries:
 - sample_name
 - project_name
 - description
 - study_species      
 - study_type
 - study_tissue
 - tissue_type
 - protocol
 - process_tool      
 - annotation
 - sample_country
 - sample_sex
 - sample_age
 - cancer_name
 - cancer_subtype
 - cancer_grade
 - cancer_stage
 - cancer_type
 - cancer_os
 - cancer_os_status
 - cancer_pfs
 - cancer_pfs_status
 - number_of_spot
 - data_access


# Browser compatibility

<table style="width:100%">
        <tr>
          <th> </th>
          <th>Firefox</th>
          <th>Chrome</th>
          <th>Safari</th>
          <th>Edge</th>
        </tr>
        <tr>
          <th>Linux</th>
          <th>√</th>
          <th>√</th>
          <th>-</th>
          <th>-</th>
        </tr>
        <tr>
          <th>Macos</th>
          <th>√</th>
          <th>√</th>
          <th>√</th>
          <th>-</th>
        </tr>
        <tr>
          <th>Windows</th>
          <th>√</th>
          <th>√</th>
          <th>-</th>
          <th>√</th>
        </tr>        
</table>
Browser compatibility of STAP, "√" for pass and "-" for not applicable.


