import { Component, ComponentOption } from "crux/dist/element";
import { scaleLinear } from "d3-scale";
import { useTemplate } from "crux/dist/ext/decorator";

export interface BrushOption extends ComponentOption {
    type: string;
    rangeX: [number, number];
    initialRangeX: [number, number];
    currentRangeX: [number, number];
    rangeY: [number, number];
    initialRangeY: [number, number];
    currentRangeY: [number, number];    
    maxDeltaX: number,
    maxDeltaY: number,
    cornerRadius: number;
    onBrushEnd: () => void;
    onBrushUpdate: () => void;
    onBrushStart: () => void;
}

@useTemplate(`
    Component {
        @let x = brushX()
        @let y = brushY()
        @let w = brushWidth()
        @let h = brushHeight()

        Rect {
            x = 0; y = 0; width = 100%; height = 100%
            fill = "rgba(0,0,0,0)"
        }
        Rect {
            ref = "mainBrush"
            x = x
            y = y
            width = w
            height = h
            fill = "rgba(200,200,200,.4)"
            stroke = "rgba(20,20,20,.6)"
            cornerRadius = prop.cornerRadius
            cursor = "move"
            on:mousedown = (ev) => handleDown(ev, 'main')
            @props prop.opt.brush
        }
        @if (prop.type == 'h'){
            @let isFull = state.stateR - state.stateL >= prop.maxDeltaX
            Rect {
                ref = "leftHandle"
                x = x - 3
                width = 6
                height = 100%
                fill = "rgba(0,0,0,0)"
                cursor = isFull ? "ew-resize" : "e-resize"
                on:mousedown = (ev) => handleDown(ev, 'l')
                @props prop.opt.leftHandle
            }       
            Rect {
                ref = "rightHandle"
                x = x + w - 3
                width = 6
                height = 100%
                fill = "rgba(0,0,0,0)"
                cursor = "ew-resize"
                on:mousedown = (ev) => handleDown(ev, 'r')
                @props prop.opt.rightHandle
            }
        }        
        @if (prop.type == 'v'){
            Rect {
                ref = "topHandle"
                y = y - 3
                width = 100%
                height = 6
                fill = "rgba(0,0,0,0)"
                cursor = "ns-resize"
                on:mousedown = (ev) => handleDown(ev, 't')
                @props prop.opt.topHandle
            }
            Rect {
                ref = "bottomHandle"
                y = y + h - 3
                width = 100%
                height = 6
                fill = "rgba(0,0,0,0)"
                cursor = "ns-resize"
                on:mousedown = (ev) => handleDown(ev, 'b')
                @props prop.opt.bottomHandle
            } 
        }       
    }
`)
export class Brush extends Component<BrushOption> {
    public defaultProp() {
        return {
            ...super.defaultProp(),
            cornerRadius: 0,
        };
    }

    public get dataRangeX(): [number, number] {
        return [this._brushScaleX(this.state.brushL), this._brushScaleX(this.state.brushR)];
    }
    public get dataRangeY(): [number, number] {
        return [this._brushScaleY(this.state.brushT), this._brushScaleY(this.state.brushB)];
    }

    private _shouldUpdateRangeNextTime = false;

    private _width = -1;
    private _height = -1;
    private _isMoving = false;
    private _moveType!: string; 
    private _brushScaleX!: d3.ScaleContinuousNumeric<number, number>;
    private _brushScaleY!: d3.ScaleContinuousNumeric<number, number>;
    private _lastX = 0;
    private _lastY = 0;

    private _bodyUpListener: any;
    private _bodyMoveListener: any;

    private _maxDeltaX;
    private _maxDeltaY;

    protected state = {
        brushL: 0,
        brushR: 0,
        brushT: 0,
        brushB: 0,
    };

    public didCreate() {
        this._brushScaleX = scaleLinear().range(this.prop.rangeX);
        this._brushScaleY = scaleLinear().range(this.prop.rangeY);
        this._bodyUpListener = (e: any) => this.handleUp(e);
        this._bodyMoveListener = (e: any) => this.handleMove(e);
    }

    public didLayout() {
        const w = this.$geometry.width;
        const h = this.$geometry.height;

        this._maxDeltaX = this.prop.maxDeltaX || w
        this._maxDeltaY = this.prop.maxDeltaY || h

        if (this._width !== w || this._height !== h) {
            let lValue!: number, rValue!: number;
            let tValue!: number, bValue!: number;
            if (!this._firstRender) {
                lValue = this._brushScaleX(this.state.brushL);
                rValue = this._brushScaleX(this.state.brushR);
                tValue = this._brushScaleY(this.state.brushT);
                bValue = this._brushScaleY(this.state.brushB);            
            }
            this._brushScaleX.domain([0, w]);
            this._brushScaleY.domain([0, h]);
            if (this.prop.initialRangeX) {
                this._setCurrentRangeX(...this.prop.initialRangeX);
            } else if (this._firstRender) {
                this.state.brushL = 0;
                this.state.brushR = w;
            } else {
                this._setCurrentRangeX(lValue, rValue);
            }
            if (this.prop.initialRangeY) {
                this._setCurrentRangeY(...this.prop.initialRangeY);
            } else if (this._firstRender) {
                this.state.brushT = 0;
                this.state.brushB = h;
            } else {
                this._setCurrentRangeY(tValue, bValue);
            }
            this._width = w;
            this._height = h;
        }
        if (this.prop.currentRangeX) {
            this._setCurrentRangeX(...this.prop.currentRangeX);
        }
        if (this.prop.currentRangeY) {
            this._setCurrentRangeY(...this.prop.currentRangeY);
        }        
    }

    public willRender() {
        if (this._shouldUpdateRangeNextTime) {
            this._brushScaleX.range(this.prop.rangeX);
            this.state.brushL = 0;
            this.state.brushR = this._brushScaleX.domain()[1];
            this._brushScaleY.range(this.prop.rangeY);
            this.state.brushT = 0;
            this.state.brushB = this._brushScaleY.domain()[1];    
            this._shouldUpdateRangeNextTime = false;
        }
    }

    private _setCurrentRangeX(l: number, r: number) {
        const i = this._brushScaleX.invert;
        this.state.brushL = i(l);
        this.state.brushR = i(r);
    }

    private _setCurrentRangeY(t: number, b: number) {
        const i = this._brushScaleY.invert;
        this.state.brushT = i(t);
        this.state.brushB = i(b);
    }
    public reset() {
        this._shouldUpdateRangeNextTime = true;
        this._width = -1;
        this._height = -1;
    }

    public setCurrentRangeX(l: number, r: number, draw: boolean = true) {
        this._setCurrentRangeX(l, r);      
        if (draw) this.draw();
    }
    public setCurrentRangeY(t: number, b: number, draw: boolean = true) {
        this._setCurrentRangeY(t, b);      
        if (draw) this.draw();
    }
    public setRangeX(l: number, r: number) {
        this._brushScaleX.range([l, r]);
        this.state.brushL = 0;
        this.state.brushR = this._brushScaleX.domain()[1];
        this.draw();
    }
    public setRangeY(t: number, b: number) {
        this._brushScaleY.range([t, b]);
        this.state.brushT = 0;
        this.state.brushB = this._brushScaleY.domain()[1];
        this.draw();
    }
    // @ts-ignore
    private brushX(): number {
        if( this.prop.type == 'h'){
            return this.state.brushL;
        }else{
            return 0;
        }
        
    }

    // @ts-ignore
    private brushY(): number {
        if( this.prop.type == 'h'){
            return 0;
        }else{
            return this.state.brushT;
        }
    }

    // @ts-ignore
    private brushWidth(): number {
        return this.state.brushR - this.state.brushL;
    }

    // @ts-ignore
    private brushHeight(): number {
        //return this.$geometry.height;
        return this.state.brushB - this.state.brushT;
    }

    // @ts-ignore
    private handleDown(e: MouseEvent, type: string) {
        this._isMoving = true;
        this._moveType = type;
        this._lastX = e.clientX;
        this._lastY = e.clientY;
        e.preventDefault();
        e.stopPropagation();
        document.body.addEventListener("mouseup", this._bodyUpListener);
        document.body.addEventListener("mousemove", this._bodyMoveListener);
        this.$v.transaction(() => {
            this._callListener("onBrushStart");
        });
    }

    private handleUp(e: MouseEvent) {
        this._isMoving = false;
        e.preventDefault();
        e.stopPropagation();
        document.body.removeEventListener("mouseup", this._bodyUpListener);
        document.body.removeEventListener("mousemove", this._bodyUpListener);
        this.$v.transaction(() => {
            this._callListener("onBrushEnd");
        });
    }

    private handleMove(e: MouseEvent) {
        if (!this._isMoving) return;
        e.preventDefault();
        e.stopPropagation();

        const offset_X = e.clientX - this._lastX;
        this._lastX = e.clientX;        
        if (this._moveType == 'l' || this._moveType == 'r'){
            if (offset_X === 0) return;
        }
        const offset_Y = e.clientY - this._lastY;
        this._lastY = e.clientY;        
        if (this._moveType == 't' || this._moveType == 'b'){
            if (offset_Y === 0) return;
        }

        var l = this.state.brushL + offset_X;
        var r = this.state.brushR + offset_X;
        var t = this.state.brushT + offset_Y;
        var b = this.state.brushB + offset_Y;

        this.$v.transaction(() => {
            switch (this._moveType) {
                case 'l':
                    if (l < 0 || l >= this.state.brushR) return;
                    if (this.state.brushR - l >= this._maxDeltaX) return;
                    this.setState({ brushL: l });
                    break;
                case 'r':
                    if (r <= this.state.brushL || r > this.$geometry.width) return;
                    if (r - this.state.brushL >= this._maxDeltaX) return;
                    this.setState({ brushR: r });
                    break;
                case 't':
                    if (t < 0 || t >= this.state.brushB) return;
                    if (this.state.brushB - t >= this._maxDeltaY) return;
                    this.setState({ brushT: t });
                    break;
                case 'b':
                    if (b <= this.state.brushT || b > this.$geometry.height) return;
                    if (b - this.state.brushT >= this._maxDeltaY) return;
                    this.setState({ brushB: b });
                    break;                    
                case 'main':
                    if (this.prop.type == 'h'){
                        if (l < 0 || r > this.$geometry.width) return;
                        this.setState({ brushL: l, brushR: r });
                    }
                    if (this.prop.type == 'v'){
                        if (t < 0 || b > this.$geometry.height) return;
                        this.setState({ brushT: t, brushB: b });
                    }
                    break;
            }
            this._callListener("onBrushUpdate");
        });
    }

    private _callListener(name: "onBrushStart" | "onBrushEnd" | "onBrushUpdate") {
        if (this.prop.type == 'h'){
            if (this.prop[name]) (this.prop[name] as any).call(null, this.dataRangeX);
        }
        if (this.prop.type == 'v'){
            if (this.prop[name]) (this.prop[name] as any).call(null, this.dataRangeY);
        }
    }
}
