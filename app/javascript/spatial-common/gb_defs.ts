import { GeneData as OriginalGeneData } from "crux/dist/utils/bioinfo/gene";
import * as _ from "lodash";

export interface GenomeRegion {
    processed?: boolean;
    chrom: string;
    start: number;
    end: number;
    genes?: Gene[];
    genesPos?: GeneData[];
    genesNeg?: GeneData[];
    rmsks?: RMask[];
    rmskLayers?: RMask[][];
    scale?: d3.ScaleContinuousNumeric<number, number>;
    thumbScale?: d3.ScaleContinuousNumeric<number, number>;
}

export class Gene {
    public name: string;
    public selected: GeneData = null;

    public static whitelist = ["protein_coding", "lincRNA"];

    constructor(public transcripts: GeneData[]) {
        this.name = transcripts[0].gene_name;
        transcripts.forEach(t => {
            if (t.trans_name.startsWith(t.gene_name)) {
                t.trans_name = t.trans_name_orig.substr(t.gene_name.length + 1);
            } else {
                t.trans_name = t.trans_name_orig.substr(t.trans_name_orig.lastIndexOf("-") + 1);
            }
        });
        this.select(null, true);
    }

    public setColor(color: string) {
        this.transcripts.forEach(x => x.color = color);
    }

    public select(transcript: string | null, onlyDefaults = false) {
        if (transcript === "none") {
            this.selected = null;
            return;
        }
        if (transcript) {
            this.selected = this.transcripts.find(x => x.trans_name_orig === transcript);
        } else {
            const validDefaultTrans = this.transcripts.filter(t => Gene.whitelist.includes(t.biotype));
            if (!onlyDefaults || validDefaultTrans.length) {
                this.selected = _.sortBy(validDefaultTrans, "trans_name")[0];
            } else {
                this.selected = null;
            }
        }
    }
}

type GeneData = OriginalGeneData & {
    start_x?: number;
    end_x?: number;
    start_x0?: number;
    end_x0?: number;
};

interface DTRMask {
    end_pos: number;
    repeat_class: string;
    repeat_element: string;
    repeat_family: string;
    start_pos: number;
    strand: "+" | "-";
}

type RMask = DTRMask & {
    color: string;
    start_x: number;
    end_x: number;
    start_x0?: number;
    end_x0?: number;
};


