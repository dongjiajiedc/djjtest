import Oviz from "crux";
import template from "./template.bvt";
import { BarView } from "viz/spatial_deconvolution_bar_view/bar_view";
import { minmax } from "crux/dist/utils/math";
import { schemeRdYlBu } from "d3-scale-chromatic";
import * as d3 from "d3";
import { event } from "crux/dist/utils"
import { processMain, processMeta, processMarker, processComparison } from "spatial-common/spatial_data";


export class MarkerComparison extends BarView {

    public rows;
    public rowsMap;
    public markerFCCutOff = 1;
    public markerAPCutOff = 0.05
    public volcanoNRow = 2;
    public comparisonWidth = 800;
    public comparisonHeight = 400;
    public boxNCol = 3;
    public boxNRow = 3;

    public expressionData; filteredMarkers;

    public category_key = 'seurat_clusters';

    public spatialImageConfig = {
        imageX: 10, imageY: 10, imageWidth: 200,
        windowWidth: 30, windowHeight: 30,
        brushMaxDeltaX: 80, brushMaxDeltaY: 80,
        inspectWidth: 200, inspectPosition: 'bottom',
        spotR: 1,
    }

    public render() {
        return this.t`${template}`;
    }

    public didCreate(){
        event.on("activeSpots", (_, spots) => {
            //console.log('receve spots', spots);
            this.filteredSpots = spots;
            processMeta(this, spots);
            processMain(this, this.expressionData, this.filteredMarkers)
            processComparison(this)
            this.$v.forceRedraw = true;
            this.redraw();
        })       
    }

    public willRender() {
        
        if (this._firstRender) {
            processMeta(this)
            processMarker(this)
            processMain(this, this.expressionData, this.filteredMarkers)
            processComparison(this)
            this.mainWidth = 1200;
            this.mainGradientFills = [];

            this.gridW = this.gridSize[0];
            this.gridH = this.gridSize[1];
            this.histLegendLabels = this.rows;

            const [min, max] = minmax(this.mainHeatmap.flat());
            this.mainRange = [min, max];
            // const gradientColors = ["#041866", 
            //     "#435497", "#98c4d1",
            //     "#be3035", "#f3e567"];
            //const gradientColors = ["#041866", "#d4c1e6", "#ba1600"];
            const gradientColors = schemeRdYlBu[5].reverse();
            const valueDiv = (this.mainRange[1] - this.mainRange[0]) / 4;
            const values = [];
            for (let i = 0; i < 5; i++) {
                values.push(this.mainRange[0] + i * valueDiv);
            }
            const gradient = d3.scaleLinear()
                // .range([this.colors.start, this.colors.end])
                // .domain(this.mainRange);
                .range(gradientColors)
                // .domain([this.mainRange[0], (this.mainRange[1] + this.mainRange[0])/2
                //             , this.mainRange[1]]);
                .domain([0, this.mainRange[1] / 4, this.mainRange[1]*2 / 4,
                    this.mainRange[1]*3 / 4,
                    this.mainRange[1]]);
                // .domain(values);
                /* @debug
            this.debug.scale1 = (x) => d3.scaleLinear().range([0, 200])
                                    .domain([-5, 2])(x);
            this.debug.scale2 = (x) => d3.scaleLinear().range([0, 200])
                        .domain([min, max])(x);
            const debug = this.mainHeatmap.flat().filter(x => x > 0).sort();
            this.debug.data = [];
            for (let i = 0; i < debug.length; i += 6) {
                this.debug.data.push([computeLog(debug[i]), debug[i]]);
            }*/
            const div = (this.mainRange[1] - this.mainRange[0]) / 20;
            for (let i = 0; i <= 20; i ++) {
                this.mainGradientFills.push(gradient(this.mainRange[0] + i * div));
            }
            this.mainColorGetter = (d) => {
                if (d === 0)
                    return this.colors.abd0;
                else {
                    //return gradient(computeLog(d));
                    return gradient(d);
                }
            };
            // this.mainColorGetter = (d) => gradient(computeLog(d + 1));
            this.valueRange = [0, max];
            const mainH = this.rows.length * this.gridH;
            if (mainH < this.mainHeight) {
                this.mainHeight = this.sizeSettings.mainHeight = mainH;
            } else {
                this.gridH = this.mainHeight / this.rows.length;
            }
            const mainW = this.filteredSpots.length * this.gridW;
            if (mainW < this.mainWidth) {
                this.mainWidth = this.sizeSettings.mainWidth = mainW;
            } else {
                this.gridW = this.mainWidth / this.filteredSpots.length;
            }
            this.histLegendPos.x = this.offsetX + this.mainWidth + this.sizeSettings.gapX;
        
            this._sizeUpdated = true;
        }
        if (this._sizeUpdated) {
            this._sizeUpdated = false;
            this.gridW = this.gridSize[0];
            this.gridH = this.gridSize[1];
            this.mainWidth = this.filteredSpots.length * this.gridW;
            this.$v.size.width = this.mainWidth + this.sizeSettings.boxHeight
                + this.offsetX + 2 * this.sizeSettings.gapX + 2 * this.sizeSettings.padding;
            
            this.mainHeight = this.rows.length * this.gridH;
            this.$v.size.height = this.mainHeight + this.metaFeatures.length * this.gridH +
            + this.offsetX + 2 * this.sizeSettings.gapY + 2 * this.sizeSettings.padding;
        
        
            
            this.histLegendPos = {x: this.sizeSettings.offsetX + this.mainWidth
                + this.sizeSettings.padding,
                y: this.sizeSettings.padding };
            this.mainLegendPos = {x: this.sizeSettings.offsetX + this.mainWidth
                + this.sizeSettings.padding,
                y: this.sizeSettings.padding + this.sizeSettings.barHeight +
                this.sizeSettings.padding};
            this.boxLegendPos = {x: this.sizeSettings.offsetX + this.mainWidth
                + this.sizeSettings.boxHeight,
                    y: this.sizeSettings.barHeight + this.sizeSettings.mainHeight 
                    + this.sizeSettings.padding + 80};
        }        
    }
    protected handleFCPos(me, el, deltaPos: [number, number]) {
        // console.log(me)
        // console.log(deltaPos)
        // console.log(el)
        // console.log(el.id)
        // console.log(el.$geometry.x)
        // console.log(el.$parent._cRange, el.parent.$geometry.width)
        var scaledRange = [0, el.$parent.$geometry.width]
        var actualRange = el.$parent._cRange
        var scaledFC = el.$geometry.x + deltaPos[0]
        if (scaledFC >= scaledRange[0] && scaledFC <= scaledRange[1]){
            var actualFC = scaledFC/scaledRange[1]*actualRange[1]
            if ((el.id == 'fc' && actualFC > 0 ||
                 el.id == '-fc' && actualFC < 0
            )){
                //console.log('fc', actualFC)
                this.markerFCCutOff = Math.abs(actualFC);
                //this.redraw();
                this._firstRender = true;
                this.$v.forceRedraw = true;
                console.log('rerender')
                this.willRender();    
            }

        }
    }    
}
