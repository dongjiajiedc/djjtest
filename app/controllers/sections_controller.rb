class SectionsController < ApplicationController

    def index
        @vis = ['id', 'section_name', 'sample_name', 'project_name', 'c_tissue_source',"tissue_name", 'c_tissue_type', "c_storage_type","c_metastasis"]

        @sections = Section.order(:section_name)
        @attrs = Section.column_names
        @invis = []
        @attrs.each_with_index do |attr, index|
            if !@vis.include?(attr)
                @invis.push(index+1)
            end
        end
        gon.push invis: @invis

        respond_to do |format|
            format.html
            format.csv { send_data @sections.to_csv }
            format.json { render json: SectionDatatable.new(view_context) }
        end
    end

    def show
        @section = Section.find(params[:id])
        @table_headers = Slice.attribute_names
        # @long_attrs = ["c_description"]
        # @link_attrs = []
        @short_attrs = [["section_name","sample_name","project_name","c_tissue_source","tissue_name"],[ "c_collection_position","c_metastasis", "c_storage_type", "c_tissue_type"]]
        respond_to do |format|
            format.html
            format.csv { send_data @section.slices.to_csv }
            format.json { render json: SliceDatatable.new(view_context,@section.id) }
        end
        gon.push section_name: @section.section_name

    end

    def export_selected
        if params[:selected_ids] !=nil
            @sections = Section.order(:section_name)
            send_data @sections.selected_to_csv(params[:selected_ids])
        end
    end

    def make_selected_file
        if params[:download_select]
            download_selected_sections
        elsif params[:download_filter]
            download_filtered_sections
        elsif params[:seqence]
            "nothing"
        end
    end

    def download_filtered_sections
        time = Time.now
        time_str = time.strftime("%Y_%m_%d")       
        time_str += ("_" + time.strftime("%k_%M")) 
        time_str = time_str.gsub(' ','')
        if params[:sample_id]
            @sample = Sample.find(params[:sample_id])
            ids = Section.filtered(params[:search_value], @sample)
            send_data Section.selected_to_csv(ids), :filename => "#{time_str}_selected_metadata.csv"
        else
            ids = Section.filtered(params[:search_value])
            send_data Section.selected_to_csv(ids), :filename => "#{time_str}_selected_metadata.csv"
        end
    end

    def download_selected_sections
        time = Time.now
        time_str = time.strftime("%Y_%m_%d")       
        time_str += ("_" + time.strftime("%k_%M")) 
        time_str = time_str.gsub(' ','')
        @sections = Section.order(:section_name)
        send_data @sections.selected_to_csv(params[:selected_ids]), :filename => "#{time_str}_selected_metadata.csv"
    end

    def download_selected_samples
        time = Time.now
        time_str = time.strftime("%Y_%m_%d")       
        time_str += ("_" + time.strftime("%k_%M")) 
        time_str = time_str.gsub(' ','')
        @sections = Section.order(:section_name)
        send_data @sections.selected_to_csv(params[:selected_ids]), :filename => "#{time_str}_selected_metadata.csv"
    end

end
