import { EditorDef, ToolbarDef } from "utils/editor";
import { extractBinInfo, pos2seg, createContinuousColorScheme, createDiscreteColorScheme, lastPage } from "./heatmap-data";
import { dataOptions, processGenomeRegions, updateGenes } from "./gb_data";
import { GenomeRegion } from "./gb_defs";
import { diffStartAt, copyObject } from "./support_func";

import { genDefaultPalette, withDefaultPalette } from "../oviz-common/palette";

export const editorRef: any = {};

/**
 * Genome Browser
 */
export const editorOpt = {
    page: -1 as any,
    showStrand: "all",
    // sampleInterval: "10",
    displayGeneName: false,
    displayAllGenes: false,
    // showStrandInSpot: false,
    mergeGeneTracks: false,
    showRepeatTrack: false,

    // SearchRegion
    defaultSearchRegion: "chr17:5,565,097-9,590,863",
    searchRegion: "chr17:5,565,097-9,590,863" as any, // TODO: Update by default
};

function updatePage(v) {
    // if (v.data.page === editorOpt.page) return;
    v.root.navigatePage(editorOpt.page);
}

function getPagesOption(v: any) {
    // const ret = [{
    //     value: -1,
    //     text: "Zoom Slider",
    // }].concat(v.data.genome_regions.map((hp, i) => ({
    //     value: i,
    //     text: `(${i + 1}) ${hp.chrom}:${hp.start}-${hp.end}`,
    // })));
    // ret[ret.length - 1].text = "[Searched Region] " + ret[ret.length - 1].text;
    // return ret;

    const ret = v.data.genome_regions.map((hp, i) => ({
        value: i,
        text: `(${i + 1}) ${hp.chrom}:${hp.start}-${hp.end}`,
    }));
    ret.pop();
    return ret;
}

function getModesOption(v: any) {
    return [
        {
            value: -1,
            text: "Zoom Slider",
        },
        ...warpIf(v.data.optFiles.bed, [{
            value: 0,
            text: "Bed Region",
        }]),
        {
            value: lastPage(v.data),
            text: "Search Bed",
        },
    ]
}

export function parseGenomeRegion(s: string) {
    return extractBinInfo(trimRegion(s));
    // const reg = /\s*[chr0-9]+\s*:\s*[0-9,]+\s*-\s*[0-9,]+\s*/g;
    // if (typeof s != "string" || !reg.test(s)) return "";
    // return extractBinInfo(s.replace(/[,\s]/g, ''));
}

function trimRegion(s: string) {
    const region = s.replace(/[,\s]/g, '');
    const reg = /[chr0-9]+:[0-9]+-[0-9]+/g;
    return reg.test(region) ? region : "";
}

function updateSearchedRegion(v: any, newRegion: string) {
    const region = trimRegion(newRegion);
    if (trimRegion(editorOpt.searchRegion) === region) {
        if (editorOpt.page === lastPage(v.data))
            alert("Searched region is no changed. / Error double reRun here.");
        // else
        // v.root.navigatePage(lastPage(v.data));
    } else if (region === "") {
        alert("Invalid region.");
    } else {
        const [chrom, start, end] = extractBinInfo(region);
        const segRange = [start, end].map(x => pos2seg(v.data, chrom, x));
        if (start >= end || segRange.includes("Invalid")) {
            alert("Invalid region.");
        } else {
            editorOpt.searchRegion = newRegion;
            editorOpt.page = lastPage(v.data);
            reRun();
        }
    }
    return newRegion;
}

/**
 * Heatmap
 */
const conf: any = {
    sampleSortBy: "default",
};

export function getReorderOptions(v: any) {
    const share = v.data.share;

    // Re-order
    return [{ value: "default", text: "Default" }].concat([
        ["id", "Sample ID"]
    ].concat(share.keyOrders.meta.map(key => [key, key[0].toUpperCase() + key.substring(1)])
    ).flatMap(([k, name]) => [
        { value: `a${k}`, text: `${name} ↑` },
        { value: `d${k}`, text: `${name} ↓` },
    ]));
}

export function updateSampleSorting(v, val) {
    const data = v.data;
    const share = data.share;

    const sampleSortBy: string = val;
    if (sampleSortBy === "default") {
        share.keyOrders.cellAll = share.keyOrders.cellOrigin;
    }
    else {
        const sOrder = sampleSortBy[0];
        const sKey = sampleSortBy.substring(1);

        const values = v.data.meta.map(d => d[sKey]);
        const [startAt, type] = diffStartAt(values, true) as [number, string];
        const getter = type === "number" ? (d => parseFloat(d[sKey].substring(startAt))) : (d => d[sKey].substring(startAt));
        share.keyOrders.cellAll = sort(data.meta, getter, sOrder === "a", share.keyOrders.cellOrigin);
    }
    updateFilter(v);
}

export function updateFilter(v) {
    const share = v.data.share;
    const metaByID = share.metaByID;
    const labelSelected = share.labels.filter(l => !l._display);
    share.keyOrders.cell = share.keyOrders.cellAll.filter(c =>
        labelSelected.every(label =>
            label.label != metaByID[c][label.category]));
}

function sort(data: any[], getter: any, asc: boolean, cellOrigin: string[]) {
    return data.sort((a, b) => {
        const [a_, b_] = [a, b].map(x => getter(x));
        const [va, vb] = asc ? [a_, b_] : [b_, a_];
        if (va < vb) return -1;
        else if (va > vb) return 1;
        else return cellOrigin.indexOf(a.id) - cellOrigin.indexOf(b.id);
    }).map(d => d.id);
}

export function updateColorScheme(v: any, meta: string) {
    const data = v.data;
    const colorDict = data.share.colors[meta];
    const hasUndefinedColors = Object.values(colorDict).some(x => typeof x === "undefined");
    if (meta === "cont") {
        hasUndefinedColors ? createContinuousColorScheme(data) : createContinuousColorScheme(data, colorDict);
    } else {
        hasUndefinedColors ? createDiscreteColorScheme(data, meta) : createDiscreteColorScheme(data, meta, colorDict);
    }
}

/**
 * Editor Factory
 */

export function editorItemFactory(v: any, title: string, type: "input" | "checkbox" | "select" | "color-picker", path: object, variable: string,
    currFunc?: (curr) => {}, retFunc?: ((v, val) => void) | ((v, val) => {}), shouldNotRun?: boolean, ref?: string) {
    if (type === "input" || type === "checkbox")
        return {
            title: title,
            type: type as "input" | "checkbox",
            // ref: (variable + title).substring(0, 15), // update selected item elsewhere
            ref: typeof ref === "undefined" ? variable : ref, // update selected item elsewhere
            value: {
                current: typeof retFunc === "undefined" ? path[variable] : currFunc(path[variable]),
                callback(val) {
                    if (typeof retFunc === "undefined")
                        path[variable] = typeof path[variable] === "number" ? parseFloat(val) : val;
                    else
                        path[variable] = retFunc(v, val); // exec retFunc and reset variable as return value
                    console.log('path', path, 'var', variable, 'path[va]', path[variable]);
                    if (typeof shouldNotRun === "undefined" || !shouldNotRun) // TODO: testing feature
                        v.run(); // TODO: Optional run
                    // else
                    //     reRun();
                },
            },
        };
    if (type === "select")
        return {
            title: title,
            type: type as "select",
            // ref: title.toLowerCase().split(" ").join("_"), // update selected item elsewhere
            ref: typeof ref === "undefined" ? variable : ref, // update selected item elsewhere
            get options() { return currFunc(v) as Array<{ value: string, text: string }> },
            get value() {
                return {
                    current: path[variable],
                    callback(val) {
                        path[variable] = val;
                        if (typeof retFunc !== "undefined") {
                            retFunc(v, val);
                        }
                        v.run();
                    },
                };
            },
        };
    if (type === "color-picker") {
        // Color Palette
        const [defaultPalette, paletteMap] = genDefaultPalette(path[variable])

        return {
            type: "vue" as const,
            title: "",
            component: "color-picker",
            data: {
                title: title,
                scheme: copyObject(path[variable]),
                palettes: withDefaultPalette(defaultPalette),
                paletteMap,
                id: "pwcolor",
                callback(colors) {
                    path[variable] = colors;
                    if (typeof retFunc !== "undefined") {
                        retFunc(v, variable);
                    }
                    v.run();
                },
            },
        };
    }
}

export function editorSectionFactory(v: any, title: string, type: "single-page" | "tabs", items: any) {
    if (type === "single-page")
        return {
            id: title.toLowerCase(),
            title: title,
            layout: type as "single-page",
            view: {
                type: "list" as const,
                items: items,
            },
        }
    if (type === "tabs") {
        const tabs = [];
        for (let i = 0; i < items.length; ++i) {
            const item = items[i];
            tabs.push({
                id: title.toLowerCase() + item.name.toLowerCase(),
                name: item.name,
                view: item.view,
            });
        }

        return {
            id: title.toLowerCase(),
            title: title,
            layout: type as "tabs",
            tabs: tabs,
        }
    }
}

export function editorTabFactory(v: any, name: string, type: "list", items: any) {
    if (type === "list")
        return {
            name: name,
            view: {
                type: type as "list",
                items: items,
            }
        }
}

export function warpIf(flag, obj) {
    return flag ? obj : [];
}

export function editorConfig(v: any): EditorDef {
    const share = v.data.share;
    const optFiles = v.data.optFiles;

    const genomeBrowserDef = warpIf(editorOpt.page != -1, [
        {
            title: "Genome Browser",
            id: "genome_browser",
            layout: "tabs" as const,
            tabs: [
                {
                    id: "genes",
                    name: "Genes",
                    view: {
                        type: "table" as const,
                        columns: ["Name", "Transcript"],
                        ref: "geneTable",
                        numberOfRows() {
                            const hp = v.data.genome_region;
                            if (!hp) { return 0; }
                            return hp.genes.length;
                        },
                        dataFor(row: number, column: string) {
                            const hp = v.data.genome_region;
                            if (!hp) { return; }
                            const data = hp.genes[row];
                            switch (column) {
                                case "Name": return data.name;
                                case "Transcript": return {
                                    type: "select" as const,
                                    value: data.selected ? data.selected.trans_name_orig : "none",
                                    opts: [{ value: "none", text: "None" }]
                                        .concat(data.transcripts.map((x: { trans_name_orig: any; }) => ({
                                            value: x.trans_name_orig, text: x.trans_name_orig
                                        }))),
                                    onChange(event: { target: HTMLSelectElement; }) {
                                        const s = event.target as HTMLSelectElement;
                                        data.select(s.options[s.selectedIndex].value);
                                        updateGenes(hp);
                                        v.run();
                                    },
                                };
                            }
                        },
                    },
                },
                {
                    id: "annotation",
                    name: "Annotation Panel",
                    view: {
                        type: "list" as const,
                        items: [
                            // {
                            //     type: "input" as const,
                            //     layout: "normal" as const,
                            //     title: "Sample interval (kb)",
                            //     bind: {
                            //         object: editorOpt, path: "sampleInterval", callback() {
                            //             try {
                            //                 const intValue = parseInt(editorOpt.sampleInterval);
                            //                 dataOptions.sampleInterval = v.data.sampleInterval = intValue;
                            //             } catch {
                            //                 window.alert("Please enter a integer value.");
                            //             }
                            //             v.root.resetBrush();
                            //             reRun();
                            // v.run();
                            //         },
                            //     },
                            // },
                            {
                                type: "select" as const,
                                layout: "compact" as const,
                                title: "Display strand",
                                options: [{ value: "all", text: "Both" }, { value: "pos", text: "+" }, { value: "neg", text: "-" }],
                                bind: {
                                    object: editorOpt, path: "showStrand", callback() {
                                        v.data.layout.showStrand = editorOpt.showStrand;
                                        v.run();
                                    },
                                },
                            },
                            {
                                type: "checkbox" as const,
                                title: "Show repeat track",
                                bind: {
                                    object: editorOpt, path: "showRepeatTrack", callback() {
                                        v.data.layout.showAnnotation = editorOpt.showRepeatTrack ? "RepeatMasker" : "none";
                                        console.trace();
                                        reRun();
                                    },
                                },
                            },
                            {
                                type: "checkbox" as const,
                                title: "Merge gene tracks",
                                bind: {
                                    object: editorOpt, path: "mergeGeneTracks", callback() {
                                        v.data.layout.mergeGeneTracks = editorOpt.mergeGeneTracks;
                                        v.run();
                                    },
                                },
                            },
                            {
                                type: "checkbox" as const,
                                title: "Display gene name",
                                bind: {
                                    object: editorOpt, path: "displayGeneName", callback() {
                                        v.data.layout.displayGeneName = editorOpt.displayGeneName;
                                        v.run();
                                    },
                                },
                            },
                            {
                                type: "checkbox" as const,
                                title: "Display all genes",
                                bind: {
                                    object: editorOpt, path: "displayAllGenes", callback() {
                                        dataOptions.displayAllGenes = editorOpt.displayAllGenes;
                                        const hp = v.data.genome_region;
                                        hp.genes.forEach((g: { select: (arg0: any, arg1: boolean) => any; }) => g.select(null, !dataOptions.displayAllGenes));
                                        v.data.genome_regions.forEach((hp: GenomeRegion) => {
                                            processGenomeRegions(hp);
                                        });
                                        updateGenes(hp);
                                        v.run();
                                    },
                                },
                            },
                            // {
                            //     type: "checkbox" as const,
                            //     title: "Show strands in genome_regions",
                            //     bind: {
                            //         object: editorOpt, path: "showStrandInSpot", callback() {
                            //             v.data.layout.showStrandInSpot = editorOpt.showStrandInSpot;
                            //             v.run();
                            //         },
                            //     },
                            // },
                        ],
                    },
                },
            ],
        },
    ]);

    return {
        sections: [
            editorSectionFactory(v, "General Settings", "single-page", [
                editorItemFactory(v, "Auto load heatmap", "checkbox", v.data.config, "autoload"),
                editorItemFactory(v, "NA cases (seperated by comma ,)", "input", v.data.config, "noneCase", (curr) => curr.join(", "), (v, val) => val.split(',').map(s => s.trim())),
                ...warpIf(!optFiles.tree, [
                    editorItemFactory(v, "Reorder samples by", "select", conf, "sampleSortBy", getReorderOptions, updateSampleSorting),
                ]),
                editorItemFactory(v, "Mode", "select", editorOpt, "page", getModesOption, updatePage, false, "mode"),
            ]),
            ...warpIf(!optFiles.tree && optFiles.meta, [
                editorSectionFactory(v, "Select categorical meta label", "tabs", [
                    {
                        name: "Meta",
                        view: {
                            type: "table" as const,
                            columns: ["Show", "Category", "Label"],
                            numberOfRows() {
                                return share.labels.length;
                            },
                            dataFor(row: number, column: string) {
                                const data = share.labels[row];
                                switch (column) {
                                    case "Category": return data.category;
                                    case "Label": return data.label;
                                    case "Show": return {
                                        type: "checkbox", // 改成vue
                                        checked: data._display,
                                        onChange(event) {
                                            data._display = event.target.checked;
                                            updateFilter(v);
                                            v.run();
                                        },
                                    };
                                }
                            },
                        }
                    },
                ])
            ]),
            editorSectionFactory(v, "Layout Settings", "tabs", [
                editorTabFactory(v, "Basic", "list", [
                    editorItemFactory(v, "Figure margin - left", "input", v.data.layout, "marginX"),
                    editorItemFactory(v, "Figure margin - top", "input", v.data.layout, "marginY"),
                    editorItemFactory(v, "Genome zoom slider - height", "input", v.data.layout, "brushHeight"),
                    editorItemFactory(v, "Margin between CNV heatmap and genome zoom slider", "input", v.data.layout, "brushY"),
                    editorItemFactory(v, "Margin between cutted dendrogram and meta heatmap", "input", v.data.layout, "legendDisplacement"),
                ]),
                editorTabFactory(v, "CNV Heatmap", "list", [
                    editorItemFactory(v, "Unit height in CNV heatmap (integer recommended)", "input", v.data.layout, "unitHeight"),
                    editorItemFactory(v, "Unit width in CNV heatmap (integer recommended)", "input", v.data.layout, "unitWidth"),
                    editorItemFactory(v, "Chromosomes - height", "input", v.data.layout, "chrLabelHeight"),
                    editorItemFactory(v, "Show vertical line between chromosomes", "checkbox", v.data.layout, "showDividingLine"),
                    editorItemFactory(v, "Desired width of CNV heatmap", "input", v.data.layout, "desireWidth", (curr) => curr, (v, val) => {v.root.updateZoomRange(); return val;}),
                    editorItemFactory(v, "Left highlight - width", "input", v.data.layout, "rowHighlightWeight"),
                    editorItemFactory(v, "Top highlight - height", "input", v.data.layout, "colHighlightWeight"),
                ]),
                ...warpIf(optFiles.meta, [
                    editorTabFactory(v, "Meta Heatmap", "list", [
                        editorItemFactory(v, "Unit width of meta heatmap", "input", v.data.layout, "metaColWidth"),
                        editorItemFactory(v, "Meta heatmap legend - width", "input", v.data.layout, "legendWidth"),
                        editorItemFactory(v, "Margin between different meta heatmap legends", "input", v.data.layout, "legendSpacing"),
                    ])
                ]),
                ...warpIf(optFiles.tree, [
                    editorTabFactory(v, "Cutted Dendrogram", "list", [
                        editorItemFactory(v, "Dendrogram - width", "input", v.data.layout, "treeWidth"),
                    ])
                ]),
            ]),
            ...warpIf(editorOpt.page != -1, [
                editorSectionFactory(v, "Search", "single-page", [
                    ...warpIf(editorOpt.page === lastPage(v.data), [
                        editorItemFactory(v, "Search local genome region, e.g. chr17:5,565,097-9,590,863", "input", editorOpt, "searchRegion", (curr) => curr, updateSearchedRegion, true),
                    ]),
                    ...warpIf(editorOpt.page != lastPage(v.data), [
                        editorItemFactory(v, "Bed region", "select", editorOpt, "page", getPagesOption, updatePage, false, "page"),
                    ]),
                ]),
            ]),
            ...genomeBrowserDef,
            ...warpIf(optFiles.meta, [
                editorSectionFactory(v, "Color Palettes", "single-page", [
                    ...share.discreteMeta.map(mk => editorItemFactory(v, `Customize ${mk} colors`, "color-picker", v.data.share.colors, mk, (curr) => curr, updateColorScheme)),
                    editorItemFactory(v, "Customize continuous meta colors", "color-picker", v.data.share.colors, "cont", (curr) => curr, updateColorScheme),
                ]),
            ]),
        ]
    };
}
