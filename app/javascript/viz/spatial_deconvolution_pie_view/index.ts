import Oviz from "crux";
import {controlGroupColors} from "oviz-common/palette";
import { register } from "page/visualizers";
import { registerEditorConfig } from "utils/editor";
import { SpatialImage } from "spatial-common/spatial_image";
import { editorConfig, editorRef } from "./editor";
import { PieView } from "./pie_view";

const MODULE_NAME = "spatial_deconvolution_pie_view";

function init() {
    if ( !window.gon || window.gon.module_name !== MODULE_NAME) return;
    const {visualizer} = Oviz.visualize({
        el: "#canvas",
        renderer: "svg",
        width: 1000,
        height: 750,
        root: new PieView(),
        theme: "light",
        data: {
            hiddenSamples: new Set(),
            gridSize: [4, 12],
            colors: {
                // control: controlGroupColors[0],
                // gout: controlGroupColors[1],
                na: "#999", //"#777",
                abd0: "#999", // "#333",
                // start: "#fff7f3",
                // end: "#0A2299",
                start: "#800000",
                org: "#FF5050",
                end: "#FFFF00",
            },
            drawTree: false,
        },
        loadData: {
            spotDeconvData: {
                fileKey: "spotDeconvData",
                type: "csv",
                loaded(){}
            },
            spotMetaData: {
                fileKey: "spotMetaData",
                type: "csv",
                loaded(){}
            },
            spatialImageData: {
                fileKey: "spatialImageData",
                type: 'image',
                loaded(){}
            },
            // ovTree: {
            //     fileKey: "cellTree",
            //     type: "newick",
            //     optional: true,
            //     loaded(d) {
            //         // d = removeNodeLength(d);
            //         // test();
            //         //this.data.species = getLeafOrder(d);
            //         return d;
            //     },
            // },
        },
        setup() {
            console.log(this["_data"]);
            //registerEditorConfig(editorConfig(this), editorRef);
        },
    });
    return visualizer;
}


register(MODULE_NAME, init);
export function registerSpatialDeconvolutionPieView(){
    register(MODULE_NAME, init);
}