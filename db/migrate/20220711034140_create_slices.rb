class CreateSlices < ActiveRecord::Migration[6.0]
  def change
    create_table :slices do |t|
      t.string :slice_name
      t.string :section_name
      t.string :sample_name
      t.string :project_name
      t.string :c_protocol
      t.integer :n_layer
      t.integer :n_diameter
      t.string :n_diameter_unit
      t.integer :n_number_of_spots
      t.integer :n_number_of_genes
      t.integer :n_tissue_hires_scalef
      t.integer :n_tissue_lowres_scalef
      t.integer :n_fiducial_pxl_diameter_fullres
      t.integer :n_spot_pxl_diameter_fullres
      t.string :p_tissue_fullres_image
      t.string :p_tissue_hires_image
      t.string :p_tissue_lowres_image
      t.string :p_aligned_fiducials
      t.string :p_detected_tissue_image
      t.string :p_spot_details
      t.string :p_rna_details
      t.string :c_rna_process_tool
      t.string :c_rna_preprocessed
      t.string :l_data_access



      t.references :section, null: false, foreign_key: true
      t.references :sample, null: false, foreign_key: true
      t.references :project, null: false, foreign_key: true
      
      t.timestamps
    end
  end
end
