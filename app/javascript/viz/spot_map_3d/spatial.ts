import { Component, ComponentOption } from "crux/dist/element";
import { template } from "crux/dist/template/tag";
import Oviz from "crux";
import { event } from "crux/dist/utils";
import { TupleType } from "typescript";
import { ColorSchemeNumeric } from "../../spatial-common/defs";
import { convertTo3d} from "./data"
import { isBuffer } from "lodash";
import { constructColors } from "../spatial_correlation/data";

interface SpatialOption extends ComponentOption{
	display_3d: boolean;
	varied_size: boolean;
	display_scale: number;
	cluster_display: boolean;
	cluster_scheme:  ColorSchemeNumeric;
    exp_scheme:  ColorSchemeNumeric;
	section_x: TupleType;
	section_y: TupleType;
	grid_width: number;
	ang_A: number;
	ang_B: number;
	layer_height: number;
	spatial_width: number;
	spatial_height: number;
	image_portal: string;
	separate_image:boolean;
	spatial_image: any;
}

export class Spatial extends Component<SpatialOption>{
	protected state = {spots: []}
	protected isMoving = false;
	protected mousePos = [0,0]
	protected xDis = 0;
	protected yDis = 0;

	public didCreate(){
        event.on("activeSpots", (_, spots) => this.setState({ spots }))
    }

	protected setActiveSpot(spot){
        event.emit("activeSpots", [spot])
    }

    protected getGridWidth(normalized_value){
		if (this.prop.varied_size)
			return this.prop.grid_width * Math.abs(normalized_value) * 5
		else
			return this.prop.grid_width
	}

	protected getFillColor(normalized_value, cluster_idx){
		if (this.prop.cluster_display) 
			return this.prop.cluster_scheme.get(cluster_idx)
		else
			return this.prop.exp_scheme.get(normalized_value)
	}

	protected getCoordinate(x, y){
		if (this.prop.display_3d)
			return convertTo3d(this.prop.ang_A, this.prop.ang_B, [x, y, this.prop.layer_height]);
		else
			return [x,y]
	}

	protected getAnatomyUrl(){
		console.log(this.prop.image_portal)
		return this.prop.image_portal;
	}

	protected getSpotXY(d){
		var x, y
		x = d['row_in_lowres'] * (this.prop.display_scale / this.prop.spatial_width) - (this.prop.display_scale - this.prop.spatial_width) / 2 + this.xDis;
		y = d['col_in_lowres'] * (this.prop.display_scale / this.prop.spatial_height) - (this.prop.display_scale - this.prop.spatial_height) / 2 + this.yDis;
		return [x,y]
	}

	protected getImgXY(){
		var x, y;
		if (this.prop.separate_image == false){
			x = - (this.prop.display_scale - this.prop.spatial_width) / 2 + this.xDis
			y = - (this.prop.display_scale - this.prop.spatial_height) / 2 + this.yDis
		}
		else{
			x = 0
			y = 700
		}
		return [x,y]
	}

	private mouseDown(){
		var ev = Oviz.event.event as MouseEvent;
		this.mousePos = [ev['layerX'], ev['layerY']]
		this.isMoving = true
		console.log("mouse is down")
	}

	private mouseUp(){
		var ev = Oviz.event.event as MouseEvent;
		if (this.isMoving){
			var xDis = this.xDis + ev['layerX'] - this.mousePos[0]
			var yDis = this.yDis + ev['layerY'] - this.mousePos[1]

			// distance for x
			if (xDis > (this.prop.display_scale - this.prop.spatial_width) / 2){
				this.xDis = (this.prop.display_scale - this.prop.spatial_width) / 2
				console.log("x border is reached")
			}
			else if (xDis < (this.prop.spatial_width - this.prop.display_scale) / 2){
				this.xDis = (this.prop.spatial_width - this.prop.display_scale) / 2
				console.log("x border is reached")
			}
			else{
				this.xDis = xDis
				console.log(ev['layerX'] - this.mousePos[0], xDis)
			}

			// distance for y
			if (yDis > (this.prop.display_scale - this.prop.spatial_height) / 2){
				this.yDis = (this.prop.display_scale - this.prop.spatial_height) / 2
				console.log("y border is reached")
			}
			else if (yDis < (this.prop.spatial_height - this.prop.display_scale) / 2){
				this.yDis = (this.prop.spatial_height - this.prop.display_scale) / 2
				console.log("y border is reached")
			}
			else{
				this.yDis = yDis
				console.log(ev['layerY'] - this.mousePos[1], yDis)
			}
		}
		this.isMoving = false
		console.log("mouse is up")
	}


	private getValue(x: any){
		if (x == 0) {return 0;}
		return Math.log(x)
	}

	render(){
		return this.t`
		Component {
			on:mousedown = mouseDown()
			//on:mousemove = mouseMove()
			on:mouseup = mouseUp()

			@if prop.display_3d == false {
				Component{
					@if prop.separate_image == false{
						Image{
							@expr coor = getImgXY()
							x = coor[0]
							y = coor[1]
							width = prop.display_scale
							height = prop.display_scale
							url = prop.spatial_image
						}
					}

					Rect{
						// track mouse movement in this rect
						width = prop.spatial_width
						height = prop.spatial_height
						fill = "red"
						fillOpacity = "0"
						stroke = "#d1d1e0"
						strokeWidth = 5
					}
				}
			}

			Component{
				@for (d, i) in prop.displayed_data{
					@let normalized_value = d.value/prop.max_value

					@let log_value = getValue(d.value)
					@let fill_color = getFillColor(log_value, d[prop.cluster_key].toString())
					@let string_value = d.value.toString()
					@let shape_width = getGridWidth(normalized_value) - 3
					@let tuple = getSpotXY(d) // calculate new position acording to 
					@let x = tuple[0]
					@let y = tuple[1]
	
					@if (x < prop.spatial_width) && (x >= 0) && (y < prop.spatial_height) && (y >= 0){
						Component{
							@let coor = getCoordinate(x, y) //convert to 3d if necessary
							x = coor[0]
							y = coor[1]
							@let isActive = state.spots.includes(d['idx'])
							@if prop.shape == "square" {
								Rect.centered {
									height = shape_width
									width = shape_width
									stroke = isActive? "yellow" : "grey";
									fill = fill_color
									on:mouseenter = setActiveSpot(d['idx'])
									on:mouseleave = setActiveSpot(null)
									behavior:tooltip {
										content = "Barcode: " + d.barcode + "<br>" +\
												"Expression Value: " + string_value
									}
								}
							}
							@if prop.shape == "circle" {
								Circle.centered {
									r = 0.5 * shape_width
									stroke = isActive? "yellow" : "grey";
									fill = fill_color
									on:mouseenter = setActiveSpot(d['idx'])
									on:mouseleave = setActiveSpot(null)
									behavior:tooltip {
										content = "Barcode: " + d.barcode + "<br>" +\
												"Expression Value: " + string_value\
									}
								}
							}
							@if prop.shape == "triangle" {
								Triangle.centered {
									height = shape_width
									width = shape_width
									stroke = isActive? "yellow" : "grey";
									fill = fill_color
									on:mouseenter = setActiveSpot(d['idx'])
									on:mouseleave = setActiveSpot(null)
									behavior:tooltip {
										content = "Barcode: " + d.barcode + "<br>" +\
												"Expression Value: " + string_value
									}
								}
							}
						}
					}
				}
			}

			@if prop.display_3d == false {	
				Component{	// Frame for blocking excessive image
					Rect {	// left
						width = 1800
						height = 1800
						anchor = @anchor("right", "top")
						fill = "white"
						strokeWidth = 0
					}
				
					Rect {	// top
						x = prop.spatial_height
						width = 1800
						height = 1800
						anchor = @anchor("right", "bottom")
						fill = "white"
						strokeWidth = 0
					}
					
					Rect {	// right
						x = prop.spatial_height
						y = prop.spatial_width
						width = 1800
						height = 1800
						anchor = @anchor("left", "bottom")
						fill = "white"
						strokeWidth = 0
					}
				
					Rect {	// down
						y = prop.spatial_width
						width = 1800
						height = 1800
						anchor = @anchor("left", "top")
						fill = "white"
						strokeWidth = 0
					}
				}
			}

			@if prop.display_3d == false && prop.separate_image == true {
				Image{
					@expr coor = getImgXY()
					x = coor[0]
					y = coor[1]
					width = prop.spatial_width
					height = prop.spatial_height
					url = prop.spatial_iamge // getAnatomyUrl()
				}
			}
		}
		`
	}
}