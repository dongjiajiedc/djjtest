class Slice < ApplicationRecord
    belongs_to :section
    
    def self.import(file)
        CSV.foreach(file.path, headers: true, encoding: 'bom|utf-8') do |row|
            slice = Slice.find_by(slice_name: row['slice_name'],section_name: row['section_name'], sample_name: row['sample_name'],project_name: row['project_name']) || new
            allowed = "^A-Za-z0-9:,-_/.+ µ"
            a=row.to_hash.slice(*column_names)
            temp=0
            a.each do |a1,a2|
                temp += a2.to_s.count(allowed)


            end
            
            if(temp ==0)
                slice.attributes = a
                section = Section.find_by(section_name: slice.section_name);
                sample = section.sample;
                project = sample.project;
                slice.project_id = project.id
                slice.sample_id = sample.id
                slice.section_id = section.id

                slice.save!
            end
        end

    end

    def self.to_csv(options={})
        CSV.generate(options) do |csv|
        csv << column_names
        all.each do |slice|
            csv << slice.attributes.values_at(*column_names)
        end
        end
    end

    def self.selected_to_csv(slice_ids, options={})
        CSV.generate(options) do |csv|
            csv << column_names
            slice_ids.each do |id|
                slice = Slice.find(id)
                csv << slice.attributes.values_at(*column_names)
            end
        end
    end

    def self.filtered(keyword, section=nil)
        search_string = []
        Slice.column_names.each do |attr|
          if Slice.columns_hash[attr].type != :string
            search_string << "cast(\"#{attr}\" as varchar(10)) like :search"
          else
            search_string << "\"#{attr}\" like :search"
          end
        end
    
        if(section!= nil)
            slices = section.slices.order(:slice_name)
            slices = slices.where(search_string.join(' or '), search: "%#{keyword}%")
          
        else
            slices = slices.order(:slice_name)
            slices = slices.where(search_string.join(' or '), search: "%#{keyword}%")
          
        end
        ids = slices.ids
    
        return ids
      end

end
