import Crux from "crux";
import template from "./template.bvt";
import {Spatial} from "./spatial";
import {Axis} from "./axis";
import {Outline} from "./outline"
import {Umap} from "./umap"

export class SpatialPI extends Crux.Component {
    public static components = {Outline, Spatial, Axis, Umap};
    public render = Crux.t`${template}`;
}