class Tissue < ApplicationRecord
    validates :tissue_name, presence: true, uniqueness: { 
        message: ->(object, data) do
          "tissue #{data[:value]} already exists. "
        end
      }, on: :create
    
      def self.import(file)
          CSV.foreach(file.path, headers: true, encoding: 'bom|utf-8') do |row|
          tissue = Tissue.find_by(tissue_name: row['tissue_name']) || new
          tissue.attributes = row.to_hash.slice(*column_names)
  
          tissue.save!
          end
      end
end
