import { Component, ComponentOption } from "crux/dist/element";
import { template } from "crux/dist/template/tag";
import { convertTo3d} from "./data";

interface OutlineOption extends ComponentOption{
}

export class Outline extends Component<OutlineOption>{
   
    private v(ang_A, ang_B, p) {
      return convertTo3d(ang_A, ang_B, p)
    }

    public render = template` 
        Component{
            //define variables
            @let w = prop.w
            @let h = prop.h
            @let l = prop.l
            @let lines = [ 
                {p1:[0, 0, 0], p2:[w, 0, 0]},
                {p1:[0, 0, 0], p2:[0, h, 0]},
                {p1:[0, 0, l], p2:[w, 0, l]},
                {p1:[0, 0, l], p2:[0, h, l]},
                {p1:[w, h, 0], p2:[w, 0, 0]},
                {p1:[w, h, 0], p2:[0, h, 0]},
                {p1:[w, h, l], p2:[w, 0, l]},
                {p1:[w, h, l], p2:[0, h, l]},

                //Remove Horizontal Lines
                //{p1:[0, 0, 0], p2:[0, 0, l]},
                //{p1:[w, 0, 0], p2:[w, 0, l]},
                //{p1:[0, h, 0], p2:[0, h, l]},
                //{p1:[w, h, 0], p2:[w, h, l]},
            ]

            @for line in lines{
                Line {
                    @let p1 = v(prop.ang_A, prop.ang_B, line.p1)
                    @let p2 = v(prop.ang_A, prop.ang_B, line.p2)
                    x1 = p1[0]; y1 = p1[1]; x2 = p2[0]; y2 = p2[1]
                    stroke = "grey"
                    width = 20
                }
            }

            //axis lines for debug
            // @let axis_lines = [
            //     {p1:[0.5*w, 0.5*h, 0], p2:[0.5*w, 0.5*h, l]},
            //     {p1:[0.5*w, 0, 0.5*l], p2:[0.5*w, h, 0.5*l]},
            //     {p1:[0, 0.5*h, 0.5*l], p2:[w, 0.5*h, 0.5*l]},
            // ]

            // @for line in axis_lines{
            //     Line {
            //         @let p1 = v(prop.ang_A, prop.ang_B, line.p1)
            //         @let p2 = v(prop.ang_A, prop.ang_B, line.p2)
            //         x1 = p1[0]; y1 = p1[1]; x2 = p2[0]; y2 = p2[1]
            //         stroke = "grey"
            //         width = 20
            //     }
            // }

        }
    ` 
}