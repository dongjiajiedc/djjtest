class Section < ApplicationRecord
    belongs_to :sample
    has_many :slices, dependent: :destroy


    def self.import(file)
        CSV.foreach(file.path, headers: true, encoding: 'bom|utf-8') do |row|
        section = Section.find_by(section_name: row['section_name'], sample_name: row['sample_name']) || new
        section.attributes = row.to_hash.slice(*column_names)

        Rails.logger.info(section.sample_name)
        sample = Sample.find_by(sample_name: section.sample_name);
        project = sample.project;

        section.project_id = project.id
        section.sample_id = sample.id

        section.save!
        end
    end


    def self.to_csv(options={})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |section|
        csv << section.attributes.values_at(*column_names)
      end
    end
  end

  def self.selected_to_csv(section_ids, options={})
  CSV.generate(options) do |csv|
    csv << column_names
    section_ids.each do |id|
      section = Section.find(id)
      csv << section.attributes.values_at(*column_names)
    end
  end
end

  def self.filtered(keyword, sample=nil)
    search_string = []
    Section.column_names.each do |attr|
      if Section.columns_hash[attr].type != :string
        search_string << "cast(\"#{attr}\" as varchar(10)) like :search"
      else
        search_string << "\"#{attr}\" like :search"
      end
    end

    if(sample!= nil)
      sections = sample.sections.order(:section_name)
      sections = sections.where(search_string.join(' or '), search: "%#{keyword}%")
      
    else
      sections = Section.order(:section_name)
      sections = sections.where(search_string.join(' or '), search: "%#{keyword}%")
      
    end
    ids = sections.ids


    return ids
  end
end
