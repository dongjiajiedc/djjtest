import { EditorDef } from "utils/editor";
import { processData } from "./index";
//import Vue from "vue";
import Vue from 'vue/dist/vue.esm.js'
import { editorSectionFactory, editorItemFactory, warpIf, updateColorScheme } from "../../spatial-common/editor";

//options for selection
const conf: any = {
    types:[]
};

export function setTypes(t:Object){  conf.types = t};

//re-render
function update(v) {
    v.forceRedraw = true;   
    v.run();
}


export function editorConfig(v: any): EditorDef{ //v: this webpage
    conf.v = v;
    return {
        sections: [
            {
                id: "general",
                title: "General Settings",
                layout: "single-page",
                view: {
                    type: "list",
                    items: [
                        {
                            title: "Search for gene",
                            type: "vue",
                            component: searchBox,
                            // bind: {
                            //     object: conf, path: "dataKey",
                            //     callback: () => selectMethod(v), 
                            // },
                        },
                    ],
                },
            },
        ],
    };
}

function setGene(v, gene: string){
    var idx = 0;
    v.data.colorKeys.forEach((g,i) => {
        if(g.includes(gene)) {
            idx=i;
            return;
        }
    });
    v.data.config.colorIdx = idx;
    // console.log("color idx",  v.data.config.colorIdx);
    processData(v.data);
    update(conf.v);
}

var searchBox = Vue.component('gene-search', {
    data: function() {        
        return { 
            genes: conf.v.data.colorKeys,
            numbers: [ 1, 2, 3, 4, 5 ],
            input: "",
            selected: [],
        }
    },
    computed: {
        searchGene: function(){
            let str = this.input
            let strLow = str.toLowerCase(), strUp = str.toUpperCase();
            //if(str=="") return [];
            return this.genes.filter(g=>g.includes(str)||g.includes(strLow)||g.includes(strUp));
        }
    },
    methods: {
        submitGene: function(){
            setGene(conf.v,this.input);
        },
        selectGene: function(){
            this.input = this.selected[0];
            this.submitGene();
        }
     },
    template: `
    <div>
        <p title="Gene_Search">Search for gene</p>
        <form v-on:submit.prevent="submitGene">
            <label for="new-todo"></label>
            <input
                v-model="input"
                id="new-todo"
                placeholder="E.g. ABCA3"
            >
            <button>Apply</button>
            <select v-model="selected" multiple style="width: 150px;" v-on:click.prevent="selectGene">
                <option v-for="n in searchGene">{{ n }}</option>
            </select>
        </form>
    </div>`
  })