![logo](tutorial_imgs/gutmeta_2.png ':size=344x261')


# Welcome to STAP
STAP is a one-stop web server of spatial transcriptomics (ST) data analysis and visualization with build-in curated ST database. 

We will introduce STAP from following aspects:

- Main features of STAP
- Database usage 
  - Browse pre-processed phenotype-related taxonomy
  - Browse projects and samples
  - Construct personal dataset from Human Gut Microbiome Database (HGMD)
 
- Submit usage
  - Submit job using build-in [Analyses](https://gutmeta.deepomics.org/submit/analyses) / [Pipeline](https://gutmeta.deepomics.org/submit/pipelines) modules
  - Monitor your jobs using [Job Query](https://gutmeta.deepomics.org/submit/job-query)
  - Check your results
- Visualization usage
  - Use manual of visualizations
  - Prepare input for pathway visualization

- Material and methods
  - Curated process in HGMD construction
  - Analysis modules scripts access

- FAQ
- Contact us

# Main features of GutMeta
### Over 20,000 curated processed samples using the lastest profiling tool
HGMD provides freely accessed processed samples with:
- taxonomy abundance, and
- curated metadata


Curated metadata had re-annoted phenotype with Medical Subject Headings, [MeSH](https://www.ncbi.nlm.nih.gov/mesh) and merged available sample information from [curatedMetagenomicData](https://waldronlab.io/curatedMetagenomicData/) and [GMrepo](https://gmrepo.humangut.info/home).
- sequncing model, 
- sex,
- age,
- age categories, 
- BMI, 
- countries and region (curated with ISO 3166-1 alpha-3),
- phenotype (curated with MeSH),
- antibiotics use, 
- curatedMetagenomicData, and
- GMrepo.

### Online analysis your data with HGMD
GutMeta provides re-usable analysis modules and pipeline: 
- Alpha diversity
- Beta diversity
- PCA
- Enterotype
- Differential testing
- Build random forest classifier
- Construct taxonomy tree
- Construct overview

Pipelines contains:
- Basic information
- Classifier 

Most analysis modules only needs two files as input, the taxonomy profiling and group information, see detailed format at [Prepare your input](#prepare-your-input).


![modules](tutorial_imgs/gutmeta_feature1.png ':size=80%')

### Interactive visualization and online adjustment
GutMeta provides multiple interative visualizations:
- tooltips
<br>

![logo](tutorial_imgs/gutmeta_interactive1.gif ':size=400x400')
<hr>

- move legend
<br>

![logo](tutorial_imgs/gutmeta_interactive2.gif ':size=400x400')
<hr>

- drag and move compounds
<br>

![logo](tutorial_imgs/gutmeta_interactive3.gif ':size=400x400')

![logo](tutorial_imgs/gutmeta_interactive4.gif ':size=400x400')
<hr>

- interactive revise title
<br>

![logo](tutorial_imgs/gutmeta_interactive5.gif ':size=400x400')
<hr>

and multiple online adjustments options, contains but not all listed:
- change to the other taxonomy level (if appliable)
- change axis range
- change threshold of parameter (for example, p value)
- reorder / sort / remove samples
- customize colors


# Database usage 
## Browse pre-processed phenotype-related taxonomy
## Browse projects and samples
## Construct personal dataset from Human Gut Microbiome Database (HGMD)
### 1. filter samples with keywords


# GutMeta Analysis pipeline tutorial
Here, we present how could the users use GutMeta analysis and visualization modules with their own data.
## Prepare your input

### 1. Profiling table (TSV file)
We recommanded users to process their raw sequencing data with [biobakery workflows](https://github.com/biobakery/biobakery_workflows).
```
[metaphlan_dir]/utils/merge_metaphlan_tables.py metaphlan_output1.txt metaphlan_output2.txt > merge.mpa.tsv
```
* REMINDER: make sure the sample names in the profiling table and the group information file are the same. MetaPhlAN would add ```_taxonomic_profile``` in the samples header. 
You could format your input using following comand line.
```
cat merge.mpa.tsv|awk '{$2=null;print $0}'|sed -e 's/ /\t/g'|le|sed -e 's/\t\t/\t/g'|sed -e 's/_taxonomic_profile//g' > input_prof.tsv
```

- **Demo input**  

| ID | sample1 | sample2 |
|---|---|---|
| k__Bacteria | 99.99223 | 99.99632 |
| k__Bacteria\|p__Actinobacteria | 1.18412 | 1.79678 |
| k__Bacteria\|p__Actinobacteria\|c__Actinobacteria | 0.87506 | 0.78075 |
| k__Bacteria\|p__Actinobacteria\|c__Actinobacteria\|o__Actinomycetales | 0 | 0.00045 |
| k__Bacteria\|p__Actinobacteria\|c__Actinobacteria\|o__Actinomycetales\|f__Actinomycetaceae | 0 | 0.00045 |
| k__Bacteria\|p__Actinobacteria\|c__Actinobacteria\|o__Actinomycetales\|f__Actinomycetaceae\|g__Actinobaculum | 0 | 0 |
| k__Bacteria\|p__Actinobacteria\|c__Actinobacteria\|o__Actinomycetales\|f__Actinomycetaceae\|g__Actinobaculum\|s__Actinobaculum_sp_oral_taxon_183 | 0 | 0 |
| k__Bacteria\|p__Actinobacteria\|c__Actinobacteria\|o__Actinomycetales\|f__Actinomycetaceae\|g__Actinomyces | 0 | 0.00045 |
| k__Bacteria\|p__Actinobacteria\|c__Actinobacteria\|o__Actinomycetales\|f__Actinomycetaceae\|g__Actinomyces\|s__Actinomyces_graevenitzii | 0 | 0 |
| k__Bacteria\|p__Actinobacteria\|c__Actinobacteria\|o__Actinomycetales\|f__Actinomycetaceae\|g__Actinomyces\|s__Actinomyces_hongkongensis | 0 | 0 |

### 2. Group information (TSV file)
- **Demo input**  

| ID | Group |
|---|---|
| sample1 | case |
| sample2 | case |
| sample3 | control |
| sample4 | control |
* REMINDER: all analysis modules were impletmented based on two group comparision, your group information could only contains two different group names.


