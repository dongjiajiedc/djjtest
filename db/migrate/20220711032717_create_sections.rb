class CreateSections < ActiveRecord::Migration[6.0]
  def change
    create_table :sections do |t|
      t.string :section_name
      t.string :sample_name
      t.string :project_name
      t.string :tissue_name
      t.string :c_tissue_source
      t.string :c_tissue_type
      t.string :c_storage_type
      t.string :c_metastasis
      t.string :c_collection_way
      t.string :c_collection_position
      t.integer :n_collection_time
      t.string :c_collection_time_unit
      t.string :c_collection_stage

      t.references :sample, null: false, foreign_key: true
      t.references :project, null: false, foreign_key: true

      t.timestamps
    end
  end
end
