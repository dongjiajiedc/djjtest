import Crux from "crux";
import template from "./template.bvt";
import {Matrix} from "./matrix";
import {Axis} from "./axis";

export class CorrelationPI extends Crux.Component {
    public static components = { Matrix, Axis };
    public render = Crux.t`${template}`;
}