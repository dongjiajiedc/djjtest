import Crux from "crux";

import template from "./template.bvt";
import{Hexagon} from "./Hexagon";
import{LegendComp} from "../../spatial-common/legendcomp"
import { interpolateRgb } from "d3-interpolate";
import {ColorSchemeNumeric} from "../../spatial-common/defs";

export class HexagonPI extends Crux.Component {

    public static components = { Hexagon, LegendComp};
    public render = Crux.t`${template}`;

    public didUpdate() {
        //console.log("component", PloidyPI.components);
        const g = (this.$ref.hexagon as any).$geometry;
        this.$v.size.height = g.height + 50;
        this.$v.size.width = g.width + 110;
        console.log('didupdate');
    }

    public setDensityColor() {
        const endColor= this.$v.data.share.colors.cont.density;
        const keys = this.$v.data.share.methodKeySet;
        const values = this.$v.data.density;

        console.log(keys);

        keys.forEach(k=>{
            console.log('start');
            let startColor = interpolateRgb("white", endColor)(0.2);
            let min = Math.min(...values[k]), max = Math.max(...values[k]);
            this.$v.data.share.colorSchemes.meta[k] = ColorSchemeNumeric.create([startColor, endColor], { type: "linear", domain: [min, max] });
            console.log('fin');
        });
        console.log('pi den fin');
    }
}
