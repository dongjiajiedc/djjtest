
import Oviz from "crux";
import * as math from "mathjs";
import { Color } from "crux/dist/color";
import { Component, ComponentOption } from "crux/dist/element";
import { findBoundsForValues } from "utils/maths";
import template from "./xyz.bvt";

interface XYZPlotOption extends ComponentOption {
    
    xRange: [number, number];
    yRange: [number, number];
    zRange: [number, number];

    // plotSideLength: number;
    
    plotWidth: number; // x
    plotHeight: number; // y
    plotLength: number; //z
    
    viewAngle: number;
    angleIsDegree: boolean;
    
    data: any[];
    dataHandler: (data: any[]) => void;
}

export class XYZPlot extends Component<XYZPlotOption> {

    private xRange;
    private yRange;
    private zRange;
    private _xScale: any;
    private _yScale: any;
    private _zScale: any;

    private origin;
    private axises;

    protected dataHandler;
    
    private focusScatter;

    private points = { 
        a1: [0,1,0],
        b1: [1,1,0],
        c1: [1,1,1],
        d1: [0,1,1],
        a2: [0,0,0],
        b2: [1,0,0],
        c2: [1,0,1],
        d2: [0,0,1],
    };

    public render() {
        return this.t`${template}`;
    }

    public didCreate() {

        this.xRange = this.prop.xRange;
        this.yRange = this.prop.yRange;
        this.zRange = this.prop.zRange;

        ["x","y","z"].forEach(c => {
            this[`_${c}Scale`] = this.createScale(c);
        }) 
        // process raw data
        if (this.prop.dataHandler) this.dataHandler = this.prop.dataHandler;
        else this.dataHandler = (data) => data.forEach(d => {
            d.data = {...d};
        });
        this.dataHandler(this.prop.data);
        
        Object.keys(this.points).forEach(k => {
            this.points[k] = [this.xRange[this.points[k][0]], this.yRange[this.points[k][1]],this.zRange[this.points[k][2]]];
        })
        // find pos
        // const p = this.getP(this.prop.viewAngle);
        // this.prop.data.forEach(d => {
        //     d.pos = math.multiply(p, d.vector)["_data"];
        // });
    }

    public willRender() {
        
    }

    private createScale(axis:string) {
        switch (axis) {
            case "x":
                return this._createScale(
                    "linear",
                    false,
                    this.xRange,
                    [0, this.prop.plotWidth],
                );
            case "y":
                return this._createScale(
                    "linear",
                    false,
                    this.yRange,
                    [0, this.prop.plotHeight],
                );
            case "z":
                return this._createScale(
                    "linear",
                    false,
                    this.zRange,
                    [0, this.prop.plotLength],
                );
        }
        // const scale = this._createScale(
        //     "linear",
        //     false,
        //     this.xRange,
        //     [0, this.prop.plotSideLength],
        // );
        // return scale;
    }

    public defaultProp() {
        return {
            ...super.defaultProp(),
            plotWidth: 200,
            plotHeight: 200,
            plotLength: 200,
            viewAngle: -45,
            angleIsDegree: true,
            // getColor: (pos: number) => "green",
            // getScatterColor: (pos: number) => "#aaa",
        };
    }

    protected getP(angle, degree = true) {
        if (degree) return math.matrix([[1,0,Math.cos(Math.PI * angle / 180)/2],[0,1,Math.sin(Math.PI * angle / 180)/2],[0,0,0]]);
        else math.matrix([[1,0,Math.cos(angle)/2],[0,1,Math.sin(angle)/2],[0,0,0]]);
        // return math.matrix([[1,0,Math.cos(1.25 * Math.PI)/2],[0,1,Math.sin(1.25 * Math.PI)/2],[0,0,0]]);
    }

    // protected getXp(angle) {
    //     const theta = Math.PI * angle / 180;
    //     return math.matrix([
    //         [Math.cos(theta),-Math.sin(theta),0],
    //         [Math.sin(theta),Math.cos(theta),0],
    //         [0,0,1]
    //     ]);
    // }
    // protected getYp(angle) {
    //     const theta = Math.PI * angle / 180;
    //     return math.matrix([
    //         [1,0,0],
    //         [0,Math.cos(theta),-Math.sin(theta)],
    //         [0,Math.sin(theta),Math.cos(theta)]
    //     ]);
    // }
    // protected getZp(angle) {
    //     const theta = Math.PI * angle / 180;
    //     return math.matrix([
    //         [Math.cos(theta),0,Math.sin(theta)],
    //         [0,1,0],
    //         [-Math.sin(theta),0,Math.cos(theta)],
    //     ]);
    // }


    protected getXp(angle) {
        const theta = Math.PI * angle / 180;
        return math.matrix([
            [1,0,0,0],
            [0,Math.cos(theta),-Math.sin(theta),0],
            [0,Math.sin(theta),Math.cos(theta),0],
            [0,0,0,1]
        ]);
    }

    protected getYp(angle) {
        const theta = Math.PI * angle / 180;
        return math.matrix([
            [Math.cos(theta),0,Math.sin(theta),0],
            [0,1,0,0],
            [-Math.sin(theta),0,Math.cos(theta),0],
            [0,0,0,1]
        ]);
    }
    
    protected getZp(angle) {
        const theta = Math.PI * angle / 180;
        return math.matrix([
            [Math.cos(theta),-Math.sin(theta),0,0],
            [Math.sin(theta),Math.cos(theta),0,0],
            [0,0,1,0],
            [0,0,0,1]
        ]);
    }
    

    // protected getInversedP() {
    //     const p = this.getP(this.prop.viewAngle);
    //     // return math.inv();
    //     console.log(p);
    //     return math.inv(p);
    // }

    protected _vectorScale(vector) {
        return [vector[0] * this.prop.plotWidth, vector[1] * this.prop.plotHeight, vector[2] * this.prop.plotLength];
    }

    protected getPos(vector) {
        const p = this.getP(this.prop.viewAngle);
        const v = this._vectorScale(vector);
        // console.log(v);
        const pos = math.multiply(p, v)["_data"];
        // console.log(pos)
        return pos;
    }
    // protected getDataPos(vector) {
    //     // const p = this.getP(this.prop.viewAngle);
    //     const t = math.matrix([
    //         [1,0,0,0],
    //         [0,1,0,0],
    //         [0,0,1,0],
    //         [this.prop.plotWidth/2, this.prop.plotHeight/2, this.prop.plotLength/2,1],
    //     ]);
    //     const xp = this.getXp(30);
    //     const yp = this.getYp(60);
    //     const zp = this.getZp(45);
    //     const v = [this._xScale(vector[0]),this._yScale(vector[1]),this._zScale(vector[2])];
    //     // console.log(v);
    //     // const pos = math.multiply(zp,math.multiply(yp,math.multiply(xp, v)))["_data"];
    //     const pos = math.multiply(zp,math.multiply(yp,math.multiply(xp, math.multiply(t,v))))["_data"];
    //     return pos.slice(0,2);
    // }
    protected getDataPos(vector) {
        const p = this.getP(this.prop.viewAngle);
        const v = [this._xScale(vector[0]),this._yScale(vector[1]),this._zScale(vector[2])];
        const pos = math.multiply(p, v)["_data"];
        // const t = math.matrix([
        //     [1,0,0,0],
        //     [0,1,0,0],
        //     [0,0,1,0],
        //     [this.prop.plotWidth/2, this.prop.plotHeight/2, this.prop.plotLength/2,1],
        // ]);
        // const xp = this.getXp(0);
        // const yp = this.getYp(0);
        // const zp = this.getZp(0);
        // const v = [this._xScale(vector[0]),this._yScale(vector[1]),this._zScale(vector[2])];
        // const temp = this.matricesMultiplication([zp,yp,xp,[...v,0]])["_data"];
        // const pos = math.multiply(p, math.matrix(temp.slice(0,3)))["_data"];
        return pos.slice(0,2);
    }

    protected matricesMultiplication(matrices: any[]) {
        if (!matrices && matrices.length <2) {
            window.alert("matricesMultiplication must provide more than two matrices");
            throw new Error("matricesMultiplication: invalid argument")
        }
        if (matrices.length === 2)
            return math.multiply(matrices[0],matrices[1]);
        else {
            const rest = matrices.slice(1);
            return math.multiply(matrices[0], this.matricesMultiplication(rest));
        } 
    }

    public setFocusScatter(vector) {
        this.focusScatter = vector;
        this.redraw();
    }

    public getRefFlats() {
        // a1 b1 c1 d1
        const flats = [
            ["a1", "b1", "c1", "d1"],
            ["a1", "d1", "d2", "a2"],
            ["d1", "c1", "c2", "d2"],
        ];
        
        return flats.map(points => points.map(k => this.getDataPos(this.points[k])));
    }

    // protected test(ev, el) {
    //     const pos = [ev.offsetX - (this.prop.x as number),
    //         ev.offsetY-(this.prop.y as number), 0];
    //     const vector = math.multiply(this.getInversedP(), pos);
    //     console.log(vector);
    //     // console.log([ev.offsetX - this.prop.x, ev.offsetY-this.prop.y]);
    // }
}

