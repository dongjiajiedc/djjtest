class CreateTissues < ActiveRecord::Migration[6.0]
  def change
    create_table :tissues do |t|
      t.string :tissue_name
      t.string :c_tissue_source
      t.string :description
      t.string :data_source

      t.timestamps
    end
  end
end
