import Oviz from "crux";
import template from "./template.bvt";

import { minmax } from "crux/dist/utils/math";
import * as d3 from "d3";
import { schemeRdYlBu, schemeRdBu, interpolateSpectral } from "d3-scale-chromatic";
import { SpatialImage } from "spatial-common/spatial_image";
import { processMeta, processMain, processHist, processLeftBox } from "spatial-common/spatial_data";
import { event } from "crux/dist/utils";

export class BarView extends Oviz.Component {
    public colors: any;
    public ovMain: any;
    public ovTree: any;
    public mainHeatmap: number[][];
    public filteredRows: string[];
    public filteredSpots: string[];
    public rows: string[];
    public mainColorGetter: any;

    public spotDeconvData: any;
    public spotMetaData: any;

    public mainDict;

    public metaFeatures;
    public metaDict;
    public metaInfo;
    public metaData;
    public boxplot;

    public sampleOrderChanged = true;
    public gridSize;
    public valueRange;
    // private colors = ["pink", "skyblue"];
    private fullDisplay = true;

    public imageWidth = 200;    
    public windowWidth = 30;
    public windowX = 85;
    public windowY = 85;


    public sizeSettings = {
        offsetX: 250, // 150,
        mainHeight: 300,
        mainWidth: 1200,
        barHeight: 200, // 180,
        boxHeight: 220,
        padding: 20,
        gapX: 10,
        gapY: 10,
        gridW: 8,
        gridH: 12,
    };
    public yPos = 0;
    public gridW = 0;
    public gridH = 0;
    public mainHeight = 300;
    public mainWidth = 1200;
    public controllerMode = "scroll";
    public offsetX = 250; // 150;

    public rowsMap = {};

    public histLegendLabels;
    public mainRange = [];
    public mainGradientFills = [];
    public histLegendPos = {x: 1000, y: 20};
    public mainLegendPos = {x: 1250, y: 520};
    public boxLegendPos = {x: 1250, y: 580};

    public spatialImageConfig = {
        imageX: 10, imageY: 10, imageWidth: 100,
        windowWidth: 15, windowHeight: 15,
        brushMaxDeltaX: 15, brushMaxDeltaY: 15,
        inspectWidth: 100, inspectPosition: 'bottom',
        spotR: 0.5,
    }

    public expressionData; filteredMarkers;

    public debug = {};

    public _sizeUpdated = true;

    public static components = { SpatialImage};

    public render() {
        
        return this.t`${template}`;
    }

    public didCreate(){
        event.on("activeSpots", (_, spots) => {
            //console.log('receve spots', spots);
            this.filteredSpots = spots;
            processMeta(this, spots);
            processMain(this, this.spotDeconvData, [])
            processHist(this, this.spotDeconvData)
            this.updateSize()
            this.$v.forceRedraw = true;
            this.redraw();
        })       
    }

    public willRender() {
        if (this._firstRender) {
            //meta(this, this.spotMetaData, this.spotDeconvData)
            processMeta(this)
            processMain(this, this.spotDeconvData, [])
            processHist(this, this.spotDeconvData)
            processLeftBox(this)
            
            this.offsetX = 150;
            this.sizeSettings.offsetX = 150;
            this.sizeSettings.barHeight = 250;

            this.mainWidth = 1200;
            

            this.gridW = this.gridSize[0];
            this.gridH = this.gridSize[1];
            this.histLegendLabels = this.rows;


            const mainH = this.rows.length * this.gridH;
            if (mainH < this.mainHeight) {
                this.mainHeight = this.sizeSettings.mainHeight = mainH;
            } else {
                this.gridH = this.mainHeight / this.rows.length;
            }
            const mainW = this.filteredSpots.length * this.gridW;
            if (mainW < this.mainWidth) {
                this.mainWidth = this.sizeSettings.mainWidth = mainW;
            } else {
                this.gridW = this.mainWidth / this.filteredSpots.length;
            }
            this.histLegendPos.x = this.offsetX + this.mainWidth + this.sizeSettings.gapX;
        
            this._sizeUpdated = true;
        }
        if (this._sizeUpdated) {
            this.updateSize()
        }        
    }

    public updateSize(){
        this._sizeUpdated = false;
        this.gridW = this.gridSize[0];
        this.gridH = this.gridSize[1];
        this.mainWidth = this.filteredSpots.length * this.gridW;
        this.$v.size.width = this.mainWidth + this.sizeSettings.boxHeight
            + this.offsetX + 2 * this.sizeSettings.gapX + 2 * this.sizeSettings.padding;
        
        this.mainHeight = this.rows.length * this.gridH;
        this.$v.size.height = this.mainHeight + this.metaFeatures.length * this.gridH +
        + this.sizeSettings.barHeight + 2 * this.sizeSettings.gapY + 2 * this.sizeSettings.padding;
    
    
        
        this.histLegendPos = {x: this.sizeSettings.offsetX + this.mainWidth
            + this.sizeSettings.padding,
            y: this.sizeSettings.padding };
        this.mainLegendPos = {x: this.sizeSettings.offsetX + this.mainWidth
            + this.sizeSettings.boxHeight - 80,
            y: this.sizeSettings.barHeight + this.sizeSettings.mainHeight 
            + this.sizeSettings.padding + 20};
        this.boxLegendPos = {x: this.sizeSettings.offsetX + this.mainWidth
            + this.sizeSettings.boxHeight,
                y: this.sizeSettings.barHeight + this.sizeSettings.mainHeight 
                + this.sizeSettings.padding + 80};
    }
    
    public state = {
        activeX: null,
        activeY: null,
        newX: null,
        newHeight: null,
        mode: null,
        updated: null,
        dragStartPos: null,
        legendPos: null,
        spots: null,
    };

    private setActive(x: number|string, y: number = null) {
        if (typeof x === "string") {
            const xPos = this.filteredSpots.indexOf(x) * this.gridW + this.offsetX;
            this.setState({activeX: xPos});
        } else
            this.setState({ activeX: x, activeY: y });
    }

    private controlMain(ev) {
        if (this.controllerMode === "zoom") {
            this.updateRange(ev);
        } else {
            this.$v.forceRedraw = false;
            this.updatePos(ev);
        }
    }
    private updateRange(ev) {
        const newHeight = (1 - ev.deltaY  / 1000) * this.mainHeight;
        this.mainHeight = newHeight > this.rows.length * this.gridH ? this.rows.length * this.gridH
                        : newHeight < 300 ? 300 : newHeight;
        this.setState({newHeight: this.mainHeight});
    }
    private updatePos(ev) {

        this.yPos = this.yPos + ev.deltaY > 0 ? 0
                : this.yPos + ev.deltaY < -this.mainHeight + 300
                    ? -this.mainHeight + 300 : this.yPos + ev.deltaY;
        this.setState({newX: this.yPos});
    }

    private swicthMode() {
        if (this.controllerMode === "zoom") {
            this.$v.forceRedraw = false;
            this.controllerMode = "scroll";
        } else {
            this.$v.forceRedraw = true;
            this.controllerMode = "zoom";
        }
        this.setState({ mode: this.controllerMode });
    }

    private fitSize() {
        this.fullDisplay = !this.fullDisplay;
        this.yPos = 0;
        this.$v.size.height = this.fullDisplay ? this.mainHeight + 600 : 1000;
        this.$v.run();
        // this.setState({updated: true});
    }

    protected handleLegendPos(_, el, deltaPos: [number, number]) {
        switch (el.id) {
            case "histLegend":
                this.histLegendPos = {x: this.histLegendPos.x + deltaPos[0],
                    y: this.histLegendPos.y + deltaPos[1]};
                break;
            case "mainLegend":
                this.mainLegendPos = {x: this.mainLegendPos.x + deltaPos[0],
                    y: this.mainLegendPos.y + deltaPos[1]};
                break;
            case "boxLegend":
                this.boxLegendPos = {x: this.boxLegendPos.x + deltaPos[0],
                    y: this.boxLegendPos.y + deltaPos[1]};
                break;
        }

        this.redraw();
    }

    protected handleWindowPos(_, el, deltaPos: [number, number]) {

        if (this.windowX + deltaPos[0] >= 0 &&
            this.windowX + deltaPos[0] <= this.imageWidth - this.windowWidth &&
            this.windowY + deltaPos[1] >= 0 &&
            this.windowY + deltaPos[1] <= this.imageWidth - this.windowWidth){
                this.windowX = this.windowX + deltaPos[0];
                this.windowY = this.windowY + deltaPos[1];

                this._firstRender = true;
                this.$v.forceRedraw = true;
                console.log('rerender')
                this.willRender();
                //this.redraw();
            }
    }
    public prettyPrint(x: any){
        if (parseInt(x) == x){
            return x;
        }else{
            return x.toFixed(2);
        }
    }    
}
