class SpeciesController < ApplicationController

    def index
    end
    def show
        @vis = ['id', 'sample_name', 'project_name', 'c_description', 'species_name', 'c_study_type', 'c_country', 'c_ethnic', 'n_age', 'c_age_unit', 'c_cancer_name']

        @user = User.find(session[:user_id])
        @species = Species.find(params[:id])
        @attrs = Species.column_names
        @sample_attrs = Sample.column_names
        # @projects = @cancer.projects
        @invis = []
        @sample_attrs.each_with_index do |attr, index|
            if !@vis.include?(attr)
                @invis.push(index+1)
            end
        end
        gon.push invis: @invis
        
        id = session[:user_id]
        @user = User.find(id) 
        @datasets = @user.datasets
        respond_to do |format|
            format.html
            # format.csv { send_data @cancer.projects.to_csv }
            format.json { render json: SampleDatatable.new(view_context, 'species', @species.species_name) }
        end
    end
end
