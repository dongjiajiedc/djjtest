class SlicesController < ApplicationController

    def index
        @vis = ['id','slice_name' ,'section_name', 'sample_name', 'project_name', 'c_protocol', 'n_layer', "n_diameter","n_number_of_genes"]

        @slices = Slice.order(:section_name)
        @attrs = Slice.column_names
        @invis = []
        @attrs.each_with_index do |attr, index|
            if !@vis.include?(attr)
                @invis.push(index+1)
            end
        end
        gon.push invis: @invis

        respond_to do |format|
            format.html
            format.csv { send_data @slices.to_csv }
            format.json { render json: SliceDatatable.new(view_context) }
        end
    end

    def show
        @slice = Slice.find(params[:id])
        # @long_attrs = ["c_description"]
        # @link_attrs = []
        @short_attrs = [["slice_name","section_name","sample_name","project_name"],[ "c_protocol","n_layer", "n_diameter", "n_number_of_genes"]]


    end

    def export_selected
        if params[:selected_ids] !=nil
            @slices = Section.order(:slice_name)
            send_data @slices.selected_to_csv(params[:selected_ids])
        end
    end

    def make_selected_file
        if params[:download_select]
            download_selected_samples
        elsif params[:download_filter]
            download_filtered_samples
        elsif params[:seqence]
            "nothing"
        end
    end

    def download_filtered_samples
        time = Time.now
        time_str = time.strftime("%Y_%m_%d")       
        time_str += ("_" + time.strftime("%k_%M")) 
        time_str = time_str.gsub(' ','')
        if params[:section_id]
            @section = Section.find(params[:section_id])
            ids = Slice.filtered(params[:search_value], @section)
            send_data Slice.selected_to_csv(ids), :filename => "#{time_str}_selected_metadata.csv"
        else
            ids = Slice.filtered(params[:search_value])
            send_data Slice.selected_to_csv(ids), :filename => "#{time_str}_selected_metadata.csv"
        end
    end

    def download_selected_samples
        time = Time.now
        time_str = time.strftime("%Y_%m_%d")       
        time_str += ("_" + time.strftime("%k_%M")) 
        time_str = time_str.gsub(' ','')
        @slices = Slice.order(:slice_name)
        send_data @slices.selected_to_csv(params[:selected_ids]), :filename => "#{time_str}_selected_metadata.csv"
    end

    
end
