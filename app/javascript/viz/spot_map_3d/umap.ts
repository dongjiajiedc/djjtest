import { Component, ComponentOption } from "crux/dist/element";
import { template } from "crux/dist/template/tag";
import { ColorSchemeNumeric } from "../../spatial-common/defs";
import {event} from "crux/dist/utils";

interface UmapOptions extends ComponentOption{
    cluster_display: boolean;
    cluster_scheme:  ColorSchemeNumeric;
    exp_scheme:  ColorSchemeNumeric;
    cluster_key: string;
    colors: {};
}

export class Umap extends Component<UmapOptions>{
    protected state = { spots: []}

    render(){
		return this.t`
		Component {
            width = 800
            XYPlot {
                width = 800; height = 600
                x = 700; y = 20

                // scale
                padding = 100
                discreteCategory = false
                categoryRange = [prop.min_x, prop.max_x]
                valueRange = [prop.min_y, prop.max_y]
                
                // axis
                Axis("bottom") { y = 100% }
                Axis("left");
                
                data = {
                    default: {
                        data: prop.umap_displayed_data
                    }
                }

                dataHandler = {
                    default: {
                        values: d => d.data,
                        pos: d => d.x,
                        value: d => d.y,
                    }
                }

                Dots {
                    data = "default"
                    :children (data){
                        Component{
                            @let spot = data['data']['idx']
                            @let isActive = state.spots.includes(spot)
                            Path{
                                d= getHex(4)
                                stroke= isActive? "yellow" : "grey";
                                style:stroke-width=1
                                style:stroke-dasharray="4,2"
                                fill= getHexColor(prop, data)
                                on:mouseenter = setActiveSpot(spot)
                                on:mouseleave = setActiveSpot(null)
                                behavior:tooltip {
                                    content = getHexInfo(prop, data);
                                }
                            }
                        }
                    }
                }
            }
        }
        `
    }

    private getHex(radius:number) {
        var x0 = 0, y0 = 0;
        var thirdPi = Math.PI / 3,
        angles = [0, thirdPi, 2 * thirdPi, 3 * thirdPi, 4 * thirdPi, 5 * thirdPi];
        var hexa = angles.map(function(angle) {
          var x1 = Math.sin(angle) * radius,
              y1 = -Math.cos(angle) * radius,
              dx = x1 - x0,
              dy = y1 - y0;
          x0 = x1, y0 = y1;
          return [dx, dy];
        });
        return "m" + hexa.join("l") + "z";
     }
    
    private getHexInfo(prop, d){
        var idx = parseInt(d['data']['idx'])
        var x = d['data']['x']
        var y = d['data']['y']
        var item = prop.displayed_data[idx]
        var barcode = item['barcode']
        var text = "Barcode: " + barcode + "<br>" + "x: " + x + "<br>" + "y: " + y
        return text
    }
    

	private getValue(x: any){
		if (x == 0) {
			return 0;
		}
		return Math.log(x)
	}

    private getHexColor(prop, d){
        var idx = parseInt(d['data']['idx']) 
        var item = prop.displayed_data[idx]

        //var normalized_value = item['value'] / prop.max_value
        var normalized_value = this.getValue(item['value'])

        var cluster_idx = item[this.prop.cluster_key]
        if (this.prop.cluster_display) 
            return this.prop.cluster_scheme.get(cluster_idx)
        else
            return this.prop.exp_scheme.get(normalized_value)
    }

    private setActiveSpot(spot){
        event.emit("activeSpots", [spot])
    }

    public didCreate(){
        event.on("activeSpots", (_, spots) => this.setState({ spots }))
    }
}