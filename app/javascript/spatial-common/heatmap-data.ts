import { ColorSchemeCategory } from "crux/dist/color";
import { interpolateRgb } from "d3-interpolate";
import { parseNewick } from "crux/dist/utils";
import { ZoomRegion, ColorSchemeNumeric, generateColors, str2int, interpolateTurbo, uniformColors } from "./defs";
import { createOnUndefined, diffStartAt, sortLabels, isArray, arrAvg } from "./support_func";
import { color } from "crux/node_modules/@types/d3";

export function cnvProcess(this: any, cnv: any[]) {
    const share = createOnUndefined(this.data, "share");
    share.segByChrOrigin = extractSegByChr(Object.keys(cnv[0]).slice(1)); // Ignore the first id header
    share.cnvByIDOrigin = createDateByID(cnv);
}

export function metaProcess(this: any, meta: any[]) {
    const optFiles = createOnUndefined(this.data, "optFiles");
    optFiles.meta = !(!meta);
    if (!meta) return;

    const share = createOnUndefined(this.data, "share");
    share.metaByID = createDateByID(meta); // Must before metaKeys init

    const discreteMeta = [];
    const continuousMeta = [];
    const metaKeys = Object.keys(meta[0]).slice(0, -1); // remove "id"
    metaKeys.forEach(mk => {
        if (mk.startsWith("c_") || mk.startsWith("e_"))
            meta.forEach(m => {
                const trimKey = mk.substring(2);
                continuousMeta.push(trimKey);
                m[trimKey] = m[mk];
                delete m[mk];
            })
        else
            discreteMeta.push(mk);
    })

    share.discreteMeta = discreteMeta;
    share.continuousMeta = [...new Set(continuousMeta)];
}

export function treeCutProcess(this: any, treeCutInfo: any) {
    const optFiles = createOnUndefined(this.data, "optFiles");
    optFiles.tree = !(!treeCutInfo);
}

export function cytoProcess(this: any, cytoData) {
    let cytoLength = {}, totalLength = 0, addCytoLength = {};

    cytoData.forEach((element) => {
        cytoLength[element['chr']] = parseInt(element['endPos']);
    });

    for (let i = 1; i <= 22; i++) {
        addCytoLength['chr' + i] = totalLength
        totalLength += cytoLength['chr' + i];
    };

    addCytoLength['chrX'] = totalLength
    totalLength += cytoLength['chrX'];
    addCytoLength['chrY'] = totalLength
    totalLength += cytoLength['chrY'];

    return { cytoLength, totalLength, addCytoLength };
}

export function extractBinInfo(str: string) {
    const token = str.split(/[:-]/) as any[];
    token[1] = parseInt(token[1]);
    token[2] = parseInt(token[2]);
    return token;
}

export function pos2seg(data: any, chr: string, pos: number): string {
    // Check valid segment
    const segByChr = data.share.segByChr;
    const chrs = Object.keys(segByChr);
    const validSeg = chrs.includes(chr) && 1 <= pos && pos <= data.cytoData.cytoLength[chr];
    if (!validSeg) return "Invalid";

    const unitLength = data.share.unitLength;
    const start = pos - pos % unitLength + 1;

    const segInChr = segByChr[chr];
    const lastSeg = segInChr[segInChr.length - 1];
    if (start === extractBinInfo(lastSeg)[1]) return lastSeg;
    return `${chr}:${start}-${start + unitLength - 1}`;
}

// function search(arr,num) {
//     var l=arr.length;
//     var left=0;
//     var right=l-1;
//     var center=Math.floor((left+right)/2);
//     while(left<=l-1&&right>=0){
//         if (arr[center]==num) return center;
//         if (left==right) return "您查找的数不存在";
//         if (arr[center]>num) {
//             right=center-1;
//             center=Math.floor((left+right)/2);
//         }else if (arr[center]<num) {
//             left=center+1;
//             center=Math.floor((left+right)/2);
//         }
//     }
// }
// var a=[1,2,3,4,5,6,7,8,9];
// console.log(search(a,-2));

function extractSegByChr(binHeaders: string[]): any {
    const segByChr: Record<string, string[]> = {};
    const chrs = [];

    binHeaders.forEach(h => {
        const binInfo = extractBinInfo(h);
        if (binInfo.length === 3) {
            const chr = binInfo[0];
            if (!chrs.includes(chr)) {
                segByChr[chr] = [];
                chrs.push(chr);
            }
            segByChr[chr].push(h);
        }
    });

    return segByChr;
}

function createDateByID(data: any[]) {
    const tblHeads = Object.keys(data[0]);
    const dataByID = {};

    data.forEach(d => {
        const id = d[tblHeads[0]];
        d.id = id;
        dataByID[id] = d;
        delete d[tblHeads[0]];
    })
    return dataByID;
}

export function integrateData(data: any) {
    createKeyOrders(data);
    createColorScheme(data);
    repairCNV(data);
    createZoomRegions(data);
    data.curr_zoom_region = data.zoom_regions[0]; // zoom_region Initialization

    if (data.optFiles.tree) {
        updateNodeCNV(data, data.config.root, data.curr_zoom_region);
        data.config.autoload = true; // autoload on tree mode
    } else {
        data.curr_zoom_region.cnvByNode = data.curr_zoom_region.cnvByIDZoomed;
    }
}

export function createKeyOrders(data: any) {
    const share = data.share;
    const keyOrders: any = {};

    // cell
    keyOrders["cellOrigin"] = Object.keys(share.cnvByIDOrigin);
    keyOrders["cellAll"] = keyOrders["cellOrigin"].map(d => d);
    keyOrders["cell"] = keyOrders["cellOrigin"].map(d => d);

    // meta
    keyOrders["meta"] = data.optFiles.meta ? Object.keys(data.meta[0]).filter(mk => mk != "id") : [];

    // chr & seg
    const chrsOrigin = Object.keys(share.segByChrOrigin);
    const chrStartAt = diffStartAt(chrsOrigin) as number;
    const chrPrefix = chrsOrigin[0].substring(0, chrStartAt);
    const chrs = [];
    for (let i = 1; i <= 22; ++i)
        chrs.push(chrPrefix + i);
    keyOrders["chr"] = chrs.concat(chrsOrigin.filter(x => !chrs.includes(x)));
    keyOrders["segOrigin"] = flatSegByChr(share.segByChrOrigin, keyOrders["chr"]);

    share.keyOrders = keyOrders;
    share.chrStartAt = chrStartAt;
    share.chrPrefix = chrPrefix;
}

export function createColorScheme(data: any) {
    const share = data.share;

    share.colorSchemes = {};
    share.metaValues = {}
    share.colors = { cont: {} };
    if (!data.optFiles.meta) {
        share.discreteMeta = [];
        share.labels = [];
    }
    share.discreteMeta.forEach(mk => share.colors[mk] = {});

    share.colorSchemes["heatmap"] = new ColorSchemeNumeric(["#367FBA", "white", "#A4444C", "black"], { type: "linear", domain: [0, 2, 6, 10], numLegendPoints: 10 });
    share.colorSchemes["meta"] = {};
    if (data.optFiles.meta) {
        createContinuousColorScheme(data);

        const labels = [];
        share.discreteMeta.forEach((mk: string) => {
            const metaValues: string[] = [...new Set(data.meta.map(d => d[mk]))] as string[];
            metaValues.sort(sortLabels);
            metaValues.forEach(label => {
                labels.push({
                    category: mk,
                    label: label,
                    _display: true,
                })
            })
            share.metaValues[mk] = metaValues;
            createDiscreteColorScheme(data, mk);
        });
        share.labels = labels;
        // let lastColorPosition: number;
        // let colorPosition: number;
        // let str = mk;
        // do {
        //     colorPosition = str2int(str);
        //     str += "h";
        // } while (lastColorPosition && Math.abs(lastColorPosition - colorPosition) < 0.3);
        // lastColorPosition = colorPosition; // Update lastColorPosition
        // const endColor = interpolateTurbo(colorPosition);
        // // const endColor = generateColors(1, mk)[0];
        // const startColor = interpolateRgb("white", endColor)(0.2);
        // colorSchemes["meta"][`${mk}`] = ColorSchemeNumeric.create([startColor, endColor], { type: "linear", domain: [min, max] });
    }
}

export function createContinuousColorScheme(data: any, colorDict?: any) {
    const share = data.share;
    if (typeof colorDict === "undefined") {
        colorDict = {};
        const colors = uniformColors(share.continuousMeta.length);
        share.continuousMeta.forEach((mk, idx) => colorDict[mk] = colors[idx]);
    }
    share.continuousMeta.forEach(mk => {
        const metaValues: number[] = data.meta.map(d => parseFloat(d[mk]));
        const [min, max] = [Math.min(...metaValues), Math.max(...metaValues)];

        let startColor, endColor;
        endColor = colorDict[mk];
        startColor = interpolateRgb("white", endColor)(0.2);
        share.colorSchemes.meta[mk] = ColorSchemeNumeric.create([startColor, endColor], { type: "linear", domain: [min, max] });
    });

    share.colors.cont = colorDict;
}

export function createDiscreteColorScheme(data: any, meta: string, colorDict?: any) {

    const share = data.share;
    const metaValues = share.metaValues[meta] as string[];
    if (typeof colorDict === "undefined") {
        colorDict = {};
        const colors = generateColors(metaValues.length, meta);
        metaValues.forEach((v, idx) => colorDict[v] = colors[idx]);
    }
    share.colorSchemes.meta[meta] = ColorSchemeCategory.create(metaValues, metaValues.map(v => colorDict[v]));
    share.colors[meta] = colorDict;
}

export function repairCNV(data: any) {
    const share = data.share;
    const cytoData = data.cytoData;

    const keyOrders = share.keyOrders;
    const cnvByIDOrigin = share.cnvByIDOrigin;
    const cytoLength = cytoData.cytoLength;
    const segByChrOrigin = share.segByChrOrigin;

    const sampleBinInfo = extractBinInfo(Object.keys(data.cnv[0])[0]);
    const unitLength: number = sampleBinInfo[2] - sampleBinInfo[1] + 1;
    const cnvByID = {};
    const segs = [];

    keyOrders.chr.forEach(chr => {
        const cytoKey = "chr" + chr.substring(share.chrStartAt);
        let pos = 1;
        const localLimit = cytoLength[cytoKey] - unitLength;
        while (pos <= localLimit) {
            segs.push(`${chr}:${pos}-${(pos += unitLength) - 1}`);
        }
        const lastKey = `${chr}:${pos}-${cytoLength[cytoKey]}`;
        if (typeof data.cnv[0][lastKey] !== "undefined")
            segs.push(lastKey);
        else {
            const segInChr = segByChrOrigin[chr];
            for (let n = segInChr.length, i = n - 1; i >= 0; --i) {
                if (segInChr[i].startsWith(`${chr}:${pos}-`)) {
                    segs.push(segInChr[i]);
                    break;
                }
            }
        }
    });

    const segByChr = extractSegByChr(segs);

    keyOrders.cellAll.forEach(c => {
        const cnvInCell = {};
        const cnvInCellOrigin = cnvByIDOrigin[c];
        segs.forEach(seg => cnvInCell[seg] = typeof cnvInCellOrigin[seg] === "undefined" ? "N/A" : cnvInCellOrigin[seg]);
        cnvByID[c] = cnvInCell
    })

    share.cnvByID = cnvByID;
    share.segByChr = segByChr;
    share.keyOrders.seg = segs;
    share.unitLength = unitLength;
}

export function zoomCNV(data: any, zoom_region: ZoomRegion, desireWidth?): any {
    desireWidth = typeof desireWidth === "undefined" ? data.layout.desireWidth : desireWidth;

    const noneCase = data.config ? data.config.noneCase : ['', 'N/A'];
    const share = data.share;

    const cnvByID = share.cnvByID;
    const cellAll = share.keyOrders.cellAll;
    const unitLength = share.unitLength;

    const zoomFactor: number = Math.ceil(zoom_region.calcRegionReletiveWidth() / desireWidth);
    const cnvByIDZoomed = {};
    const segByChrZoomed = {};
    const segs = zoom_region.getSegs();
    const segByChr = extractSegByChr(segs);
    const chrs = Object.keys(segByChr);
    console.log("Zooming: on chrom(s) ", chrs);
    cellAll.forEach(c => cnvByIDZoomed[c] = {});

    if (zoomFactor <= 1) { // skip when no zooming
        // zoom_region.cnvByIDZoomed = copyObject(cnvByID);
        // zoom_region.segByChrZoomed = copyObject(segByChr);
        // zoom_region.segZoomed = flatSegByChr(zoom_region.segByChrZoomed);
        console.log("Zooming: factor <= 1!");
        zoom_region.cnvByIDZoomed = cnvByID;
        zoom_region.segByChrZoomed = segByChr;
        zoom_region.segZoomed = segs;
        return;
    }
    chrs.forEach(chr => {
        const segOfChr = segByChr[chr];
        const segsZoomed = [];
        const nColAfterZoomed = Math.ceil(segOfChr.length / zoomFactor);

        for (let i = 0; i < nColAfterZoomed; ++i) {
            const zoomUnitSegs = segOfChr.slice(i * zoomFactor, (i + 1) * zoomFactor);
            const startPos = extractBinInfo(zoomUnitSegs[0])[1];
            const endPos = extractBinInfo(zoomUnitSegs[zoomUnitSegs.length - 1])[2];
            if (endPos - startPos + 1 < zoomFactor * unitLength / 2) break; // Dropout when less than a half unit
            const zoomUnitKey = `${chr}:${startPos}-${endPos}`;
            segsZoomed.push(zoomUnitKey);

            cellAll.forEach(c => {
                const values = zoomUnitSegs.map(k => cnvByID[c][k]);
                const validValues = values.filter(v => !noneCase.includes(v));
                cnvByIDZoomed[c][zoomUnitKey] = validValues.length === 0 || validValues.length < values.length / 2 // Dropout when half of the value are none
                    ? "N/A"
                    : arrAvg(validValues, 3);
            });
        }
        segByChrZoomed[chr] = segsZoomed;
    });

    zoom_region.cnvByIDZoomed = cnvByIDZoomed;
    zoom_region.segByChrZoomed = segByChrZoomed;
    zoom_region.segZoomed = flatSegByChr(segByChrZoomed);
}

function flatSegByChr(segByChr: any, chrOrders?: string[]): string[] {
    const chrs = chrOrders ? chrOrders : Object.keys(segByChr);
    return chrs.flatMap(chr => segByChr[chr])
}

export function createZoomRegions(data: any) {
    ZoomRegion.segRef = data.share.keyOrders.seg;
    data.zoom_regions = [];

    const zoom_region = ZoomRegion.create();
    zoomCNV(data, zoom_region);
    data.zoom_regions.push(zoom_region);

    for (let i = 1; i < data.config.nZoomRegion; i++) {
        const clone = Object.assign(Object.create(Object.getPrototypeOf(zoom_region)), zoom_region);
        data.zoom_regions.push(clone);
    }
}

function setTree(t: any, treeData: any, root: string) {
    const leafRef = treeData[root].leafs // root's global leafs
    if (!t.hasOwnProperty('children')) {
        t.dist_to_root = t["length"]
        t.nodes = [t,]
        t.l_leafs = [t.name,]
        t.links = []
        const leafs = treeData[t.name].leafs // global leafs
        if (leafs.length === 0) leafs.push(t.name); // For the old version of tree data
        t.y = (leafRef.indexOf(leafs[0]) + leafRef.indexOf(leafs[leafs.length - 1]) + 1) / 2
        return;
    }

    t.dist_to_root = t["length"];
    t.nodes = [t,];
    t.l_leafs = [];
    t.links = [];

    t.children.forEach(c => {
        setTree(c, treeData, root);
        c.nodes.forEach(n => {
            n.dist_to_root += t["length"];
        })
        t.nodes.push(...c.nodes);
        t.l_leafs.push(...c.l_leafs);
        t.links.push([t.name, c.name]);
        t.links.push(...c.links);
    })
    t.y = (t.children[0].y + t.children[1].y + 1) / 2
}

export function drawTree(treeData: any, root: string) {
    const currTree = treeData[root]
    if (currTree.hasCompute === true) return;
    const tempTree: any = parseNewick(currTree.newick).children[0];

    tempTree.length = 0;
    tempTree.name = root;
    setTree(tempTree, treeData, root);

    const coords = {};
    tempTree.nodes.forEach(n => {
        coords[n.name] = [n.dist_to_root, n.y];
    });

    currTree.l_leafs = tempTree.l_leafs;
    currTree.links = tempTree.links;
    currTree.coords = coords;
    currTree.lifetime = Math.max(...tempTree.nodes.map(n => n.dist_to_root));
    currTree.hasCompute = true;

    const computeTree = {};
    tempTree.nodes.forEach((node: any) => computeTree[node.name] = node);
    currTree.computeTree = computeTree;
}

export function updateNodeCNV(data: any, localRoot: string, zoom_region?: ZoomRegion) {
    const share = data.share;
    const config = data.config;
    const treeData = data.treeData;

    const noneCase = config.noneCase;
    const keyOrders = share.keyOrders;
    // const segs = zoom_region.segZoomed;
    const cnvByIDZoomed = zoom_region.cnvByIDZoomed;

    drawTree(treeData, localRoot);
    keyOrders["node"] = treeData[localRoot].l_leafs;
    keyOrders["cell"] = treeData[localRoot].leafs

    const nodeContents = {};
    keyOrders["node"].forEach((node: string) => nodeContents[node] = treeData[node].leafs);
    console.log(nodeContents);

    [zoom_region.cnvByNode, share.sizeByNode] = avgCNVByGroup(cnvByIDZoomed, keyOrders["node"], nodeContents, noneCase, true);
}

export function avgCNVByGroup(cnvByID, groupOdr: string[], groupContents: Record<string, string[]>, noneCase = [], fitSize: boolean = false, features?: string[]) {
    if (!features) features = Object.keys(cnvByID[Object.keys(cnvByID)[0]]);

    const cnvByGroup = {};
    const sizeByGroup = {};
    groupOdr.forEach((g: string) => {
        const cnv = {};
        features.forEach(f => {
            const values = groupContents[g].map(c => cnvByID[c][f]);
            const validValues = values.filter(v => !noneCase.includes(v));
            cnv[f] = validValues.length === 0 || validValues.length < values.length / 2
                ? "N/A"
                : arrAvg(validValues, 3);
        })
        cnvByGroup[g] = cnv;
        sizeByGroup[g] = groupContents[g].length;
    })

    return [cnvByGroup, sizeByGroup];
}

export function lastPage(d: any) {
    return d.genome_regions.length - 1;
}