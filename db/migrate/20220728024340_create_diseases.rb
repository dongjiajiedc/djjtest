class CreateDiseases < ActiveRecord::Migration[6.0]
  def change
    create_table :diseases do |t|
      t.string :disease_name
      t.string :disease_type
      t.string :data_source
      t.string :description
      t.timestamps
    end
  end
end
