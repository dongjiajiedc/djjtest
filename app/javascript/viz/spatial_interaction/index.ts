import Oviz from "crux";

import { editorConfig, editorRef } from "./editor";
import { SpatialInteraction } from "./spatial_interaction";
import * as d3 from "d3";
import { ComplexBoxplot, processBoxData } from "oviz-components/complex-boxplot";
import { ComplexScatterplot } from "oviz-components/complex-scatterplot";
import { EditText } from "oviz-components/edit-text";
import { GridPlot } from "oviz-components/grid-plot";
import { groupedChartColors } from "oviz-common/palette";
import { register } from "page/visualizers";
import { rankDict, sortByRankKey} from "utils/bio-info";
import DataUtils from "utils/data";
import { registerEditorConfig } from "utils/editor";
import { findBoundsForValues } from "utils/maths";

import { minmax } from "crux/dist/utils/math";
import { brewPalette, MetaInfo } from "viz/meta-overview/data";
import { StackedCruveArea } from "./stacked-curve-area";

// const ageDiv = 40;
const MODULE_NAME = "spatial_interaction";

export function init(id ,path1 ,path2,path3, eid ,plot_name) {

    // if (!window.gon || window.gon.module_name !== MODULE_NAME) return;

    Oviz.visualize({
        el: id,
        root: new SpatialInteraction(),
        components: {GridPlot, EditText, StackedCruveArea},
        renderer: "svg",
        width: 1300,
        height: 1300,
        data: {
            hiddenSamples: new Set(),
            gridSize: [4, 12],
            colors: {
                // control: controlGroupColors[0],
                // gout: controlGroupColors[1],
                na: "#999", //"#777",
                abd0: "#999", // "#333",
                // start: "#fff7f3",
                // end: "#0A2299",
                start: "#800000",
                org: "#FF5050",
                end: "#FFFF00",
            },
            drawTree: false,
        },
        loadData: {
            graphData: {
                fileKey: "graphData",
                url: path1,
                type: "csv",
                loaded() {}
            },
            spotMetaData: {
                fileKey: "spotMetaData",
                url: path2,
                optional: true,
                type: "csv",
                loaded() {}    
            },
            spatialImageData: {
                fileKey: "spatialImageData",
                url: path3,
                optional: true,
                type: "image",
                loaded() {}    
            }
        },
        setup() {
            console.log(this["_data"]);
            // registerEditorConfig(editorConfig(this),"getVue",plot_name, editorRef);
        },
    });

}



// register(MODULE_NAME, init);

// export function registerSpatialInteraction() {
//     // register(MODULE_NAME, init);
// }
