class SampleDatatable < ApplicationDatatable
  private

  def data
    samples.map do |sample|
      [].tap do |column|
        column << ""
        Sample.column_names.each do |attr|
          if attr != 'id'
            column << "<div class='table_cell'> #{sample[attr]} </div>"
          else
            column << sample[attr]
          end
        end
        column << "<button class='btn btn-1'> #{link_to('Show', "/samples/#{sample.id}")} </button>"
      end
    end
  end

  def count
    if(@obj==nil)
      Sample.count
      else
        if @obj == 'project'

          Sample.where(project_id: @config).count
        elsif @obj == 'cancer'

          Sample.where(cancer_name: @config).count

        elsif @obj == 'species' 

          Sample.where(species_name: @config).count
        elsif @obj == "disease"

          Sample.where(disease_name: @config).count
        end
      end        
  end

  def total_entries
    samples.total_count
    # will_paginate
    # users.total_entries
  end


  def samples
    fetch_samples
  end

  
  def fetch_samples
    search_string = []
    Sample.column_names.each do |attr|
      if Sample.columns_hash[attr].type != :string
        search_string << "cast(\"#{attr}\" as varchar(10)) like :search"
      else
        search_string << "\"#{attr}\" like :search"
      end
    end
    # search_col =['sample_name', 'project_name', 'experiment_type']
    # search_col.each do |term|
    #   search_string << "#{term} like :search"
    # end

    # will_paginate
    # users = User.page(page).per_page(per_page)
    samples = Sample.order("#{sort_column} #{sort_direction}")
    samples = samples.page(page).per(per_page)
    if params[:search][:value] != ""
      samples = samples.where(search_string.join(' or '), search: "%#{params[:search][:value]}%")
    else
      samples = samples.page(page).per(per_page)
    end
    #samples = samples.where(search_string.join(' or '), search: "%#{params[:search][:value]}%")

    if(@obj!=nil)
      # samples.where(project_id: @obj)

      if(@obj == 'project')
        samples.where(project_id: @config)

      elsif(@obj == 'cancer')

        samples.where(cancer_name: @config)

      elsif(@obj == 'species')

        samples.where(species_name: @config)
      elsif(@obj == 'disease')
        
        samples.where(disease_name: @config)
      end
    else
      samples
    end


  end

  def columns
    Sample.column_names
    # %w(first_name last_name email phone_number)
  end
end