import { Component, ComponentOption } from "crux/dist/element";
import { template } from "crux/dist/template/tag";
import * as d3Shape from "d3-shape"
import * as d3 from "d3"

interface MatrixOption extends ComponentOption{
}

export class Matrix extends Component<MatrixOption>{
	private getGridWidth(gridWidth, varied_size, data){
		if (varied_size == true)
			return gridWidth * Math.abs(data)
		return gridWidth
	}

	private getLink(sub_line, source_x, source_y, target_x, target_y){
		if (sub_line == "curve"){
			var linkGen = d3Shape.linkHorizontal()
								 .source(x => x.source)
								 .target(x => x.target)
			return linkGen({source: [source_x, source_y], target: [target_x, target_y]})
		}
		else{
			var lineGen = d3.line()([[source_x, source_y], [target_x, target_y]])
			return lineGen;
		}
	
	}

	private getColor(sub_display, data, scheme){
		if (sub_display == "color" || sub_display == "color + width")
			return scheme.get(data);
		return "grey";
	}

	private getWidth(sub_display, data){
		const width = 3
		if (sub_display == "width" || sub_display == "color + width")
			return (data +1)/2 * width
		return width * 0.5
	}

	public render = template` 
       	Component {
			rotation = @rotate(prop.general_angle)
			x = prop.padding_x
			y = prop.padding_y
			
			//matrix
			//----y axis
			Component{
				x =  prop.y_padding_x
				y =  prop.y_padding_y
				@for (d, i) in prop.result.instanceNames{ 
					//text on y axis
					Text {
						key = i + "t1"
						text = d
						y = (i + 0.5) * prop.gridWidth //y is distance from top
						anchor = @anchor(prop.y_dir, "middle")
						rotation = @rotate(prop.y_angle)
					}
				}
			}
			
			//----x axis
			Component{
				x = prop.x_padding_x
				y = prop.x_padding_y
				@for (d, i) in prop.result.instanceNames{ 
					Text {
						key = i + "t2"
						text = d
						x = (i + 0.5) * prop.gridWidth; 
						anchor = @anchor(prop.x_dir, "middle")
						rotation = @rotate(prop.x_angle)
					}		
				}
			}
			
			//----main blocks
			@for (d, i) in prop.displayedData {
				//main shape
				Component {
					x = (d.col + 0.5) * prop.gridWidth
					y = (d.row + 0.5) * prop.gridWidth
					
					//tooltip
					@let grid_width = getGridWidth(prop.gridWidth, prop.varied_size, d.data)
					@let fill_color = prop.colorScheme.scheme.get(d.data)
					@let name_x = prop.result.instanceNames[d.col]
					@let name_y = prop.result.instanceNames[d.row]
					@let d_str = d.data.toString()
					@if prop.shape == "square" {
						Rect.centered {
							height = grid_width
							width = grid_width
							stroke = "#d1d1e0"
							fill = fill_color
							behavior:tooltip {
								content = name_x + " , " + name_y + " : " + d_str + "<br>p value: " + d.p
							}
						}
					}
					@if prop.shape == "circle" {
						Circle.centered {
							r = 0.5 * grid_width
							stroke = "#d1d1e0"
							fill = fill_color
							behavior:tooltip {
								content = name_x + " , " + name_y + " : " + d_str
							}
						}
					}
					@if prop.shape == "triangle" {
						Triangle.centered {
							height = grid_width
							width = grid_width
							stroke = "#d1d1e0"
							fill = fill_color
							behavior:tooltip {
								content = name_x + " , " + name_y + " : " + d_str
							}
						}
					}
					
				}

				@if (prop.display_p_value && d.p){
					Component{
						x = d.col * prop.gridWidth
						y = d.row * prop.gridWidth
						@let margin = 3
						@if d.p >= 0.05 {
							Line {
								x1 = 0 + margin; y1 = 0 + margin; x2 = prop.gridWidth - margin; y2 = prop.gridWidth - margin
								stroke = "purple"
							}
							Line {
								x1 = prop.gridWidth - margin; y1 = 0 + margin; x2 = 0 + margin; y2 = prop.gridWidth - margin
								stroke = "purple"
							}
						}
					}
				}
			}

			//sub-matrix
			@if (prop.display_submatrix & prop.sub_result) {
				//----labels
				@for (d, i) in prop.sub_result.submatrix_names{
					Component{
						x =  0.25 * prop.graphSize + (i + 0.5) * prop.sub_gridWidth
						y = -0.25 * prop.graphSize + (i + 0.5) * prop.sub_gridWidth
						//TODO: adjust the position of labels
						Text {
							x = 10
							text = d
							anchor = @anchor(prop.sub_dir, "middle")
							rotation = @rotate(prop.y_angle)
						}		
					}
				}

				//----links
				@for (d, i) in prop.sub_displayed_data{
					//source nodes
					@let source_x = 0.25 * prop.graphSize + (d.col + 0.5) * prop.sub_gridWidth
					@let source_y = -0.25 * prop.graphSize + (d.col + 0.5) * prop.sub_gridWidth
					Component{
						x = source_x
						y = source_y
						Circle.centered {
							r = 2.5
							stroke = "#d1d1e0"
							fill = "purple"
						}
					}

					//target nodes
					@let target_x = (d.row + 0.5) * prop.gridWidth
					@let target_y = (d.row + 0.5) * prop.gridWidth
					Component{
						x = target_x
						y = target_y
						Circle.centered {
							r = 2.5
							stroke = "#d1d1e0"
							fill = "purple"
						}
					}

					//lines
					@let matrix_name = prop.result.instanceNames[d.row]
					@let submatrix_name = prop.sub_result.submatrix_names[d.col]
					@let d_str = d.data.toString()
					@expr path =  getLink(prop.sub_line, source_x, source_y, target_x, target_y)
					Component{
						Path{
							d = path
							stroke = getColor(prop.sub_display, d.data, prop.colorScheme.scheme)
							strokeWidth = getWidth(prop.sub_display, d.data)
							strokeOpacity = 0.5
							fill = 'none'
							behavior:tooltip {
								content = submatrix_name + " , " + matrix_name + " : " + d_str
							}
						}
					}
				}
			}
		}
	` ;
}

