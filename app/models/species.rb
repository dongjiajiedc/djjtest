class Species < ApplicationRecord
    validates :species_name, presence: true, uniqueness: { 
      message: ->(object, data) do
        "Species #{data[:value]} already exists. "
      end
    }, on: :create
  
    def self.import(file)
        CSV.foreach(file.path, headers: true, encoding: 'bom|utf-8') do |row|
        species = Species.find_by(species_name: row['species_name']) || new
        species.attributes = row.to_hash.slice(*column_names)

        species.save!
        end
    end

end
