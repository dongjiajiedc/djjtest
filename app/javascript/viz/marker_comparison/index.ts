import Oviz from "crux";

import { editorConfig, editorRef } from "./editor";
import { MarkerComparison } from "./marker_comparison";

import { ComplexBoxplot, processBoxData } from "oviz-components/complex-boxplot";
import { ComplexScatterplot } from "oviz-components/complex-scatterplot";
import { EditText } from "oviz-components/edit-text";
import { GridPlot } from "oviz-components/grid-plot";
import { groupedChartColors } from "oviz-common/palette";
import { register } from "page/visualizers";
import { rankDict, sortByRankKey} from "utils/bio-info";
import DataUtils from "utils/data";
import { registerEditorConfig } from "utils/editor";
import { findBoundsForValues } from "utils/maths";

import { minmax } from "crux/dist/utils/math";
import { brewPalette, MetaInfo } from "viz/meta-overview/data";


const xAxisIndex = 0;
const yAxisIndex = 1;
const startColor = "blue";
const endColor = "red";

// const ageDiv = 40;
const shapes = ["Circle", "Triangle", "Rect"];
const MODULE_NAME = "marker_comparison";

function init() {

    if (!window.gon || window.gon.module_name !== MODULE_NAME) return;

    const {visualizer} = Oviz.visualize({
        el: "#canvas",
        root: new MarkerComparison(),
        components: {GridPlot, EditText},
        renderer: "svg",
        width: 1300,
        height: 1300,
        data: {
            hiddenSamples: new Set(),
            gridSize: [4, 12],
            colors: {
                // control: controlGroupColors[0],
                // gout: controlGroupColors[1],
                na: "#999", //"#777",
                abd0: "#999", // "#333",
                // start: "#fff7f3",
                // end: "#0A2299",
                start: "#800000",
                org: "#FF5050",
                end: "#FFFF00",
            },
            drawTree: false,
        },
        loadData: {
            markerData: {
                fileKey: "markerData",
                type: "csv",
                loaded() {}

            },
            expressionData: {
                fileKey: "expressionData",
                type: "csv",
                loaded() {}    
            },
            spotMetaData: {
                fileKey: "spotMetaData",
                type: "csv",
                loaded() {}    
            },
            spatialImageData: {
                fileKey: "spatialImageData",
                type: "image",
                loaded() {}    
            }
        },
        setup() {
            console.log(this["_data"]);
            //registerEditorConfig(editorConfig(this), editorRef);
        },
    });

    return visualizer;
}



register(MODULE_NAME, init);

export function registerMarkerComparison() {
    register(MODULE_NAME, init);
}
