import { EditorDef } from "utils/editor";
import { process } from "./data";
//import Vue from "vue";
import Vue from 'vue/dist/vue.esm.js'
import { editorSectionFactory, editorItemFactory, warpIf, updateColorScheme } from "../../spatial-common/editor";

//options for selection
const conf: any = {
    types:[]
};

export function setTypes(t:Object){  conf.types = t};

//re-render
function update(v) {
    v.forceRedraw = true;   
    v.run();
}


function updateClass(v, selections) { 
    v.data.share.classes = selections;
   console.log("classes in editor", v.data.share.classes);
    process(v.data.metaData);
    update(v);
}

function changeMode(v) { 
    switch(conf.hexMode) {
        case "hexagon mode":  v.data.share.hexMode = true;break;
        case "scatter mode" : v.data.share.hexMode = false; break;
    }
    process(v.data.metaData);
    update(v);
}

function setRadius(v) { 
    if (!v.data.share.hexMode) {
        window.alert("You can set width of hexagon only in Hexagon Mode!");
        return;
    }
    v.data.share.hexaWidth = parseFloat(conf.r);
    if(v.data.share.hexaWidth>15) v.data.share.hexaWidth=15;
    v.data.share.radius = parseFloat(conf.r)/Math.sqrt(3)*2;
    process(v.data.metaData);
    update(v);
}

function changeColor(v){
   if(v.data.share.hexMode){
    switch(conf.color){
        case "mean": v.data.share.getMean = true;break;
        case "median": v.data.share.getMean = false;break;
    }
    process(v.data.metaData);
    update(v);
   }
}

function selectMethod(v, selections){
    console.log("selections in editor", selections);

    v.data.share.coorKeys = selections;
    console.log("coorkeys in editor", v.data.share.coorKeys);
    process(v.data.metaData);
    update(v);
 }

function setGene(v,  gene: string){
    var idx = 0;
    v.data.share.geneKey.forEach((g,i) => {
        if(g.includes(gene)) idx=i;
    });
    v.data.share.geneIdx = idx;
    console.log("geneIdx",  v.data.share.geneIdx);
    process(v.data.metaData);
    update(conf.v);
}

function updateHeight(v){
    v.data.share.height = parseInt(conf.height);
    process(v.data.metaData);
    update(v);
}

function updateWidth(v){
    v.data.share.width = parseInt(conf.width);
    process(v.data.metaData);
    update(v);
}

export function editorConfig(v: any): EditorDef{ //v: this webpage
    let catKeys = v.data.share.classes.filter(c=>!c.startsWith("g_")&&!c.startsWith("c_"));

    conf.v = v;
    conf.hexMode = "hexagon mode", conf.choice = ["hexagon mode", "scatter mode"];
    conf.r = v.data.share.hexaWidth;
    conf.pointR = v.data.share.pointRadius;
    conf.color = "mean";
    conf.classes =  v.data.share.classes.filter(k=> !k.startsWith("g_")).map(c=>{
        if(c.startsWith("c_")) return c.slice(2,);
        return c;
    });
    if(v.data.share.meta != null) conf.genes = v.data.share.geneKey//.map(g=>{return g.slice(2,);});
    //if(v.data.share.hexMode) classes.push("Density");

    var coorKeys = v.data.share.coorKeys, dataKeys = [];
    for(var i=0;i<coorKeys.length;i = i+2)  dataKeys.push(coorKeys[i].slice(2,-1));
    conf.methods = dataKeys;
    conf.height = parseInt(v.data.share.height), conf.width = parseInt(v.data.share.width);
    return {
        sections: [
            {
                id: "general",
                title: "General Settings",
                layout: "single-page",
                view: {
                    type: "list",
                    items: [
                        {
                            title:"Select embedding methods",
                            type: "vue",
                            component: methodBox,
                        },
                        {
                            title: "Hexagon mode",
                            type: "select",
                            options: conf.choice,
                            bind: {
                                object: conf, path: "hexMode",
                                callback: () => changeMode(v), 
                            },
                        }, 
                        {
                            title: "Set width of hexagon bin",
                            type: "input",
                            bind: {
                                object: conf, path: "r",
                                callback: () => setRadius(v), 
                            },
                        }, 
                        {
                            title: "Hexagon bin averaging scheme",
                            type: "select",
                            options:  ["mean", "median"],
                            bind: {
                                object: conf, path: "color",
                                callback: () => changeColor(v), 
                            },
                        }, 
                        {
                            title: "Radius of scatter point",
                            type: "input",
                            bind: {
                                object: conf, path: "pointR",
                                callback: () => {
                                    v.data.share.pointRadius = parseFloat(conf.pointR);
                                    update(v);
                                }, 
                            },
                        }, 
                        {
                            title: "Search for gene",
                            type: "vue",
                            component: v.data.share.meta? searchBox: null,
                            // bind: {
                            //     object: conf, path: "dataKey",
                            //     callback: () => selectMethod(v), 
                            // },
                        }, 
                        {
                            title: "Embedding plot height",
                            type: "input",
                            bind: {
                                object: conf, path: "height",
                                callback: () => updateHeight(v),
                            },
                        }, 
                        {
                            title: "Embedding plot width",
                            type: "input",
                            bind: {
                                object: conf, path: "width",
                                callback: () => updateWidth(v),
                            },
                        },
                    ],
                },
            },
            {
                id: "filter",
                title: "Select categorical meta label",
                layout: "single-page",
                view: {
                    type: "list",
                    items: [
                        {
                            title: "Colored by",
                            type: "vue",
                            component: checkBox,
                            // bind: {
                            //     object: conf, path: "class",
                            //     callback: () => updateClass(v), //update classification upon new type is selected
                            // },
                        }, 
                    ],
                },
            },
            ...warpIf(v.data.optFiles.meta, [
                editorSectionFactory(v, "Color Palettes", "single-page", [
                    editorItemFactory(v, "Customize continuous meta colors", "color-picker", v.data.share.colors, "cont", (curr) => curr, updateColorScheme),
                    ...catKeys.map(mk => editorItemFactory(v, `Customize ${mk} colors`, "color-picker", v.data.share.colors, mk, (curr) => curr, updateColorScheme)),
                ]),
            ]),
        ],
    };
}

var checkBox = Vue.component('my-checkbox', {
    data: function() {
        return { 
            selections: conf.classes,
            selected: conf.classes,
        }
    },
    computed: {
        getSelection: function(){
        }
    },
    methods: {
        submitSelection: function(){
            console.log("selected", this.selected);
            updateClass(conf.v, this.selected);
        },
     },
    template: `
    <div>
        <p title="Annotation">Colored by</p>
        <tr v-for="(item,index) in selections" >
          <td>
             <input type="checkbox" :id="index" :value="item" v-model="selected" @change="submitSelection">
            </td>
            <td>{{ item }}</td>
        </tr>  
    </div>`
  })

  var methodBox = Vue.component('select-method', {
    data: function() {
        return { 
            selections: conf.methods,
            selected: conf.methods,
        }
    },
    computed: {
        getMethod: function(){
        }
    },
    methods: {
        submitMethod: function(){
            selectMethod(conf.v, this.selected);
        },
     },
    template: `
    <div>
        <p title="Mecthod Selection">Select embedding methods</p>
        <tr v-for="(item,index) in selections" >
          <td>
             <input type="checkbox" :id="index" :value="item" v-model="selected" @change="submitMethod">
            </td>
            <td>{{ item }}</td>
        </tr>  
    </div>`
  })

var searchBox = Vue.component('gene-search', {
    data: function() {
        return { 
            genes: conf.genes,
            numbers: [ 1, 2, 3, 4, 5 ],
            input: "",
            selected: [],
        }
    },
    computed: {
        searchGene: function(){
            var str = this.input, strLow = str.toLowerCase(), strUp = str.toUpperCase();
            //if(str=="") return [];
            return this.genes.filter(g=>g.includes(str)||g.includes(strLow)||g.includes(strUp));
        }
    },
    methods: {
        submitGene: function(){
            setGene(conf.v,this.input);
        },
        selectGene: function(){
            this.input = this.selected[0];
            this.submitGene();
        }
     },
    template: `
    <div>
        <p title="Gene_Search">Search for gene</p>
        <form v-on:submit.prevent="submitGene">
            <label for="new-todo"></label>
            <input
                v-model="input"
                id="new-todo"
                placeholder="E.g. ABCA3"
            >
            <button>Apply</button>
            <select v-model="selected" multiple style="width: 150px;" v-on:click.prevent="selectGene">
                <option v-for="n in searchGene">{{ n }}</option>
            </select>
        </form>
    </div>`
  })