import * as cv from "color-convert";
import Crux from "crux";
import * as d3 from "d3";
import * as _ from "lodash";

import { GeneRawData, toGeneData } from "crux/dist/utils/bioinfo/gene";
import { Visualizer } from "crux/dist/visualizer/visualizer";
import { Gene, GenomeRegion } from "./gb_defs";
import { editorOpt, editorRef, parseGenomeRegion } from "./editor";

const pages: any[] = [];

export const dataOptions = {
    unifySamples: false,
    // sampleInterval: 10,
    displayLeftouts: false,
    displayAllGenes: false,
};

export function genome_regionsLoaded_mediator(this: any, genome_regions: GenomeRegion[]) {
    if (!this.data.hasOwnProperty("optFiles")) this.data.optFiles = {};
    // this.data.optFiles.bed = true;
    this.data.optFiles.bed = !(!genome_regions);

    if (!genome_regions) genome_regions = [];

    // Default
    // TODO: no valid checking here, take care to set the default value
    console.log("Default genome region: ", editorOpt.searchRegion)
    const r = parseGenomeRegion(editorOpt.searchRegion);
    console.log(r);
    genome_regions.push({
        chrom: r[0],
        start: r[1],
        end: r[2],
    })
    genome_regionsLoaded(genome_regions);
    return genome_regions;
}

// export function genome_regionsLoaded_mediator(this: any, genome_regions: GenomeRegion[]) {
//     if (!this.data.hasOwnProperty("optFiles")) this.data.optFiles = {};
//     this.data.optFiles.bed = !(!genome_regions);
//     if (genome_regions) {
//         this.data.optFiles.bed = true;
//         genome_regionsLoaded(genome_regions);
//     }
// }

export function genome_regionsLoaded(genome_regions: GenomeRegion[]) {
    // const gap = dataOptions.sampleInterval;
    // const displayLeftouts = dataOptions.displayLeftouts;

    pages.length = 0;
    genome_regions.forEach((gr, index) => {
        pages.push({ value: index, text: `(${index + 1}) ${gr.chrom}:${gr.start}-${gr.end}` });
    });
    console.log(genome_regions)
    return genome_regions;
}

export function genesLoaded(data: any[]) {
    data.forEach(({ data }, i) => {
        const hp = this.data.genome_regions[i];
        const genes = (data as GeneRawData[]).map(toGeneData);
        hp.genes = Object.values(_.groupBy(genes, "gene_name")).map(x => new Gene(x));
        const colorInterval = Math.PI * 2 / hp.genes.length;
        const r = 72;
        hp.genes.forEach((g, i) => {
            const deg = colorInterval * i;
            const a = Math.cos(deg) * r;
            const b = Math.sin(deg) * r;
            g.setColor(`rgb(${cv.lab.rgb([75, a, b])})`);
        });
        // if (genes.length > 0) {
        //     hp.start = Math.min(hp.start, d3.min(genes, x => x.most_left_pos));
        //     hp.end = Math.max(hp.end, d3.max(genes, x => x.most_right_pos));
        // }
    });
}

function sortChr(a: string, b: string): number {
    return parseInt(a.substr(3)) - parseInt(b.substr(3));
}

// WIP
const rmskColor = Crux.color.ColorSchemeCategory.create(
    ["DNA", "LINE", "Low_complexity", "LTR", "RC", "Satellite", "Simple_repeat", "SINE"],
    d3.schemeSet3 as string[],
);

export function processGenomeRegions(hp: GenomeRegion, isInitial = false) {
    // rmsks
    if (editorOpt.showRepeatTrack === true) {
        hp.rmsks.forEach(rmsk => {
            rmsk.start_x = hp.scale(rmsk.start_pos);
            rmsk.end_x = hp.scale(rmsk.end_pos);
            if (isInitial) {
                rmsk.color = rmskColor.get(rmsk.repeat_class);
                rmsk.start_x0 = rmsk.start_x; rmsk.end_x0 = rmsk.end_x;
            }
        });
        if (isInitial) {
            const layout = Crux.algo.stackedLayout(hp.rmsks)
                .value(x => x.start_x)
                .extent(x => [x.start_x - 0.5, x.end_x + 0.5]);
            hp.rmskLayers = layout.run();
        }
    }
    // merged genes
    updateGenes(hp);
}

export function updateGenes(hp: GenomeRegion) {
    selectGenes(hp);
    mergeGenes(hp);
}

function selectGenes(hp: GenomeRegion) {
    const selected = hp.genes.map(x => x.selected).filter(x => x);
    [hp.genesPos, hp.genesNeg] = _.partition(selected, x => x.strand === "+");
}

export function mergeGenesUpdate(hp: GenomeRegion) {
    [hp.genesNeg, hp.genesPos].forEach(genes => {
        genes.forEach(g => {
            g.start_x = hp.scale(g.most_left_pos);
            g.end_x = hp.scale(g.most_right_pos);
            // if (typeof g.start_x0 !== "number") {
            g.start_x0 = g.start_x; g.end_x0 = g.end_x;
            // }
        });
    });
}

function mergeGenes(hp: GenomeRegion) {
    [hp.genesNeg, hp.genesPos].forEach(genes => {
        genes.forEach(g => {
            g.start_x = hp.scale(g.most_left_pos);
            g.end_x = hp.scale(g.most_right_pos);
            if (typeof g.start_x0 !== "number") {
                g.start_x0 = g.start_x; g.end_x0 = g.end_x;
            }
        });
    });
}

export function updateDisplayedStats(v: any) {
    const hp: GenomeRegion = v.data.genome_region;
    if (!hp) { return; }
    const [start, end] = hp.scale.domain();
    //const spots = hp.displayedSpots.filter(x => x.pos >= start && x.pos <= end);
    //v.data.layout.displayedSampleCount = new Set(spots.map(x => x.sampleID)).size;
    //v.data.layout.displayedSpotCount = spots.length;
}

export function navigate(v: Visualizer, page: number, initial = false) {
    const d = v.data;
    if (page < 0 || page >= d.genome_regions.length) return false;

    const hp: GenomeRegion = d.genome_regions[page];
    hp.genes.forEach(g => g.select(null, !dataOptions.displayAllGenes));
    selectGenes(hp);

    if (initial) {
        d.genome_regions.forEach((hp: GenomeRegion) => {
            hp.scale = d3.scaleLinear()
                .domain([hp.start, hp.end])
                .range([0, v.size.width - 120]);
            hp.thumbScale = hp.scale.copy();
            processGenomeRegions(hp, true);
        });
    } else {
        mergeGenes(hp);
    }

    d.genome_region = hp;
    // d.page = page;
    updateDisplayedStats(v);

    ["genome_regionTable", "geneTable", "leftoutTable"].forEach(updateEditorTable);

    editorOpt.page = page;
    updateEditorItem("page");
    return true;
}

export function updateEditorItem(ref: string) {
    if (editorRef[ref]) editorRef[ref].update();
}

function updateEditorTable(ref: string) {
    if (editorRef[ref]) editorRef[ref].updateData();
}
