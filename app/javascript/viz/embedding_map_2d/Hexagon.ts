import { Component, ComponentOption } from "crux/dist/element";
import { template } from "crux/dist/template/tag";

interface HexagonOption extends ComponentOption{
  height: number
  width: number
  xMax: number
  yMax: number
  xMin: number
  yMin: number
  data: any
  radius: number
}

export class Hexagon extends Component<HexagonOption>{
    public render = template` 
    Component{
      width = prop.width
      XYPlot {
        width = prop.width; height = prop.height; 	
        x = 0.5*prop.width>200? 0.5*prop.width : 200
        y = 0.3*prop.height
       
        //set ranges for axis
        @let dataset = prop.hexMode?prop.data : prop.points
        categoryRange = [prop.xMin,prop.xMax]
        valueRange = [prop.yMin,prop.yMax]
        hasPadding = false
        data = dataset
       
        // AxisBackground {}
        // AxisBackground {orientation = "vertical"}
        Axis {color = "#000";orientation = "bottom"; y = 100%; includeEndTicks = false}
        Axis {color="#000";orientation = "left"; includeEndTicks = false}
 

        @if (prop.hexMode == false) {
          @for (d, i) in prop.points {
            Circle.centered {
              //key = i
              x = @scaledX(d[0]); y = @scaledY(d[1])
              r = prop.pointRadius; fill = prop.isGene==true?d[2]:prop.pointColors[prop.title][d[4]]
              stroke= prop.isGene==true?"gray":"none"
               behavior:tooltip {
                 content = "PC1: " + d[0].toFixed(3) + "<br>PC2: " + d[1].toFixed(3) + "<br>" + prop.title + ": " + d[4] + "<br>Cell ID: " + d[3];
                 //content = d[3] + ", "+ d[4] + "<br>[PC1 , PC2]: [" + d[0].toFixed(3)+ " , " + d[1].toFixed(3) + "]";
                }
            }
          }
        }
        @else{
          @expr xUnit = (prop.xMax-prop.xMin)/prop.width;
          @expr yUnit = (prop.yMax-prop.yMin)/prop.height;
          @for (d, i) in prop.data{
            //filter hexagon exceeding aix
            //@if d.x-prop.hexaWidth*xUnit>prop.xMin&&d.y-prop.radius*yUnit>prop.yMin&&d.x+prop.hexaWidth*xUnit<prop.xMax&&d.y+prop.radius*yUnit<prop.yMax{
              Path{
                x = @scaledX(d.x); y = @scaledY(d.y)
                //@expr console.log("x y", d.x, d.y);
                d= getHex(prop.radius)// "M0 0 m -10 -10 h 20 v 20 h -20 z "
                stroke="gray"
                style:stroke-width=2
                style:stroke-dasharray="4,2"
                fill= getHexColor(prop, d)
                behavior:tooltip {
                  content = getHexInfo(prop, d);
                 }
              }
            //}
          
          }
        }
        Text {
          text = prop.xLabel
          x = 50%; y = @geo(100, 40)
          anchor = @anchor("top", "center")
          fill = "#000"
        }
        Component {
          x = -20; y = 50%
          rotation = @rotate(-90)
          Text {
            text = prop.yLabel
            anchor = @anchor("bottom", "center")
            fill = "#000"
          }
        }
      }
      @if prop.drawLegend{
        Text {
          anchor = @anchor("top", "center")
          x = 100; y = 15%
          text = prop.getDensity?"Density" : prop.title
          style:font-style= prop.getItalic? 'italic':""
          fontSize = 18
          fill = "#000"
        }
        @if !prop.continuousValue && !prop.getDensity && !prop.isGene{
          Legend {
            anchor = @anchor("top", "center")
            x = 100; y = 30%
            data = getLegendData(prop.title)
            bg.fill = prop.legendColor
            label.fill = prop.textColor
            bg.stroke = "none"
            padding = 8
            lineHeight = 12
          }
        }
      }
    }
`;

  //get a hexagon shape
  private getHex(radius:number) {
    var x0 = 0, y0 = 0;
    var thirdPi = Math.PI / 3,
    angles = [0, thirdPi, 2 * thirdPi, 3 * thirdPi, 4 * thirdPi, 5 * thirdPi];
    var hexa = angles.map(function(angle) {
      var x1 = Math.sin(angle) * radius,
          y1 = -Math.cos(angle) * radius,
          dx = x1 - x0,
          dy = y1 - y0;
      x0 = x1, y0 = y1;
      return [dx, dy];
    });
    return "m" + hexa.join("l") + "z";
  }

  //get hexagon tooltip information
  private getHexInfo(prop, d){
    let xUnit = (prop.xMax-prop.xMin)/prop.width, yUnit = (prop.yMax-prop.yMin)/prop.height;
    let substr = prop.title, IDs = [];
    if (prop.continuousValue) substr += prop.getMean? "(Mean)" : "(Median)";
    substr += ": " + d.value +"<br>";
    let str = "";//"Center: ("+d.x.toFixed(4)+", "+d.y.toFixed(4)+")<br>"
    str += "PC1: ("+(d.x-prop.hexaWidth*xUnit).toFixed(4)+ ", "+(d.x+prop.hexaWidth*xUnit).toFixed(4)+")<br>";
    str += "PC2: ("+(d.y-prop.radius*yUnit).toFixed(4)+ ", "+(d.y+prop.radius*yUnit).toFixed(4)+")<br>";
    str += prop.getDensity?"Densiy: "+ d.length + "<br>" : substr;
    str += "Cell number: " + d.length + "<br>"+"Cell IDs:<br>";
    for(let i=0;i<d.length;i = i+1) IDs.push(d[i][3]);
    for(let i=0;i<d.length&&i<=10;i = i+1) str += IDs[i]+"<br>";
    if (d.length>10) str += "...";
    return str;
  }

  private getHexColor(prop, d){
    if(prop.getDensity) return prop.densityColors.get(d.length);
    else if(prop.isGene) return prop.color.get(d.value);
    else if(prop.continuousValue) return d.color;
    else return prop.pointColors[prop.title][d.value];
  }

  // generate data for legend
  private getLegendData(k){
    console.log('legend start');
    console.log(k);
    const colors = this.$v.data.share.colors[k];
    const keys = Object.keys(colors);
    console.log('legend fin');
    return keys.map((key) => {
        return { label: key, fill: colors[key], type: 'Rect' };
    });
  }

}
