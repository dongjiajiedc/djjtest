import Oviz from "crux";
import { register } from "page/visualizers";
import { registerEditorConfig } from "utils/editor";
import { editorConfig, editorRef } from "./editor";
import { BarView } from "./bar_view";

const MODULE_NAME = "spatial_deconvolution_bar_view";

function init() {
    if ( !window.gon || window.gon.module_name !== MODULE_NAME) return;
    const {visualizer} = Oviz.visualize({
        el: "#canvas",
        renderer: "svg",
        width: 1500,
        height: 1200,
        root: new BarView(),
        theme: "light",
        data: {
            hiddenSamples: new Set(),
            gridSize: [4, 12],
            colors: {
                // control: controlGroupColors[0],
                // gout: controlGroupColors[1],
                na: "#999", //"#777",
                abd0: "#999", // "#333",
                // start: "#fff7f3",
                // end: "#0A2299",
                start: "#800000",
                org: "#FF5050",
                end: "#FFFF00",
            },
            drawTree: false,
        },
        loadData: {
            spotDeconvData: {
                fileKey: "spotDeconvData",
                type: "csv",
                loaded(d){
                    return d;
                },
            },
            spotMetaData: {
                fileKey: "spotMetaData",
                type: "csv",
                loaded(d){
                    return d;
                },
            },
            spatialImageData: {
                fileKey: "spatialImageData",
                type: 'image',
                loaded(d){
                    return d;
                }
            },
        },
        setup() {
            //registerEditorConfig(editorConfig(this), editorRef);
        },
    });
    return visualizer;
}


register(MODULE_NAME, init);
export function registerSpatialDeconvolutionBarView(){
    register(MODULE_NAME, init);
}