import Oviz from "crux";
// import { editorConfig, editorRef } from "./editor";
import template from "./template.bvt";

import { groupedChartColors} from "oviz-common/palette";
import { LegendComp } from "../../spatial-common/legendcomp"
import {register} from "page/visualizers";
import { ColorSchemeNumeric } from "../../spatial-common/defs";
import { rankDict, sortByRankKey } from "utils/bio-info";
import { registerEditorConfig } from "utils/editor";
import { editorConfig } from "./editor"

import * as math from "mathjs";
import { XYZPlot } from "./xyz-plot";

const yLabel = "Beta diversity";

const MODULE_NAME = "embedding_map_3d";

function init() {
    if (!window.gon || window.gon.module_name !== MODULE_NAME) return;

    const {visualizer} = Oviz.visualize({
        el: "#canvas",
        template: `svg {
            height = 1200; width = 1000
            @expr console.log("data in template", data);
            Component {
                XYZPlot {
                    x = 350; y = 200;
                    data = data
                    xRange = xRange
                    yRange = yRange
                    zRange = zRange
                    viewAngle = -45
                }
                LegendComp {
                    x = 200; y = 200
                    data = colorScheme.legendData(false, true, true)
                    bg.fill =  'white' 
                    bg.stroke = "none"
                    label.fill = "#000"
                    padding = 8
                    lineHeight = 12
                    innerDistance = 0
                }
                // Circle.centered {
                //     x = 200; y = 200;
                //     fill = "red"
                // }
            }
            
        }`,
        components: { XYZPlot, LegendComp },
        data: {
            config: {                
                colorIdx: 4,
            },
            colors: { box: groupedChartColors[0], scatter: "pink",
                violin: "LightSteelBlue"},
        },
        loadData: {
            expressionData: {
                fileKey: "expressionData",
                type: "csv",
                loaded(data) {
                    //color data
                    console.log("expression data", data);
                    data.forEach(d => {
                        delete d["Unnamed: 0"];
                    });
                    return data;
                },
            },
            metaSpotData: {
                fileKey: "metaSpotData",
                type: "csv",
                loaded(data) {
                    //3D coordinates
                    console.log("meta data", data);
                    let points = data.map(d => [d["e_PC_1"], d["e_PC_2"], d["e_PC_3"]]);
                    let xPos = [], yPos = [], zPos = [];
                    points.forEach(p => {
                        xPos.push(p[0]);
                        yPos.push(p[1]);
                        zPos.push(p[2]);
                    });
                    this.data.xRange = [Math.min(...xPos), Math.max(...xPos)];
                    this.data.yRange = [Math.min(...yPos), Math.max(...yPos)];
                    this.data.zRange = [Math.min(...zPos), Math.max(...zPos)];
                    return points;
                },
            },
            markerData: {
                fileKey: "markerData",
                type: "csv",
                loaded() {}

            },
            spatialImageData: {
                fileKey: "spatialImageData",
                type: "image",
                loaded() {}    
            },
        },
        setup() {
            // const data = [];
            // for (let i =0; i< 1000; i++) {
            //     const vector = [1,1,1].map(_ => Math.floor(Math.random() * 101));
            //     data.push({color: i%3 ===2 ? "green" : i%3 ===1 ? "orange" : "red", vector});
            // }
            processData(this.data);
            registerEditorConfig(editorConfig(this));
        },
    });

    return visualizer;
}

export function registerEmbeddingMap3D() {
    register(MODULE_NAME, init);
}

export function processData(data) {
    console.log("this.data", data);
    //cerate color scheme
    let colorData = [];
    data.colorKeys = Object.keys(data.expressionData[0]);
    const colorKey = data.colorKeys[data.config.colorIdx];
    data.expressionData.forEach(d=>{
        colorData.push(d[colorKey]);
    });
    const min_color = "#B3E5FC";
    const middle_color = "#29B6F6"
    const max_color = "#01579B";
    data.colorScheme = new ColorSchemeNumeric([min_color, middle_color, max_color], 
        { type: "linear", domain: [0,Math.max(...colorData)/2, Math.max(...colorData)]});
    //construct the output array
    let res = [];    
    data.metaSpotData.forEach((d,i)=>{
        // let temp = {color: colorScheme.get(colorData[i]), vector:d};
        res.push({color: data.colorScheme.get(colorData[i]), vector:d});
    });
    data.data = res;
}

register(MODULE_NAME, init);
