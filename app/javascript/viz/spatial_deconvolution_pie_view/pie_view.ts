import Oviz from "crux";
import template from "./template.bvt";
import { SpatialImage } from "spatial-common/spatial_image";
import { processMeta, processPie, processMain, processHist} from "spatial-common/spatial_data";
import {event} from "crux/dist/utils"

export class PieView extends Oviz.Component {
    
    public histLegendPos = {x: 90, y: 280};
    public spotDeconvData; filteredSpots;
    public spatialImageConfig = {
        imageX: 20, imageY: 20,
        inspectWidth: 600, inspectPosition: 'right'
    }
    public spatialImageConfig2 = {
        imageX: 20, imageY: 20,
        inspectWidth: 600, inspectPosition: 'bottom'
    }

    public static components = { SpatialImage};

    public render() {
        
        return this.t`${template}`;
    }

    public willRender() {
        processMeta(this)
    }


}
