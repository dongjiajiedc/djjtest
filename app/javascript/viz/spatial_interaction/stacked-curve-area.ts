import { template } from "crux/dist/template/tag";
import { BaseChart, BaseChartOption } from "crux/dist/element/chart/base-chart";
import { inheritData, StackedChart } from "crux/dist/element/chart/utils/stacked";
import * as d3 from "d3";

export interface StackedCurveAreaOption extends BaseChartOption {
    barWidth: number;
    curveType: string;
}

export class StackedCruveArea extends BaseChart<StackedCurveAreaOption> implements StackedChart {
    public render = template`
    Component {
        @for key in dataKeys {
            Component {
                key = key
                @let path = getPath(key)
                @let d = { path: path, key: key }
                @yield children with d default {
                    Path { d = path; fill = "#aaa" }
                }
            }
        }
    }
    `;

    public data!: Record<string, any>;
    public dataKeys!: string[];
    public dataPos!: any[];
    
    // @ts-ignore
    private getPath(key: string) {
        var barWidth = this.prop.barWidth || 0;
        const data = this.dataPos.reduce((array, p) => {
            const d = this.data[p][key];
            let point1 = [
                this._scale(d.pos, true) - barWidth/2,
                this._scale(d.minValue + d.value, false),
                this._scale(d.minValue, false),
            ] as [number, number, number];
            array.push(point1);
            if (barWidth != 0){
                let point2 = [
                    this._scale(d.pos, true) + barWidth/2,
                    this._scale(d.minValue + d.value, false),
                    this._scale(d.minValue, false),
                ] as [number, number, number];
                array.push(point2);
            }
            return array;
        }, []);

        var curveType = this.prop.curveType || 'curve';
        var curveFactory;
        if (curveType == 'curve'){
            curveFactory = d3.curveMonotoneX;
        }else{ // 'linear'
            curveFactory = d3.curveLinear;
        }
        

        let path = d3.area()
        .x((d) => d[0])
        .y0((d) => d[1])
        .y1((d) => d[2])
        .curve(curveFactory)(data)

        return path;
    }

    protected inheritData() {
        inheritData.call(this);
    }
}
