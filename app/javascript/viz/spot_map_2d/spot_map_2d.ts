import Oviz from "crux";
import template from "./template.bvt";
import { processMeta, processEmbedding } from "spatial-common/spatial_data";
import { event } from "crux/dist/utils";
import { SpatialImage } from "spatial-common/spatial_image_edited";

export class SpotMap2D extends Oviz.Component {

    public spatialImageConfig = {
        imageX: 20, imageY: 20, imageWidth: 400,
        inspectWidth: 400, spotR: 2,
        windowWidth: 60, windowHeight: 60,
        inspectPosition: 'bottom',
        annoType: 'c_seurat_clusters'
    }

    public filteredSpots;

    public static components = { SpatialImage};
    public render() {
        return this.t`${template}`;
    }

    public didCreate(){
        event.on("activeSpots", (_, spots) => {
            //console.log('receve spots', spots);
            this.filteredSpots = spots;
            //this.$v.forceRedraw = true;
            this.redraw();
        })       
    }

    public willRender() {
        if (this._firstRender) {
            processMeta(this)
            processEmbedding(this)
        }
    }  
}
