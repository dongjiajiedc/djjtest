class SliceDatatable < ApplicationDatatable
    private
  
    def data
        slices.map do |slice|
        [].tap do |column|
          column << ""
          Slice.column_names.each do |attr|
            if attr != 'id'
              column << "<div class='table_cell'> #{slice[attr]} </div>"
            else
              column << slice[attr]
            end
          end
            column << "<button class='btn btn-1'> #{link_to('Show', slice)} </button>"

        end
      end
    end
  
    def count
      if(@obj==nil)
        Slice.count
        else
        Slice.where(section_id: @obj).count
        end    
    end
  
    def total_entries
        slices.total_count
      # will_paginate
      # users.total_entries
    end
  

    def slices
      fetch_slices
    end

    
    def fetch_slices
      search_string = []
      Slice.column_names.each do |attr|
        if Slice.columns_hash[attr].type != :string
          search_string << "cast(\"#{attr}\" as varchar(10)) like :search"
        else
          search_string << "\"#{attr}\" like :search"
        end
      end

      slices = Slice.order("#{sort_column} #{sort_direction}")
      slices = slices.page(page).per(per_page)
      if params[:search][:value] != ""
        slices = slices.where(search_string.join(' or '), search: "%#{params[:search][:value]}%")
      else
        slices = slices.page(page).per(per_page)
      end

      if(@obj!=nil)
        slices.where(section_id: @obj)
      else
        slices
      end
    end
  
    def columns
        Slice.column_names
    end
  end