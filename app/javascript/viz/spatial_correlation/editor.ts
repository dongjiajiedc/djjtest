import { EditorDef } from "utils/editor";
import { editorSectionFactory, warpIf, editorTabFactory} from "../../spatial-common/editor"; 
import { processMatrix, loadAxis, loadMatrix, loadSubmatrix, constructColors } from "./data"
import { genDefaultPalette, withDefaultPalette } from "oviz-common/palette";

export const editorRef = {} as any;

//conf is configuration
const conf: any = {
    type_choice: ["full", "upper", "lower", "triangle", "inverted triangle"],
    pos_choice: ["right", "left", "top", "bottom"],
    display_choice: ["color", "width", "color + width"],
    line_choice: ["curve", "straight"],
    shape_choice: ["square", "triangle", "circle"],
};

export function copyObject(obj: any) {
    const result = {};
    Object.entries(obj).forEach(([k, v]) => result[k] = v);
    return result;
}


function run(v) {
    v.forceDraw = true;
    v.run();
}


export function editorConfig(v: any): EditorDef{
    conf.type = v.data.matrix.type;
    conf.pos =  v.data.axis.pos;
    conf.display = v.data.submatrix.display;
    conf.line = v.data.submatrix.line;
    conf.shape = v.data.matrix.shape;
    conf.varied_size = v.data.matrix.varied_size;
    conf.display_submatrix = v.data.display_submatrix
    conf.display_p_value = v.data.display_p_value
    const [defaultPalette, paletteMap] = genDefaultPalette(v.data.colorScheme.colors)

    return {
        sections: [
            editorSectionFactory(v, "General Settings", "tabs",[
                // editorTabFactory(v, "Correlation", "list", [
                //     {
                //         title: "display sub matrix",
                //         type: "checkbox",
                //         value: {
                //             current: conf.display_submatrix,
                //             callback: ()=>{
                //                 v.data.display_submatrix = conf.display_submatrix
                //                 run(v);
                //           }
                //       }
                //     },
                //     {
                //         title: "display p value",
                //         type: "checkbox",
                //         value: {
                //             current: conf.display_p_value,
                //             callback: ()=>{
                //                 v.data.display_p_value = conf.display_p_value
                //                 run(v);
                //           }
                //       }
                //     },
                // ]),
                editorTabFactory(v, "Correlation", "list", [
                    {
                        title: "type of correlation",
                        type: "select",
                        options:conf.type_choice,
                        bind: {
                            object: conf, path: "type",
                            callback: ()=>{
                                v.data.matrix.type = conf.type;//returns the different options
                                processMatrix(v)
                                loadMatrix(v);
                                v.data.axis.pos = v.data.matrix.default_axis_pos;
                                loadAxis(v);
                                loadSubmatrix(v);
                                run(v);
                            }
                        }
                    },
                    {
                        title: "position of color axis",
                        type: "select",
                        options: conf.pos_choice,
                        bind: {
                            object: conf, path: "pos",
                            callback: ()=>{
                                v.data.axis.pos =conf.pos;//returns the different options
                                loadAxis(v);
                                run(v);
                          }
                      }
                    },
                    {
                        title: "display of submatrix data",
                        type: "select",
                        options: conf.display_choice,
                        bind: {
                            object: conf, path: "display",
                            callback: ()=>{
                                v.data.submatrix.display = conf.display;//returns the different options
                                run(v);
                            }
                        }
                    },
                    {
                        title: "display of submatrix line",
                        type: "select",
                        options: conf.line_choice,
                        bind: {
                            object: conf, path: "line",
                            callback: ()=>{
                                v.data.submatrix.line = conf.line;//returns the different options
                                run(v);
                            }
                        }
                    },
                    {
                        title: "shape of correlation",
                        type: "select",
                        options: conf.shape_choice,
                        bind: {
                            object: conf, path: "shape",
                            callback: ()=>{
                                v.data.matrix.shape = conf.shape;//returns the different options
                                run(v);
                            }
                        }
                    },
                    {
                        title: "display shapes in varied size",
                        type: "checkbox",
                        bind: {
                            object: conf, path: "varied_size",
                            callback: ()=>{
                                v.data.matrix.varied_size = conf.varied_size;//returns the different options
                                run(v);
                            }
                        }
                    },
                ]),
            ]),

            ...warpIf(true, [
               editorSectionFactory(v, "Color Palettes", "single-page", [
                    {
                         type: "vue" as const,
                         title: "",
                         component: "color-picker",
                         data: {
                             title: "Choose Min-Max Color",
                             scheme: copyObject(v.data.colorScheme["colors"]),
                             palettes: withDefaultPalette(defaultPalette),
                             paletteMap,
                             id: "pwcolor",
                             callback(colors) {
                                 v.data.colorScheme["colors"] = colors;
                                 if (typeof constructColors !== "undefined") {
                                     constructColors(v, "colors");
                                 }
                                 v.run();
                             },
                        },
                    },
                ]),
            ]),
        ],
    };
}