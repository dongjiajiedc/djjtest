class DiseasesController < ApplicationController
    def index
    end

        
    def show
        @vis = ['id', 'sample_name', 'project_name', 'c_description', 'species_name', 'c_study_type', 'c_country', 'c_ethnic', 'n_age', 'c_age_unit', 'disease_name']
        @short_attrs = [['cancer_name', 'cancer_type', 'data_source', 'number_of_related_projects'], ['number_of_samples', 'sub_cancer', 'primary_site']]

        @disease = Disease.find(params[:id])
        @attrs = Disease.column_names
        @sample_attrs = Sample.column_names
        # @projects = @cancer.projects
        @invis = []
        @sample_attrs.each_with_index do |attr, index|
            if !@vis.include?(attr)
                @invis.push(index+1)
            end
        end
        gon.push invis: @invis
        
        respond_to do |format|
            format.html
            format.csv { send_data @disease.projects.to_csv }
            format.json { render json: SampleDatatable.new(view_context, 'disease', @disease.disease_name) }
        end
    end
end
