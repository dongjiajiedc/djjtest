class SectionDatatable < ApplicationDatatable
    private
  
    def data
        sections.map do |section|
        [].tap do |column|
          column << ""
          Section.column_names.each do |attr|
            if attr != 'id'
              column << "<div class='table_cell'> #{section[attr]} </div>"
            else
              column << section[attr]
            end
          end
            column << "<button class='btn btn-1'> #{link_to('Show', section)} </button>"
        end
      end
    end
  
    def count
      if(@obj==nil)
      Section.count
      else
        if(@obj == "sample")
          Section.where(sample_id: @config).count
        elsif(@obj == "tissue")
          Section.where(tissue_name: @config,c_tissue_type: 'normal').count
        end
      end
    end
  
    def total_entries
        sections.total_count
    end
  

    def sections
      fetch_sections
    end

    
    def fetch_sections
      search_string = []
      Section.column_names.each do |attr|
        if Section.columns_hash[attr].type != :string
          search_string << "cast(\"#{attr}\" as varchar(10)) like :search"
        else
          search_string << "\"#{attr}\" like :search"
        end
      end
      sections = Section.order("#{sort_column} #{sort_direction}")
      sections = sections.page(page).per(per_page)

      if params[:search][:value] != ""
        sections = sections.where(search_string.join(' or '), search: "%#{params[:search][:value]}%")
      else
        sections = sections.page(page).per(per_page)
      end

      
      if(@obj!=nil)
          if(@obj == "sample")
            sections.where(sample_id: @config)
          elsif(@obj == "tissue")
            sections.where(tissue_name: @config,c_tissue_type: 'normal')
          end
      else
        sections
      end

    end
  
    def columns
      Section.column_names
    end
  end