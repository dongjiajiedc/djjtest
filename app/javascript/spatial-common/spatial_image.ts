import { Component, ComponentOption } from "crux/dist/element";
import { template } from "crux/dist/template/tag";
//import template from "./spatial_image.bvt"
import {Brush} from "./brush"
import { event } from "crux/dist/utils";
import { processPie } from "./spatial_data";

interface SpatialImageOption extends ComponentOption{
    config: any,
    spatialImageData: any,
    spotDeconvData: any,
    metaDict: any;
    metaInfo: any;
}

export class SpatialImage extends Component<SpatialImageOption>{

    public imageX; imageY; imageWidth;
    public windowX; windowY; windowWidth; windowHeight;
    public inspectX; inspectY; inspectWidth; inspectPosition; inspectRatio;
    public padding; sliderWidth; gap;
    public scaledWindowX; scaledWindowY; scaledWindowWidth; scaledWindowHeight;
    public scaledImageX; scaledImageY; scaledImageWidth; scaledImageHeight;
    public spotR;

    public pieLegendPos; annoLegendPos;

    public brushMaxDeltaX; brushMaxDeltaY;

    public annoType;

    public filteredSpots; pieData;
    
    public static components = {Brush};
    

    protected state = {};

    public willRender() {
        if (this._firstRender){
            this.imageX = this.prop.config.imageX || 0; 
            this.imageY = this.prop.config.imageY || 0; 
            this.imageWidth = this.prop.config.imageWidth || 200;
            this.windowWidth = this.prop.config.windowWidth || 30;   
            this.windowHeight = this.prop.config.windowHeight || 30;
            this.inspectWidth = this.prop.config.inspectWidth || 200;
            this.inspectPosition = this.prop.config.inspectPosition || 'right';
            this.spotR = this.prop.config.spotR || 1;
            this.brushMaxDeltaX = this.prop.config.brushMaxDeltaX || null
            this.brushMaxDeltaY = this.prop.config.brushMaxDeltaY || null
            this.pieLegendPos = this.prop.config.pieLegendPos || {x: 90, y: 280}
            this.annoLegendPos = this.prop.config.annoLegendPos || {x: 500, y: 500}

            this.annoType = this.prop.config.annoType || null

            this.padding = 50;
            this.sliderWidth = 10;
            this.gap = 10;

            this.windowX = this.imageWidth/2 - this.windowWidth/2; 
            this.windowY = this.imageWidth/2 - this.windowHeight/2; 
        
            this.inspectRatio = this.inspectWidth/Math.max(this.windowWidth, this.windowHeight);
            this.scaledImageWidth = this.imageWidth*this.inspectRatio;
    
            this.scaledWindowWidth = this.windowWidth*this.inspectRatio
            this.scaledWindowHeight = this.windowHeight*this.inspectRatio
    
            this.inspectX = this.getInspectPosition()[0];
            this.inspectY = this.getInspectPosition()[1];
            this.scaledWindowX = this.getInspectPosition()[2];
            this.scaledWindowY = this.getInspectPosition()[3];    
            this.scaledImageX = this.getInspectPosition()[4];
            this.scaledImageY = this.getInspectPosition()[5];

            this.filteredSpots = this.filterSpot()
            console.log(this.prop.spotDeconvData)
            if(this.prop.spotDeconvData){
                this.pieData = processPie(this.prop.spotDeconvData)
            }
        }
           
    }


    // public render() {
    //     return this.t`${template}`;
    // };
    public render = template`
    Component{
        // scaled image
        Component{
            x = scaledImageX
            y = scaledImageY
            width = scaledImageWidth; height = scaledImageWidth
            Image.full{
                url = prop.spatialImageData 
            }     
        }
        // background
        Rect{
            x = 0; y = 0
            width = inspectX
            height = 2000
            fill = 'white'
        }
        Rect{
            x = 0; y = 0
            width = 2000
            height = inspectY
            fill = 'white'
        }        
        Rect{
            x = inspectX + inspectWidth; y = 0
            width = 2000
            height = 2000
            fill = 'white'
        }
        Rect{
            x = 0; y = inspectY + inspectWidth
            width = 2000
            height = 2000
            fill = 'white'
        }  
        // inspect window
        Rect{
            x = scaledWindowX; y = scaledWindowY
            width = scaledWindowWidth; height = scaledWindowHeight
            stroke = '#ffbb4d'
            strokeWidth = 2
            fill = 'transparent'
        }      
        // image
        Component{
            // behavior:drag {
            //     onDrag = @bind(handleWindowPos)
            // }
            x = imageX; y = imageY
            width = imageWidth; height = imageWidth
            Image.full {
                url = prop.spatialImageData 
            }

            // spot
            @if (annoType){
                @for (spot, i) in Object.keys(prop.metaDict){
                    Circle.centered{
                        static = true
                        x = parseInt(prop.metaDict[spot]['c_pxl_col_in_lowres'])*imageWidth/600
                        y = parseInt(prop.metaDict[spot]['c_pxl_row_in_lowres'])*imageWidth/600             
                        fill = prop.metaInfo[annoType].scheme.get(prop.metaDict[spot][annoType])
                        r = spotR
                    }
                }
                // window
                Rect{
                    x = windowX; y = windowY;
                    width = windowWidth; height = windowHeight;
                    stroke = 'black' // '#DC143C';
                    fill = 'rgba(255, 187, 77, 0)'
                    // behavior:tooltip{
                    //     content = 'DRAG to inspect the image.'
                    // }
                }  
            }
            @else{
                // window
                Rect{
                    x = windowX; y = windowY;
                    width = windowWidth; height = windowHeight;
                    //stroke = '#DC143C';
                    fill = 'rgba(255, 187, 77, 0.5)'
                    // behavior:tooltip{
                    //     content = 'DRAG to inspect the image.'
                    // }
                }  
                @for (spot, i) in filteredSpots{
                    Circle.centered{
                        x = parseInt(prop.metaDict[spot]['c_pxl_col_in_lowres'])*imageWidth/600
                        y = parseInt(prop.metaDict[spot]['c_pxl_row_in_lowres'])*imageWidth/600             
                        //fill = 'rgba(255, 187, 77)'
                        r = spotR
                    }
                }
            }

        }
        // image slider
        Rect{
            x = imageX + imageWidth + gap; y = imageY
            width = sliderWidth; height = imageWidth
            fill = '#DCDCDC'
        }
        Rect{
            x = imageX ; y = imageY + imageWidth + gap
            width = imageWidth; height = sliderWidth
            fill = '#DCDCDC'
        }   
        // window slider
        Brush{ // vertical
            ref = 'vertical_brush'
            type = 'v'
            x = imageX + imageWidth + gap; y = imageY
            width = sliderWidth; height = imageWidth
            rangeX = [0, sliderWidth]
            rangeY = [0, imageWidth]
            initialRangeY = [windowY, windowY + windowHeight] 
            maxDeltaY = brushMaxDeltaY
            onBrushEnd = @bind(handleVSliderPos)
            brush.fill = '#fff'
            topHandle.fill = '#ffbb4d'
            bottomHandle.fill = '#ffbb4d'
            behavior:tooltip{
                content = 'DRAG to inspect the image.'
            }
        }        
        Brush{ // horizontal
            ref = 'horizontal_brush'
            type = 'h'
            x = imageX ; y = imageY + imageWidth + gap
            width = imageWidth; height = sliderWidth
            rangeX = [0, imageWidth]
            rangeY = [0, sliderWidth]
            initialRangeX = [windowX, windowX + windowWidth]   
            maxDeltaX = brushMaxDeltaX         
            onBrushEnd = @bind(handleHSliderPos)
            brush.fill = '#fff'
            leftHandle.fill = '#ffbb4d'
            rightHandle.fill = '#ffbb4d'
            behavior:tooltip{
                content = 'DRAG to inspect the image.'
            }
        }
        // Mapping
        Polygon{
            points = getMappingArea()
            fill = 'rgba(255, 187, 77, 0.5)'
            //stroke = '#ffbb4d'
            //strokeWidth = 2
        }        
        Component{
            x = scaledImageX
            y = scaledImageY
            width = scaledImageWidth; height = scaledImageWidth   
            @for (spot, i) in filteredSpots{
                @let x = parseInt(prop.metaDict[spot]['c_pxl_col_in_lowres'])*imageWidth/600*inspectRatio
                @let y = parseInt(prop.metaDict[spot]['c_pxl_row_in_lowres'])*imageWidth/600*inspectRatio
                @if (pieData){
                    @if (Object.keys(pieData).includes(spot)){
                        PieChart.centered{
                            x = x; y = y; 
                            data = pieData[spot]
                            :children(d){
                                Arc{
                                    //@expr console.log(d)
                                    x1 = d.start
                                    x2 = d.end
                                    r1 = 0
                                    r2 = 0.8 * spotR* inspectRatio
                                    fill = d.color
                                    behavior:tooltip{
                                        content = getPieSpotInfo(d, spot)
                                    }
                                }
                            }
                            colorScheme = pieData.colorScheme
                        }
                    }
                }
                @else{
                    Circle.centered{
                        x = x; y = y  
                        fill = 'rgba(255, 187, 77, 0.3)'
                        r = spotR * inspectRatio
                        behavior:tooltip{
                            content = getSpotInfo(spot)
                        }
                    }
                }
            }
        }
        //legends
        @if (pieData){
            Component {
                key = "peiLegend"
                // static = true;
                height = 156; width = 200
                behavior:drag {
                    onDrag = @bind(handleLegendPos)
                }
                @props pieLegendPos
                Rows {
                    @for (row, i) in pieData.rows {                       
                        Component {
                            height = 13
                            Rect {
                                height = 8; width = 10; 
                                x = 2; y = 2
                                fill = pieData.colorScheme.get(row)
                            }
                            Text {
                                fontSize = 10; x = 15; y = 1
                                text = row
                            }
                        }                        
                    }
                }
            }
        }
        @if (annoType){
            Component {
                key = "annoLegend"
                // static = true;
                height = 156; width = 200
                behavior:drag {
                    onDrag = @bind(handleLegendPos)
                }
                @props annoLegendPos
                Rows {
                    Component{
                        height = 13
                        Text {
                            fontSize = 10; x = 2; y = 1
                            text = annoType
                        }
                    }
                    @for (type, i) in prop.metaInfo[annoType].values {                       
                        Component {
                            height = 13
                            Rect {
                                height = 8; width = 10; 
                                x = 2; y = 2
                                fill = prop.metaInfo[annoType].scheme.get(type)
                            }
                            Text {
                                fontSize = 10; x = 15; y = 1
                                text = type
                            }
                        }                        
                    }
                }
            }
        }        
    }
    `;    

    public defaultProp() {
        return {
            ...super.defaultProp(),
        };
    }

    public didCreate(){
        event.on("activeSpots", (_, spots) => {
            this.filteredSpots = spots;
        })       
    }

    protected setActiveSpot(spots){   
        event.emit("activeSpots", spots)
    }

    protected getPieSpotInfo(d, spot){
        return `Spot: ${spot}<br>Cell: ${d.data.name}<br>Ratio: ${d.data.percentage}%`
    }
    protected getSpotInfo(spot){
        return `Spot: ${spot}`
    }
    
    protected getInspectPosition(){
        var inspectX, inspectY;
        var scaledImageX, scaledImageY;
        var scaledWindowX, scaledWindowY;
        if (this.inspectPosition == 'right'){
            inspectX = this.imageX + this.imageWidth + this.padding;
            inspectY = this.imageY;
        }else{
            inspectX = this.imageX;
            inspectY = this.imageY + this.imageWidth + this.padding;

        }
        scaledWindowX = inspectX + (this.inspectWidth - this.scaledWindowWidth)/2;
        scaledWindowY = inspectY + (this.inspectWidth - this.scaledWindowHeight)/2;
        scaledImageX = this.scaledWindowX - this.windowX*this.inspectRatio;
        scaledImageY = this.scaledWindowY - this.windowY*this.inspectRatio;
        return [inspectX, inspectY, scaledWindowX, scaledWindowY, scaledImageX, scaledImageY]
    }

    protected getMappingArea(){
        var points;
        if(this.inspectPosition == 'right'){
            points = [
                [this.imageX + this.imageWidth + this.gap + this.sliderWidth, this.imageY + this.windowY], 
                [this.imageX + this.imageWidth + this.gap + this.sliderWidth, this.imageY + this.windowY + this.windowHeight],
                [this.scaledWindowX, this.scaledWindowY + this.scaledWindowHeight],
                [this.scaledWindowX, this.scaledWindowY]
            ]
        }else{
            points = [
                [this.imageX + this.windowX, this.imageY + this.imageWidth + this.gap + this.sliderWidth], 
                [this.imageX + this.windowX + this.windowWidth, this.imageY + this.imageWidth + this.gap + this.sliderWidth],
                [this.scaledWindowX + this.scaledWindowWidth, this.scaledWindowY],
                [this.scaledWindowX, this.scaledWindowY]
            ]
        }
        return points;
    }
    protected handleWindowPos(_, el, deltaPos: [number, number]) {
        //console.log(this.windowX + deltaPos[0])
        if (this.windowX + deltaPos[0] >= 0 &&
            this.windowX + deltaPos[0] <= this.imageWidth - this.windowWidth &&
            this.windowY + deltaPos[1] >= 0 &&
            this.windowY + deltaPos[1] <= this.imageWidth - this.windowHeight){
                this.windowX = this.windowX + deltaPos[0];
                this.windowY = this.windowY + deltaPos[1];

                (this.$ref.horizontal_brush as unknown as Brush).setCurrentRangeX(this.windowX, this.windowX + this.windowWidth);
                (this.$ref.vertical_brush as unknown as Brush).setCurrentRangeY(this.windowY, this.windowY + this.windowHeight);

                this.scaledImageX = this.scaledImageX - deltaPos[0]*this.inspectRatio;
                this.scaledImageY = this.scaledImageY - deltaPos[1]*this.inspectRatio;

                //this._firstRender = true;
                this.$v.forceRedraw = true;
                //this.willRender();
                this.redraw();
            }
    }
    protected handleVSliderPos(range: [number, number]) {
        var deltaT = range[0] - this.windowY;
        var deltaB = range[1] - (this.windowY + this.windowHeight);
        (this.$ref.vertical_brush as unknown as Brush).setCurrentRangeY(range[0], range[1]);

        this.windowHeight = range[1] - range[0];
        this.windowY = range[0];
        this.updateScale()

        this.redraw();
    } 
    
    protected handleHSliderPos(range: [number, number]) {
        (this.$ref.horizontal_brush as unknown as Brush).setCurrentRangeX(range[0], range[1]);

        this.windowWidth = range[1] - range[0];
        this.windowX = range[0];
        this.updateScale() 
           
        this.redraw();
    }    

    protected updateScale(){
        this.inspectRatio = this.inspectWidth/Math.max(this.windowWidth, this.windowHeight);
        this.scaledImageWidth = this.imageWidth*this.inspectRatio;
    
        this.scaledWindowWidth = this.windowWidth*this.inspectRatio
        this.scaledWindowHeight = this.windowHeight*this.inspectRatio    

        this.inspectX = this.getInspectPosition()[0];
        this.inspectY = this.getInspectPosition()[1];
        this.scaledWindowX = this.getInspectPosition()[2];
        this.scaledWindowY = this.getInspectPosition()[3];    
        this.scaledImageX = this.getInspectPosition()[4];
        this.scaledImageY = this.getInspectPosition()[5];

        this.setActiveSpot(this.filterSpot());
    }

    protected handleLegendPos(_, el, deltaPos: [number, number]) {
        switch (el.id) {
            case "histLegend":
                this.pieLegendPos = {x: this.pieLegendPos.x + deltaPos[0],
                    y: this.pieLegendPos.y + deltaPos[1]};
                break;
        }

        this.redraw();
    }

    protected filterSpot(){
        console.log('filter spot')
        var filteredSpots = [];
        Object.keys(this.prop.metaDict).forEach(spot => {
            var v = this.prop.metaDict[spot];
            var x = parseInt(v['c_pxl_col_in_lowres'])*this.imageWidth/600;
            var y = parseInt(v['c_pxl_row_in_lowres'])*this.imageWidth/600;
    
            if (x >= this.windowX &&
                x <= this.windowX + this.windowWidth &&
                y >= this.windowY &&
                y <= this.windowY + this.windowHeight
                ){
                    filteredSpots.push(spot)
                }
        });
        
        return filteredSpots;
    }
}