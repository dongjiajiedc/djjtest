import Oviz from "crux";
import template from "./template.bvt";
import { BarView } from "viz/spatial_deconvolution_bar_view/bar_view";
import { processMeta, processGraph } from "spatial-common/spatial_data"
import { minmax } from "crux/dist/utils/math";
import { computeLog } from "utils/maths";
import { schemeRdYlBu } from "d3-scale-chromatic";
import * as d3 from "d3";


export class SpatialInteraction extends Oviz.Component {

    public result;
    public render() {
        return this.t`${template}`;
    }

    public willRender() {
        processMeta(this, [])
        processGraph(this)

        var areaData = {
            'y1': [34, 40, 17, 38, 36, 22, 44, 43, 6, 37, 30],
            'y2': [32, 15, 18, 24, 44, 18, 20, 31, 43, 22, 25],
            'y3': [12, 39, 17, 9, 22, 21, 49, 33, 23, 16, 33]
        }
        const c = Oviz.color.schemeCategory("light", ['y1', 'y2', 'y3']);
        this.result = {areaData, c}
    }
    
    public getRibbonPathForNode(source, target, radius){
        var path = d3.ribbon()({
            source: {
                startAngle: source.circleAngle/180*Math.PI,
                endAngle: source.circleAngle/180*Math.PI,
                radius: radius,
            },
            target: {
                startAngle: target.circleAngle/180*Math.PI,
                endAngle: target.circleAngle/180*Math.PI,
                radius: radius,
            },          
        });       
        return path;
    }
    public getRibbonPath(pair, radius){
        var path = d3.ribbon()({
            source: {
                startAngle: pair.source.startAngle,
                endAngle: pair.source.endAngle,
                radius: radius,
            },
            target: {
                startAngle: pair.target.startAngle,
                endAngle: pair.target.endAngle,
                radius: radius,
            },          
        });       
        return path;      
    }
    public getArcPath(x){
        return d3.arc()(x);
    }

}
