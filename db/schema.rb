# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_07_28_024340) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "analyses", force: :cascade do |t|
    t.string "name", null: false
    t.json "files_info"
    t.integer "mid"
    t.text "description"
    t.bigint "analysis_category_id"
    t.text "cover_image"
    t.string "url", null: false
    t.text "documentation"
    t.text "references"
    t.text "about"
    t.text "rendered_ref"
    t.text "rendered_doc"
    t.text "rendered_about"
    t.integer "position"
    t.boolean "hidden", default: false
    t.integer "multiple_mid", default: -1
    t.integer "single_demo_id", default: -1
    t.integer "multiple_demo_id", default: -1
    t.integer "single_result_id", default: -1
    t.integer "multiple_result_id", default: -1
    t.index ["analysis_category_id"], name: "index_analyses_on_analysis_category_id"
  end

  create_table "analyses_visualizers", force: :cascade do |t|
    t.bigint "analysis_id", null: false
    t.bigint "visualizer_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["analysis_id"], name: "index_analyses_visualizers_on_analysis_id"
    t.index ["visualizer_id"], name: "index_analyses_visualizers_on_visualizer_id"
  end

  create_table "analysis_categories", force: :cascade do |t|
    t.string "name", null: false
    t.integer "position"
  end

  create_table "analysis_pipelines", force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.integer "pid"
    t.text "description"
    t.boolean "hidden", default: false
    t.integer "position"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "cover_image"
    t.string "url", null: false
    t.integer "single_demo_id", default: -1
    t.integer "multiple_demo_id", default: -1
    t.integer "single_result_id", default: -1
    t.integer "multiple_result_id", default: -1
    t.integer "multiple_pid", default: -1
    t.text "documentation"
    t.text "rendered_doc"
  end

  create_table "analysis_user_data", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "analysis_id"
    t.bigint "task_output_id"
    t.json "chosen", null: false
    t.boolean "use_demo_file", default: true
    t.index ["analysis_id"], name: "index_analysis_user_data_on_analysis_id"
    t.index ["task_output_id"], name: "index_analysis_user_data_on_task_output_id"
    t.index ["user_id"], name: "index_analysis_user_data_on_user_id"
  end

  create_table "cancers", force: :cascade do |t|
    t.string "cancer_name"
    t.string "cancer_type"
    t.string "data_source"
    t.text "description"
    t.integer "number_of_related_projects"
    t.integer "number_of_samples"
    t.string "sub_cancer"
    t.string "primary_site"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "datasets", force: :cascade do |t|
    t.string "name"
    t.string "description"
    t.string "tag"
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_datasets_on_user_id"
  end

  create_table "datasets_samples", id: false, force: :cascade do |t|
    t.bigint "dataset_id"
    t.bigint "sample_id"
    t.index ["dataset_id"], name: "index_datasets_samples_on_dataset_id"
    t.index ["sample_id"], name: "index_datasets_samples_on_sample_id"
  end

  create_table "diseases", force: :cascade do |t|
    t.string "disease_name"
    t.string "disease_type"
    t.string "data_source"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "module_requirements", force: :cascade do |t|
    t.bigint "analysis_id"
    t.bigint "analysis_pipeline_id"
    t.index ["analysis_id"], name: "index_module_requirements_on_analysis_id"
    t.index ["analysis_pipeline_id"], name: "index_module_requirements_on_analysis_pipeline_id"
  end

  create_table "organs", force: :cascade do |t|
    t.string "primary_site"
    t.integer "num_of_projects"
    t.text "project_list"
    t.integer "num_of_samples"
    t.string "data_type"
    t.string "program"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string "project_name"
    t.string "cancer_name"
    t.string "database"
    t.string "preprocessed"
    t.string "major_related_publications"
    t.string "publication_link"
    t.string "published_year"
    t.string "platform"
    t.string "original_description"
    t.string "resolution"
    t.integer "num_of_samples"
    t.integer "num_of_slices"
    t.integer "num_of_spots"
    t.integer "num_of_genes"
    t.integer "num_of_cells"
    t.string "species"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "samples", force: :cascade do |t|
    t.string "sample_name"
    t.string "project_name"
    t.string "description"
    t.string "species_name"
    t.string "cancer_name"
    t.string "disease_name"
    t.string "c_study_type"
    t.string "c_country"
    t.string "c_ethnic"
    t.string "c_sex"
    t.integer "n_age"
    t.string "c_age_unit"
    t.string "c_tumor_subtype"
    t.string "c_tumor_grade"
    t.string "c_tumor_stage"
    t.string "c_tumor_type"
    t.string "c_diease_name"
    t.string "c_cancer_type"
    t.integer "n_os"
    t.string "c_os_unit"
    t.string "c_os_status"
    t.integer "n_pfs"
    t.string "c_pfs_unit"
    t.string "c_pfs_status"
    t.integer "num_of_slices"
    t.integer "num_of_spots"
    t.integer "num_of_genes"
    t.bigint "project_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["project_id"], name: "index_samples_on_project_id"
  end

  create_table "sections", force: :cascade do |t|
    t.string "section_name"
    t.string "sample_name"
    t.string "project_name"
    t.string "tissue_name"
    t.string "c_tissue_source"
    t.string "c_tissue_type"
    t.string "c_storage_type"
    t.string "c_metastasis"
    t.string "c_collection_way"
    t.string "c_collection_position"
    t.integer "n_collection_time"
    t.string "c_collection_time_unit"
    t.string "c_collection_stage"
    t.bigint "sample_id", null: false
    t.bigint "project_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["project_id"], name: "index_sections_on_project_id"
    t.index ["sample_id"], name: "index_sections_on_sample_id"
  end

  create_table "slices", force: :cascade do |t|
    t.string "slice_name"
    t.string "section_name"
    t.string "sample_name"
    t.string "project_name"
    t.string "c_protocol"
    t.integer "n_layer"
    t.integer "n_diameter"
    t.string "n_diameter_unit"
    t.integer "n_number_of_spots"
    t.integer "n_number_of_genes"
    t.integer "n_tissue_hires_scalef"
    t.integer "n_tissue_lowres_scalef"
    t.integer "n_fiducial_pxl_diameter_fullres"
    t.integer "n_spot_pxl_diameter_fullres"
    t.string "p_tissue_fullres_image"
    t.string "p_tissue_hires_image"
    t.string "p_tissue_lowres_image"
    t.string "p_aligned_fiducials"
    t.string "p_detected_tissue_image"
    t.string "p_spot_details"
    t.string "p_rna_details"
    t.string "c_rna_process_tool"
    t.string "c_rna_preprocessed"
    t.string "l_data_access"
    t.bigint "section_id", null: false
    t.bigint "sample_id", null: false
    t.bigint "project_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["project_id"], name: "index_slices_on_project_id"
    t.index ["sample_id"], name: "index_slices_on_sample_id"
    t.index ["section_id"], name: "index_slices_on_section_id"
  end

  create_table "species", force: :cascade do |t|
    t.string "species_name"
    t.string "data_source"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "task_outputs", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "output_id", null: false
    t.json "file_paths", null: false
    t.bigint "analysis_id"
    t.bigint "task_id"
    t.index ["analysis_id"], name: "index_task_outputs_on_analysis_id"
    t.index ["task_id"], name: "index_task_outputs_on_task_id"
    t.index ["user_id"], name: "index_task_outputs_on_user_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "tid", null: false
    t.string "status", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "analysis_id"
    t.bigint "analysis_pipeline_id"
    t.boolean "is_demo", default: false
    t.index ["analysis_id"], name: "index_tasks_on_analysis_id"
    t.index ["analysis_pipeline_id"], name: "index_tasks_on_analysis_pipeline_id"
    t.index ["user_id"], name: "index_tasks_on_user_id"
  end

  create_table "tissues", force: :cascade do |t|
    t.string "tissue_name"
    t.string "c_tissue_source"
    t.string "description"
    t.string "data_source"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.integer "dataset_n"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "visualizers", force: :cascade do |t|
    t.string "name", null: false
    t.string "js_module_name", null: false
  end

  create_table "viz_data_sources", force: :cascade do |t|
    t.string "data_type", null: false
    t.boolean "optional", default: false
    t.boolean "allow_multiple", default: false
    t.bigint "visualizer_id"
    t.index ["visualizer_id"], name: "index_viz_data_sources_on_visualizer_id"
  end

  create_table "viz_file_objects", force: :cascade do |t|
    t.bigint "analysis_id"
    t.bigint "user_id"
    t.bigint "viz_data_source_id"
    t.string "name", null: false
    t.text "file", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["analysis_id"], name: "index_viz_file_objects_on_analysis_id"
    t.index ["user_id"], name: "index_viz_file_objects_on_user_id"
    t.index ["viz_data_source_id"], name: "index_viz_file_objects_on_viz_data_source_id"
  end

  add_foreign_key "analyses_visualizers", "analyses"
  add_foreign_key "analyses_visualizers", "visualizers"
  add_foreign_key "datasets", "users"
  add_foreign_key "samples", "projects"
  add_foreign_key "sections", "projects"
  add_foreign_key "sections", "samples"
  add_foreign_key "slices", "projects"
  add_foreign_key "slices", "samples"
  add_foreign_key "slices", "sections"
end
