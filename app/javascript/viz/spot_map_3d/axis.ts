import { Component, ComponentOption } from "crux/dist/element";
import { template } from "crux/dist/template/tag";

interface AxisOption extends ComponentOption{
    version:string;
}


export class Axis extends Component<AxisOption>{

    private get_text(i: any){
        if (this.prop.version.includes('human_DLPFC')){
            const list_idx =  ['L1', 'L2', 'L3', 'L4', 'L5', 'L6', 'WM']
            console.log(list_idx[i])
            return list_idx[i]
        }
        else{
            return 'cluster ' + i.toString()
        }
    }

    render(){
		return this.t`
            Component{
                x = prop.axis_padding_x
                y = prop.axis_padding_y

                @let axis_length = prop.axis_length
                @let cluster_display = prop.cluster_display
                @let cluster_scheme = prop.cluster_scheme
                @let cluster_number = prop.cluster_number
                @let cluster_array = prop.cluster_array

                @if cluster_display == false{
                    Rect{
                        y = - 0.5 * axis_length;
                        height = 0.5 * axis_length;
                        width = prop.axis_width;
                        fill = @gradient("bg_pos")
                    }
                    Rect {
                        height =  0.5 * axis_length; 
                        width = prop.axis_width;
                        fill = @gradient("bg_neg")
                    }
                    Axis {
                        y = -0.5 * axis_length;
                        yScale = @scale-linear(5, 0, 2, axis_length)
                        orientation = "left"
                        fill = "#d1d1e0"
                    }
                }
                @else{
                    @for (d, i) in cluster_array{
                        @let rect_height = axis_length/cluster_number
                        @let rect_pos = (i - cluster_number/2) * rect_height
                        @let idx_text = get_text(i)

                        Rect {
                            y = rect_pos
                            height =  rect_height
                            width = prop.axis_width;
                            fill = cluster_scheme.get(d)
                            stroke = "black"
                        }
                        Text{
                            anchor = @anchor("middle","left")
                            x = prop.axis_width + 5;
                            y = rect_pos + 0.5 * rect_height //to keep it in middle
                            text = idx_text
                            fill = "white"
                            fontSize = 12
                        }
                    }
                }
            
            }
        ` 
    }
}