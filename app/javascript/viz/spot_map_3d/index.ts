import Crux from "crux";
import { registerDefaultBioInfoComponents } from "crux/dist/element/global";
import { register } from "page/visualizers";
import { registerEditorConfig } from "../../utils/editor";
import { editorConfig } from "./editor";
import { initialize} from "./data"
import { SpatialPI } from "./spatialPI";
import Oviz from "crux";


registerDefaultBioInfoComponents();

const MODULE_NAME = "spot_map_3d";

export function init(id ,path1 ,path2 , eid ,plot_name) {
    // if (!window.gon || window.gon.module_name !== MODULE_NAME)  return;
    Oviz.visualize({
        el: id,
        renderer: "svg",
        root: new SpatialPI(0),
        data:{},
        loadData: {
            //  v1 and v2 are two different slices of a tissue
            spatial_data_v1: {
                fileKey: "spatialGeneV1",
                url: path1,
                type: "csv",
                loaded(){},
            },
            spatial_data_v2: {
                fileKey: "spatialGeneV2",
                url: path1,
                type: "csv",
                loaded(){},
            },
            spatialImageData: {
                fileKey: "spatialImageData",
                url: path2,
                type: 'image',
                loaded(d){
                    return d;
                }
            }
        },
        setup() {
            initialize(this);
            // registerEditorConfig(editorConfig(this));
            console.log(this.data);
        }
    });
}
