import Crux from "crux";
import * as d3 from "d3";
import { ColorSchemeNumeric, generateColors} from "../../spatial-common/defs";
import { ColorSchemeCategory } from "crux/dist/color";

const GRID_WIDTH_SCALE = 0.012;
const SPATIAL_WIDTH = 600;
const SPATIAL_HEIGHT = 600;
const cluster_columns = ['cluster_2','cluster_3','cluster_4','cluster_5','cluster_6','cluster_7','cluster_8','cluster_9','cluster_10']
const additional_columns = ["SpatialDE_PCA","SpatialDE_pool_PCA","HVG_PCA","pseudobulk_PCA","markers_PCA","SpatialDE_UMAP",
                            "SpatialDE_pool_UMAP","HVG_UMAP","pseudobulk_UMAP","markers_UMAP","SpatialDE_PCA_spatial",
                            "SpatialDE_pool_PCA_spatial","HVG_PCA_spatial","pseudobulk_PCA_spatial","markers_PCA_spatial",
                            "SpatialDE_UMAP_spatial","SpatialDE_pool_UMAP_spatial","HVG_UMAP_spatial","pseudobulk_UMAP_spatial",
                            "markers_UMAP_spatial"]
const meta_columns = ['version','barcode','row','col','pxl_col_in_fullres','pxl_row_in_fullres','pxl_col_in_lowres','pxl_row_in_lowres',
                      'pxl_col_in_highres','pxl_row_in_highres', 'umap_x', 'umap_y']
const base_path_1 = "https://github.com/shellmik/Spatial-Genomics-Processing/blob/master/data/"
const base_path_2 = "/spatial/tissue_lowres_image.png?raw=true"

function rgbToHex(a, b, c) {
    var r = /^\d{1,3}$/;
    if (!r.test(a) || !r.test(b) || !r.test(c)) 
        return window.alert("wrong input rgb");
    var hexs = [a.toString(16), b.toString(16), c.toString(16)];
    for (var i = 0; i < 3; i++) if (hexs[i].length == 1) hexs[i] = "0" + hexs[i];
    return "#" + hexs.join("");
}


export function convertTo3d(ang_A: any, ang_B:any, p: any) {
    var x = p[0]; var y = p[1]; var z = p[2];

    x = x - 0.5 * SPATIAL_WIDTH
    y = y - 0.5 * SPATIAL_HEIGHT

    const A = (ang_A / 180) * Math.PI;
    const B = (ang_B / 180) * Math.PI;

    const x_p = x * Math.cos(B) - y * Math.cos(A)
    const y_p = x * Math.sin(B) + y * Math.sin(A)

    return [x_p, - (y_p + z)];
}


export function defineMinMaxColors(v: any, _colors: any){
    const min_max_colors = _colors
    const min_max_scheme = new ColorSchemeNumeric([min_max_colors.min, min_max_colors.middle, min_max_colors.max], { type: "linear", domain: [0, 2.5, 5]});
    v.defineGradient("bg_pos", "vertical", [min_max_colors.max, min_max_colors.middle]);
    v.defineGradient("bg_neg", "vertical", [min_max_colors.middle, min_max_colors.min]);
    v.data.spatial_color = {exp_scheme: min_max_scheme, exp_colors: min_max_colors};
}


export function defineClusters(v: any, _cluster_key: any){
    var cluster_key = _cluster_key
    var cluster_number;
    var cluster_array = [];
    
    if (additional_columns.includes(_cluster_key)){
        cluster_number = 7
    }
    else{
        cluster_number = (parseInt(_cluster_key.split('_')[1]));
    }

    for(let i = 1; i<= cluster_number; i++){
        cluster_array.push(i.toString());
    }
    const cluster_columns_all = cluster_columns.concat(additional_columns)
    v.data.spatial_cluster = {cluster_number, cluster_key, cluster_array, cluster_columns_all}
}


export function defineClusterColors(v:any, colorDict?: any){
    const cluster_array = v.data.spatial_cluster.cluster_array
    var cluster_scheme;
    if (typeof colorDict == "undefined") {
        cluster_scheme = ColorSchemeCategory.create(cluster_array, generateColors(cluster_array.length, 'cluster'));
    }
    else
        cluster_scheme = ColorSchemeCategory.create(cluster_array, cluster_array.map(v => colorDict[v]));
    v.data.spatial_cluster.cluster_scheme = cluster_scheme
}


export function processData(v: any, n:any, _displayed_genome: any){
    const spatial_data = v.data[n]
    const name = n + "_info"
    
    // meta info
    const columns = spatial_data.columns
    const len_column = columns.length
    const len_row = spatial_data.length

    // process column values
    var cluster_columns_all = cluster_columns.concat(additional_columns)

    // process genomes
    var all_genomes = []
    const displayed_genome = _displayed_genome
    for (let i = 0; i < len_column; i++){
        const gene = columns[i]
        if (!cluster_columns_all.includes(gene) && !meta_columns.includes(gene)){
            all_genomes.push(gene)
        }
    }

    // process elements
    const displayed_data = [];
    const umap_displayed_data = [];
    var max_value = 0;
    var max_x = 0, min_x = 0, max_y = 0, min_y = 0;

    for(let i = 0; i < len_row; i++){
        const elem_row = spatial_data[i]
        //fill the element
        var elem = {}
        var umap_elem = {}

        elem['value']= parseInt(elem_row[displayed_genome])
        if (elem['value'] > max_value){
            max_value = elem['value']
        }
        //row, col, barcode
        elem['row'] = parseInt(elem_row.row)
        elem['col'] = parseInt(elem_row.col)
        elem['row_in_lowres'] = parseInt(elem_row.pxl_row_in_lowres)
        elem['col_in_lowres'] = parseInt(elem_row.pxl_col_in_lowres)
        elem['row_in_highres'] = parseInt(elem_row.pxl_row_in_highres)
        elem['col_in_highres'] = parseInt(elem_row.pxl_col_in_highres)
        elem['barcode'] = elem_row.barcode
        elem['idx'] = i

        //umap
        umap_elem['x'] = parseFloat(elem_row.umap_x)
        umap_elem['y'] = parseFloat(elem_row.umap_y)
        if (umap_elem['x'] > max_x){ max_x = umap_elem['x']}
        if (umap_elem['x'] < min_x){ min_x = umap_elem['x']}
        if (umap_elem['y'] > max_y){ max_y = umap_elem['y']}
        if (umap_elem['y'] < min_y){ min_y = umap_elem['y']}
        umap_elem['idx'] = i

        //clusters
        cluster_columns_all.forEach(function (key, idx) {
            elem[key] = elem_row[key]
        });

        displayed_data.push(elem)
        umap_displayed_data.push(umap_elem)
    }

    // cases where value for all barcodes are 0
    if (max_value == 0) {max_value = 9999}
    max_x = Math.ceil(max_x)
    min_x = Math.floor(min_x)
    max_y = Math.ceil(max_y)
    min_y = Math.floor(min_y)

    v.data[name] = {umap_displayed_data, max_x, min_x, max_y, min_y,
        displayed_data, max_value, all_genomes, displayed_genome}
}

export function setDisplayScale(v, display_scale: number){
    v.data.spatial_background.display_scale = display_scale
    v.data.spatial_background.grid_width = display_scale * GRID_WIDTH_SCALE
}

export function initialize(v: any){
    // min max color
    const min_color = String(rgbToHex(34, 103, 173));
    const middle_color = "white"
    const max_color = String(rgbToHex(178, 24, 44));
    defineMinMaxColors(v, {"min": min_color, "middle": middle_color, "max": max_color})

    // spatial tissue config
    v.size.width = 1600;
    v.size.height = 1600;
    var spatial_width = SPATIAL_WIDTH;
    var spatial_height = SPATIAL_HEIGHT;
    var display_scale = 600;
    v.data.spatial_background = {spatial_width, spatial_height, display_scale}
    setDisplayScale(v, display_scale)

    // spatial shape
    const shape = "circle";
    const varied_size = false;
    v.data.spatial_shape = {shape, varied_size};

    // spatial 3d
    const display_3d = true;
    const ang_A = -45;
    const ang_B = 20;
    const layer_height = 250;
    v.data.spatial_3d = {display_3d, ang_A, ang_B, layer_height}

    // spatial_cluster config
    defineClusters(v, 'cluster_7')
    defineClusterColors(v)
    const cluster_display = false
    v.data.spatial_cluster.cluster_display = cluster_display;

    // displayed data
    const version = v.data['spatial_data_v1'][0]['version']
    const displayed_data_list = ['spatial_data_v1', 'spatial_data_v2']
    var displayed_genome
    
    //TODO: set a config file
    if (version.includes('human_DLPFC')){ 
        displayed_genome = 'SNAP25'
    }
    else if (version.includes('human_breast')){
        displayed_genome = 'NEAT1'
    }
    else {
        displayed_genome =  v.data['spatial_data_v1'].columns[3]
    }
    displayed_data_list.forEach(n => {processData(v, n, displayed_genome)});
    v.data.displayed_data_list = displayed_data_list;

    // process axis
    const axis_length = 0.5 * v.data.spatial_background.spatial_height
    const axis_width = 30
    const axis_padding_x = v.data.spatial_background.spatial_width + 0.5 * axis_width + 20
    const axis_padding_y = 0.5 * v.data.spatial_background.spatial_height
    v.data.axis_position = {axis_length, axis_width, axis_padding_x, axis_padding_y}

    //image_portal
    v.data.version = version
    v.data.image_portal = base_path_1 + version + base_path_2
    v.data.separate_image = false
}
