import { Component, ComponentOption } from "crux/dist/element";
import { template } from "crux/dist/template/tag";

interface AxisOption extends ComponentOption{
}

export class Axis extends Component<AxisOption>{
    public render = template` 
	Component{
		Component{
			x = prop.apadding_x
			y = prop.apadding_y
			rotation = @rotate(prop.axis_angle)
			Rect {
				height =  prop.axis_length*0.5; width = 30
				fill = @gradient("bg_pos")
			}
			Rect {
				y = 0.5*prop.axis_length
				height =  prop.axis_length*0.5; width = 30
				fill = @gradient("bg_neg")
			}
			Axis {
				yScale = @scale-linear(1, -1, 0, prop.axis_length)
				orientation = "left"
			}
		}
	}
    ` 
}