import Crux from "crux";
import Oviz from "crux";
import { registerDefaultBioInfoComponents } from "crux/dist/element/global";
import { register } from "page/visualizers";
import { registerEditorConfig } from "../../utils/editor";
import { editorConfig, editorRef } from "./editor";
import { initialize, constructColors, processMatrix, processSubmatrix, loadMatrix, loadAxis, loadSubmatrix} from "./data";
import { CorrelationPI } from "./correlationPI";



registerDefaultBioInfoComponents();
const MODULE_NAME = 'spatial_correlation'
export function init(id ,path1 ,path2,path3, eid ,plot_name) {
    // if (!window.gon || window.gon.module_name !== MODULE_NAME)  return;
    Oviz.visualize({
        el: id,
        renderer: "svg",
        root: new CorrelationPI(0), //components
        loadData: {
            main_matrix: {
                fileKey: "corrMatrix",
                url: path1,
                type: "csv",
                loaded(){},
            },
            sub_matrix: {
                fileKey: "corrSubmatrix",
                url: path2,
                type: "csv",
                optional: true,
                loaded(){},
            },
            p_value: {
                fileKey: "corrPvalue",
                url: path3,
                type: "csv",
                optional: true,
                loaded(){}
            }
        },
        setup() {
            initialize(this);
            processMatrix(this);
            processSubmatrix(this);
            loadMatrix(this);
            loadAxis(this);
            loadSubmatrix(this);
            registerEditorConfig(editorConfig(this),"getVue1",plot_name);
            //registerEditorConfig(MODULE_NAME, editorConfig(this), [], editorRef);
        }
    });
}

// register(MODULE_NAME, init);
// export function registerSpatialCorrelation(){
//     register(MODULE_NAME, init);
// }