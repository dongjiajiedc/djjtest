class CreateSamples < ActiveRecord::Migration[6.0]
  def change
    create_table :samples do |t|
      t.string :sample_name
      t.string :project_name
      t.string :description
      t.string :species_name
      t.string :cancer_name
      t.string :disease_name
      t.string :c_study_type
      t.string :c_country
      t.string :c_ethnic
      t.string :c_sex
      t.integer :n_age
      t.string :c_age_unit
      
      t.string :c_tumor_subtype
      t.string :c_tumor_grade
      t.string :c_tumor_stage
      t.string :c_tumor_type
      t.string :c_diease_name
      t.string :c_cancer_type 
      t.integer :n_os
      t.string :c_os_unit
      t.string :c_os_status
      t.integer :n_pfs
      t.string :c_pfs_unit
      t.string :c_pfs_status

      t.integer :num_of_slices
      t.integer :num_of_spots
      t.integer :num_of_genes
     
      t.references :project, null: false, foreign_key: true

      t.timestamps
    end
  end
end
