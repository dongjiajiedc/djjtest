import Crux from "crux";
import * as d3 from "d3";
import { ColorSchemeNumeric } from "../../spatial-common/defs";
import { registerEditorConfig } from "../../utils/editor";
import { BIconChevronDoubleLeft } from "bootstrap-vue";

//general settings
const width = 1050;
const height = 600;
const graphSize = 400;
const wordLen = 85;
const padding = 210;
//colors
const minColor = String(RgbToHex(34, 103, 173));
const maxColor = String(RgbToHex(178, 24, 44));
//initialization
const type = "lower";
const pos = "right";
const display = "color";
const line = "curve";
const shape = "square"
const varied_size = false

function RgbToHex(a, b, c) {
    var r = /^\d{1,3}$/;
    if (!r.test(a) || !r.test(b) || !r.test(c)) 
    return window.alert("wrong input rgb");
    var hexs = [a.toString(16), b.toString(16), c.toString(16)];
    for (var i = 0; i < 3; i++) if (hexs[i].length == 1) hexs[i] = "0" + hexs[i];
    return "#" + hexs.join("");
}

// can change the scale here
function getColorScheme(minColor, maxColor){
    return new ColorSchemeNumeric([minColor, "white", maxColor], { type: "linear", domain: [-1, 0, 1]});
}


export function constructColors(v: any, c: any){
    const colors = v.data.colorScheme.colors;
    const scheme = getColorScheme(colors.min, colors.max);
    v.data.colorScheme = {scheme, colors};
    v.defineGradient("bg_pos", "vertical", [colors.max, "white"]);
    v.defineGradient("bg_neg", "vertical", ["white", colors.min]);
}


export function initialize(v: any){
    console.log('-----')
    console.log(v)
    //v.size
    v.size.width = width;
    v.size.height = height;
    //v.data
    v.data.width = width;
    v.data.height = height;
    v.data.graphSize = graphSize;
    v.data.wordLen = wordLen;
    v.data.padding = padding;
    //matrix and axis
    v.data.matrix = {type, shape, varied_size};
    v.data.axis = {pos};
    v.data.submatrix = {display, line};
    //colors 
    const colors =  {"min": minColor, "max": maxColor};
    const scheme = getColorScheme(minColor, maxColor);
    v.data.colorScheme = {scheme, colors};
    v.defineGradient("bg_pos", "vertical", [maxColor, "white"]);
    v.defineGradient("bg_neg", "vertical", ["white", minColor]);
    //choices
    v.data.display_submatrix = true
    v.data.display_p_value = true
}


export function processMatrix(v:any){
    const d = v.data.main_matrix;
    const instanceNames = d.columns.slice(1, d.columns.length);
    const len = instanceNames.length;
    
    const matrixData = [];
    d.forEach(dat => {
        const temp = [];
        instanceNames.forEach(key => {
            temp.push(parseFloat(dat[key]));
        });
        matrixData.push(temp);
    });

    const p = v.data.p_value;
    const pvalueData = [];
    p.forEach(dat => {
        const temp = [];
        instanceNames.forEach(key => {
            temp.push(parseFloat(dat[key]));
        });
        pvalueData.push(temp);
    });

     const type = v.data.matrix.type;
     const shape = v.data.matrix.shape;
     const shape_varied = v.data.matrix.shape_varied;
     const displayedData = [];
     if ( type == "full"){
         for (let i = 0; i < len; i++) {
             for (let j = 0; j < len; j++) {
                 const elem = {data: 0, p: 0, row: 0, col: 0};
                 elem.data = matrixData[i][j];
                 elem.p = Math.abs(pvalueData[i][j]);
                 elem.row = i;
                 elem.col = j;
                 displayedData.push(elem);
             }
         }
     }
     else {
         for (let i = 0; i < len; i++) {
             for (let j = 0; j < i; j++) {
                 const elem = {data: 0, p: 0, row: 0, col: 0};
                 elem.data = matrixData[i][j];
                 elem.p = Math.abs(pvalueData[i][j]);
                 elem.row = i;
                 elem.col = j;
                 displayedData.push(elem);
             }
         }
     }

    v.data.gridWidth = graphSize/len;
    v.data.dataLen =  len;
    v.data.result = {instanceNames, matrixData};
    v.data.matrix = {type, shape, varied_size, displayedData}
}


export function processSubmatrix(v:any){
    const d = v.data.sub_matrix;
    const submatrix_names = d.columns.slice(1, d.columns.length);
    const len_col = submatrix_names.length;
    
    const submatrix_data = [];
    d.forEach(dat => {
        const temp = [];
        submatrix_names.forEach(key => {
            temp.push(parseFloat(dat[key]));
        });
        submatrix_data.push(temp);
    });

    const len_row = submatrix_data.length
    const displayed_data = []
    for (let i = 0; i < len_row; i++) {
        for (let j = 0; j < len_col; j++) {
            const elem = {data: 0, row: 0, col: 0};
            elem.data = submatrix_data[i][j];
            elem.row = i;
            elem.col = j;
            displayed_data.push(elem);
        }
    }

    v.data.sub_gridWidth = graphSize/len_col;
    v.data.sub_dataLen = len_col;
    v.data.sub_result = {submatrix_names, submatrix_data};
    v.data.submatrix = {display, line, displayed_data};
}


export function loadMatrix(v: any){
    //type is changed dynamically by the editor
    const type = v.data.matrix.type;
    const shape = v.data.matrix.shape;
    const displayedData = v.data.matrix.displayedData;
    var general_angle, graph_width, graph_height, padding_x, padding_y;
    var x_angle, x_dir, y_angle, y_dir, y_padding_x, y_padding_y, x_padding_x, x_padding_y;
    var default_axis_pos, axis_x, axis_y, x_apadding_x, x_apadding_y, y_apadding_x, y_apadding_y;
    
   //default setting
    if(type == "full"|| type == "lower")
    {
        general_angle = 0;
        graph_width = wordLen + graphSize;
        graph_height = wordLen + graphSize;
        padding_x = (width - graph_width) * 0.2+ wordLen;
        padding_y = (height - graph_height) * 0.1;
        //y axis
        y_angle = 0, y_dir = "right";
        y_padding_y = 0;
        y_padding_x = -5;
        //x axis
        x_angle = -90, x_dir = "right";
        x_padding_y = graphSize + 5;
        x_padding_x = 0;
        //color axis
        default_axis_pos = "right";
        axis_x = axis_y = graphSize;
        x_apadding_x = padding_x + axis_x;
        x_apadding_y = padding_y;
        y_apadding_x = padding_x + axis_x;
        y_apadding_y = padding_y + graph_height;
    }
    else if(type == "upper")
    {
        general_angle = 180;
        graph_width = wordLen + graphSize;
        graph_height = wordLen + graphSize;
        padding_x = (width - graph_width) * 0.5 + graphSize;
        padding_y = (height - graph_height) * 0.5 + graphSize;
        //y axis
        y_angle = 180, y_dir = "left";
        y_padding_y = 0;
        y_padding_x = -5;
        //x axis
        x_angle = -90, x_dir = "right";
        x_padding_y = graphSize + 5;
        x_padding_x = 0;
        //color axis
        default_axis_pos = "left";
        axis_x = axis_y = graphSize;
        x_apadding_x = padding_x - graphSize + graph_width;
        x_apadding_y = padding_y - graphSize;
        y_apadding_x = padding_x - graphSize + axis_x;
        y_apadding_y = padding_y - graphSize + axis_y;
    }
    else if ( type  == "triangle")
    {
        general_angle = 135;
        graph_width = (graphSize + wordLen) * Math.sqrt(2);
        graph_height = (graphSize +  wordLen)/ Math.sqrt(2) - 10;
        padding_x = 0.5 * (graphSize * Math.sqrt(2) + width)
        padding_y = 0.5 *(graph_height + height);
        //y axis
        y_angle = -180, y_dir = "left";
        y_padding_y = 0;
        y_padding_x = -5;
        //x axis
        x_angle = -90, x_dir = "right";
        x_padding_y = graphSize + 5;
        x_padding_x = 0;
        //color axis
        default_axis_pos = "bottom";
        axis_x = graphSize* Math.sqrt(2);
        axis_y = graphSize/Math.sqrt(2);
        x_apadding_x = 0.5 * (width + graph_width);
        x_apadding_y = padding_y - axis_y;
        y_apadding_x = padding_x;
        y_apadding_y = padding_y;
    }
    else if ( type == "inverted triangle")
    {
        general_angle = -45;
        graph_width = (graphSize + wordLen) * Math.sqrt(2);
        graph_height = (graphSize +  wordLen)/Math.sqrt(2) - 10;
        padding_x = 0.5 * (width - graphSize * Math.sqrt(2) )
        padding_y = 0.5 *(height - graph_height);
        //y axis
        y_angle = 0, y_dir = "right";
        y_padding_y = 0;
        y_padding_x = -5;
        //x axis
        x_angle = 90, x_dir = "left";
        x_padding_y = graphSize + 5;
        x_padding_x = 0;
        //color axis
        default_axis_pos = "top";
        axis_x = graphSize* Math.sqrt(2);
        axis_y = graphSize/Math.sqrt(2);
        x_apadding_x = 0.5 * (width + graph_width);
        x_apadding_y = padding_y;
        y_apadding_x = padding_x + axis_x;
        y_apadding_y = padding_y + graph_height;
    }
    
    v.data.matrix = {
        //TODO: change displayedData to displayed_data, add connected_data
        type, shape, varied_size, displayedData, general_angle, graph_width, graph_height, padding_x, padding_y,
        default_axis_pos, axis_x, axis_y, x_apadding_x, x_apadding_y, y_apadding_x, y_apadding_y,
        x_angle, x_dir, x_padding_x, x_padding_y, y_angle, y_dir, y_padding_x, y_padding_y
    };
}


export function loadAxis(v: any){
    const pos = v.data.axis.pos;
    const graph_height = v.data.matrix.graph_height;
    const graph_width = v.data.matrix.graph_width;
    const axis_x = v.data.matrix.axis_x;
    const axis_y = v.data.matrix.axis_y;
    const x_apadding_x = v.data.matrix.x_apadding_x;
    const x_apadding_y = v.data.matrix.x_apadding_y;
    const y_apadding_x = v.data.matrix.y_apadding_x;
    const y_apadding_y = v.data.matrix.y_apadding_y;
    var axis_angle, axis_length, apadding_x, apadding_y;

    if(pos == "right") {
        axis_angle = 0;
        axis_length = axis_y;
        apadding_x = x_apadding_x + padding 
        apadding_y = x_apadding_y;
    }
    else if(pos == "left"){
        axis_angle = 0;
        axis_length = axis_y;
        apadding_x = x_apadding_x - graph_width - padding - 30;
        apadding_y =  x_apadding_y
    }
    else if(pos == "top"){
        axis_angle = 90;
        axis_length = axis_x;
        apadding_x = y_apadding_x;
        apadding_y = y_apadding_y - graph_height - padding - 30;
       
    }
    else if(pos == "bottom"){
        axis_angle =  90
        axis_length = axis_x;
        apadding_x = y_apadding_x;
        apadding_y = y_apadding_y + padding;
    }
    v.data.axis = {pos, axis_angle, axis_length, apadding_x, apadding_y};
}


export function loadSubmatrix(v: any){
    const display = v.data.submatrix.display;
    const line = v.data.submatrix.line;
    const displayed_data = v.data.submatrix.displayed_data;
    var sub_dir;
    if (v.data.matrix.y_dir == "right")
        sub_dir = "left"
    else
        sub_dir = "right"
    v.data.submatrix = {display, line, displayed_data, sub_dir};
}