import { EditorDef, registerEditorConfig } from "../../utils/editor";
import { editorSectionFactory, warpIf, editorTabFactory} from "../../spatial-common/editor";
import { genDefaultPalette, withDefaultPalette } from "oviz-common/palette";
import Vue from 'vue/dist/vue.esm.js'
import { defineMinMaxColors, defineClusters, defineClusterColors, processData, setDisplayScale} from "./data"

export const editorRef = {} as any;
const conf: any = {
    shape_choice: ["square", "triangle", "circle"],
    scale_choice: ['600 * 600', '900 * 900', '1200 * 1200', '1800 * 1800'],
};

export function copyObject(obj: any) {
    const result = {};
    Object.entries(obj).forEach(([k, v]) => result[k] = v);
    return result;
} 

export function reloadEditor(v: any){
    registerEditorConfig(editorConfig(v));
}

export function getSearchBox(v:any, n: any){
    return Vue.component('gene-search', {
        data: function() {
            return {
                title: n,
                genes: v.data[n + '_info'].all_genomes,
                input: '',
                selected: [],
            }
        },
        computed: {
            searchGene: function(){
                var str = this.input;
                const str_lower = str.toLowerCase();
                const str_upper = str.toUpperCase();
                return this.genes.filter(g=>g.includes(str)||g.includes(str_lower)||g.includes(str_upper));
            }
        },
        methods: {
            submitGene: function(){
                processData(v, n, this.input);
                v.run();
            },
            selectGene: function(){
                this.input = this.selected[0];
            },
            clearGene: function(){
                this.input = ''
            }
        },
        template: `
        <div>
            <p title="Gene_Search" v-once>Search for gene in '{{title}}'</p>
            <table>
                <tr>
                    <input v-model="input"
                        id="new-todo"
                        placeholder="E.g. NEAT1"
                        style = "width: 223px;
                                    background-color: white;
                                    color: black;
                                    border-radius: 3px;">
                    <select v-on:click.prevent="selectGene" 
                            v-model="selected" 
                            multiple
                            style="width: 223px;
                                background-color: #6C757D;
                                color: white;
                                border-radius: 3px;" >
                        <option v-for="n in searchGene">{{ n }}</option>
                    </select>
                </tr>
                <tr>
                    <button v-on:click.prevent="submitGene" 
                            style="border-radius: 3px; 
                                background-color: #6C757D;
                                color: white;
                                padding: 0px 15px;">Apply</button>
                    <button v-on:click.prevent="clearGene" 
                            style="border-radius: 3px;
                                background-color: #6C757D;
                                color: white;
                                padding: 0px 15px;">Clear</button>
                </tr>
            </table>
        </div>`,
        style:`
        ::placeholder {
            color: red;
            opacity: 1; /* Firefox */
        }`
    })
}

export function getSlider(v:any, a:any, init_a:any){
    return Vue.component('slider',{
        data: function() {
            return {
                title: a,
                angle: '',
                init_angle: init_a
            }
        },
        methods: {
            changeAngle: function(){
                v.data.spatial_3d[a]  = this.angle;
                console.log(this.angle)
                v.run();
            },
        },
        template: `
        <div class="slidecontainer">
            <p title="Angle Slider" v-once>Angle Slider for {{title}} </p>
            <input type="range" 
                min="-180" 
                max="180" 
                value="{{init_angle}}"
                class="slider" 
                v-model="angle" 
                v-on:click.prevent="changeAngle" >
        </div>`
    })
}

export function editorConfig(v: any): EditorDef {
    conf.shape = v.data.spatial_shape.shape
    conf.varied_size = v.data.spatial_shape.varied_size

    //clusters
    conf.cluster_display = v.data.spatial_cluster.cluster_display
    conf.cluster_key = v.data.spatial_cluster.cluster_key
    conf.cluster_choice_list = v.data.spatial_cluster.cluster_columns_all
    
    //conf.cluster_method = v.data.spatial_cluster.cluster_method
    conf.display_3d = v.data.spatial_3d.display_3d
    conf.ang_A = v.data.spatial_3d.ang_A
    conf.ang_B = v.data.spatial_3d.ang_B
    
    conf.display_scale = '600 * 600'
    conf.image_portal = v.data.image_portal
    
    const [defaultPaletteExp, paletteMapExp] = genDefaultPalette(v.data.spatial_color.exp_colors)
    const [defaultPaletteCluster, paletteMapCluster] = genDefaultPalette(v.data.spatial_cluster.cluster_scheme.colors)

    return {
        sections: [
            editorSectionFactory(v, "General Settings", "tabs", [
                editorTabFactory(v, "Gene", "list", [
                    {
                        title: "Search for v1 gene",
                        type: "vue",
                        component: getSearchBox(v, v.data.displayed_data_list[0])
                    }, 
                    {
                        title: "Search for v2 gene",
                        type: "vue",
                        component: getSearchBox(v, v.data.displayed_data_list[1])
                    }
                ]),
                editorTabFactory(v, "Spatial", "list", [
                    {
                        title: "Display spots in varied size",
                        type: "checkbox",
                        bind: {
                            object: conf, path: "varied_size",
                            callback: ()=>{
                                v.data.spatial_shape.varied_size = conf.varied_size;//returns the different options
                                v.run();
                            }
                        }
                    },
                    {
                        title: "Select shape of spots",
                        type: "select",
                        options: conf.shape_choice,
                        bind: {
                            object: conf, path: "shape",
                            callback: ()=>{
                                v.data.spatial_shape.shape = conf.shape;//returns the different options
                                v.run();
                            }
                        }
                    },
                    {
                        title: "Display spots in clusters",
                        type: "checkbox",
                        bind: {
                            object: conf, path: "cluster_display",
                            callback: ()=>{
                                v.data.spatial_cluster.cluster_display = conf.cluster_display;//returns the different options
                                v.run();
                            }
                        }
                    },
                    {
                        title: "Select method of clustering",
                        type: "select",
                        options: conf.cluster_choice_list,
                        bind: {
                            object: conf, path: "cluster_key",
                            callback: ()=>{
                                defineClusters(v, conf.cluster_key);
                                defineClusterColors(v)
                                v.run();
                                reloadEditor(v);
                            }
                        }
                    },
                ]),
                editorTabFactory(v, "3d", "list", [
                    {
                        title: "Display plot in 3d",
                        type: "checkbox",
                        bind: {
                            object: conf, path: "display_3d",
                            callback: ()=>{
                                v.data.spatial_3d.display_3d = conf.display_3d;//returns the different options
                                v.run();
                            }
                        }
                    },
                    {
                        title: "Change angle A of the 3d plot",
                        type: "vue",
                        component: getSlider(v, 'ang_A', conf.ang_A)
                    },
                    {
                        title: "Change angle B of the 3d plot",
                        type: "vue",
                        component: getSlider(v, 'ang_B', conf.ang_B)
                    }
                ]),
                editorTabFactory(v, "Tissue", "list", [
                    {
                        title: "Input link to image",
                        type: "input",
                        bind: {
                            object: conf, path: "image_portal",
                            callback: ()=>{
                                v.data.image_portal = conf.image_portal
                                v.run();
                            }
                        }
                    },
                    {
                        title: "Resolution options",
                        type: "select",
                        options: conf.scale_choice,
                        bind: {
                            object: conf, path: "display_scale",
                            callback: ()=>{
                                if (conf.display_scale == '600 * 600'){
                                    setDisplayScale(v, 600);
                                }
                                else if (conf.display_scale == '900 * 900'){
                                    setDisplayScale(v, 900);
                                }
                                else if (conf.display_scale == '1200 * 1200'){
                                    setDisplayScale(v, 1200);
                                }
                                else if (conf.display_scale == '1800 * 1800'){
                                    setDisplayScale(v, 1800);
                                }
                                v.run()
                            }
                        }
                    },
                    {
                        title: "Separate spots and image",
                        type: "checkbox",
                        bind: {
                            object: conf, path: "separate_image",
                            callback: ()=>{
                                v.data.separate_image = conf.separate_image
                                v.run();
                            }
                        }
                    },
                ]),
            ]),
            ...warpIf(true, [
                editorSectionFactory(v, "Color Palettes", "single-page", [
                    {
                        type: "vue" as const,
                        title: "",
                        component: "color-picker",
                        data: {
                            title: "Choose Min-Max Color",
                            scheme: copyObject(v.data.spatial_color.exp_colors),
                            palettes: withDefaultPalette(defaultPaletteExp),
                            paletteMap: paletteMapExp,
                            id: "pwcolor",
                            callback(colors) {
                                if (typeof defineMinMaxColors !== "undefined") {
                                    defineMinMaxColors(v, colors);
                                }
                                v.run();
                            },
                        },
                    },
                    {
                        type: "vue" as const,
                        title: "",
                        component: "color-picker",
                        data: {
                            title: "Choose Cluster Color",
                            scheme: copyObject(v.data.spatial_cluster.cluster_scheme.colors),
                            palettes: withDefaultPalette(defaultPaletteCluster),
                            paletteMap: paletteMapCluster,
                            id: "pwcolor",
                            callback(colors) {
                                if (typeof defineClusterColors !== "undefined") {
                                    defineClusterColors(v, colors)
                                }
                                v.run();
                            },
                        },
                    },
                ]),
            ]),
        ],
    };
}