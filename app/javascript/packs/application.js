import 'bootstrap';
import Vue from 'vue';
import $ from 'jquery';
import VJstree from 'vue-jstree';
import uploader from 'vue-simple-uploader';

window.jQuery = $;
window.gon = window.gon || {};

Vue.use(VJstree);
Vue.use(uploader);
const ALERT_TIMEOUT = 5000;

import JobSubmit from '../job-submit.vue';
import JobQuery from '../job-query.vue';
import DemoQuery from '../demo-query.vue';

import JobSubmitPipeline from "../job-submit-pipeline.vue";
import databaseOverview from "../vapp-database.vue";
import projectFraction from "../pj-show-fraction";
import projectSubtype from "../pj-show-subtype";
import tutorial from '../tutorial.vue';
import tutorial_analysis from '../tutorial_analysis.vue';
import sc_graph_1 from "../sc-graph-1.vue";
import sc_graph_2 from "../sc-graph-2.vue";
import sc_graph_3 from "../sc-graph-3.vue";
import sc_graph_4 from "../sc-graph-4.vue";
import index_table from "../index_table.vue";


import VApp from "page/vapp.vue";

// import EditText from "oviz-components/edit-text-vue.vue";
import FilterSamplesBind from "oviz-components/filter-samples-bind.vue";
import FilterSamples from "oviz-components/filter-samples.vue";

import ColorPicker from "page/builtin/color-picker.vue";
import SectionFiles from "page/builtin/section-files.vue";



import { registerDefaultEditorConfig } from "utils/editor";

Vue.component("filter-samples", FilterSamples);
Vue.component("filter-samples-bind", FilterSamplesBind);
Vue.component("color-picker", ColorPicker);
Vue.component("section-files", SectionFiles);



function initVApp() {
    if (document.getElementById("vapp")) {
        const vapp = new Vue({
            el: document.getElementById("vapp"),
            render: h => h(VApp),            
        });
        registerDefaultEditorConfig();
    }
}

document.addEventListener("turbolinks:load", initVApp);
document.addEventListener("DOMContentLoaded", initVApp);

document.addEventListener('DOMContentLoaded', (event) =>  {
    const vueLoadList = [
        ['#vapp-job-submit', JobSubmit],
        ['#vapp-job-query', JobQuery],
        ['#vapp-job-submit-pipeline', JobSubmitPipeline],
        ['#vapp-database', databaseOverview],
        ['#pj_subtype', projectSubtype],
        ['#pj_fraction', projectFraction],
        ['#tutorial',tutorial],
        ['#vapp-demo-query', DemoQuery],
        ['#tutorial_analysis',tutorial_analysis],
        ['#sc_graph_1',sc_graph_1],
        ['#sc_graph_2',sc_graph_2],
        ['#sc_graph_3',sc_graph_3],
        ['#sc_graph_4',sc_graph_4],
        ['#index_table',index_table],
    ];

    $('[data-toggle="tooltip"]').tooltip({
        html:true,
        container: 'body',

    });
    $('[data-toggle="popover"]').popover({
        html:true,
        container: 'body',
    });
    //$('.selectpicker').selectpicker();
    $('.carousel').carousel();


    $('#alerts .alert-group').each((i, el) => {
        const alertGroup = $(el);
        const bar = alertGroup.find('.progress-bar');

        alertGroup.find('.close').on('click', () => {
            alertGroup.slideUp(300);
        });

        bar.css('width', '100%');
        setTimeout(() => {
            alertGroup.slideUp(300);
        }, ALERT_TIMEOUT);
    });

    vueLoadList.forEach(([selector, component, props = {}]) => {
        const el = document.querySelector(selector);
        if (!el) return;
        const _ = new Vue({
            el,
            render: h => h(component, { props }),
        });
    });

    $('.sidebar-toggle').on('click', () => {
        $(document.getElementById("sidebar")).toggleClass('active');

    });

    $('#alerts .alert-group').each((i, el) => {
        const alertGroup = $(el);
        const bar = alertGroup.find('.progress-bar');

        alertGroup.find('.close').on('click', () => {
            alertGroup.slideUp(300);
        });

        bar.css('width', '100%');
        setTimeout(() => {
            alertGroup.slideUp(300);
        }, ALERT_TIMEOUT);
    });

    $('.cookies-eu-button').on('click', () => {
        $('#cookies-eu-banner').css("display", "None")
    })
});



