class Disease < ApplicationRecord
    validates :disease_name, presence: true, uniqueness: { 
        message: ->(object, data) do
          "disease #{data[:value]} already exists. "
        end
      }, on: :create
    
      def self.import(file)
          CSV.foreach(file.path, headers: true, encoding: 'bom|utf-8') do |row|
          disease = Disease.find_by(disease_name: row['disease_name']) || new
          disease.attributes = row.to_hash.slice(*column_names)
  
          disease.save!
          end
      end

    def self.to_csv(options={})
      CSV.generate(options) do |csv|
        csv << column_names
        all.each do |disease|
          csv << disease.attributes.values_at(*column_names)
        end
      end
    end

    def self.selected_to_csv(disease_ids, options={})
      CSV.generate(options) do |csv|
        csv << column_names
        disease_ids.each do |id|
          disease = Disease.find(id)
          csv << disease.attributes.values_at(*column_names)
        end
      end
    end
end
