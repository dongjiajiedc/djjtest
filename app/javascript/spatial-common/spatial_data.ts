import Oviz from "crux";
import { ColorScheme, ColorSchemeCategory, ColorSchemeGradient } from "crux/dist/color";
import { minmax } from "crux/dist/utils/math";
import { scaleLinear } from "d3-scale";
import { groupedColors2, rainbow1, rainbowL} from "oviz-common/palette";
import { schemeRdPu, interpolateSpectral, schemeRdYlBu } from "d3-scale-chromatic";
import { findBoundsForValues} from "utils/maths";
import { parse } from "utils/newick";
import * as d3 from "d3";
import { ColorSchemeNumeric } from "./defs";

const defaultScheme = groupedColors2;

export const brewPalette = [
    "#bebada",
    "#fb8072",
    "#80b1d3",
    "#fdb462",
    "#b3de69",
    "#fccde5",
    "#d9d9d9",
    "#bc80bd",
    "#ccebc5",
    "#ffed6f",
    "#8dd3c7",
    "#ffffb3",
];

export function processEmbedding(v){
    v.embeddingData = {}
    Object.keys(v.metaInfo).forEach(meta => {
        if (meta.startsWith('e_') && meta.endsWith('_1')){
            var embed = meta.replace('e_', '').replace('_1', '')
            v.embeddingData[embed] = {}
            var xLabel = meta
            var yLabel = meta.replace('_1', '_2')
            v.embeddingData[embed]['categoryRange'] = [v.metaInfo[xLabel].min, v.metaInfo[xLabel].max]
            v.embeddingData[embed]['valueRange'] = [v.metaInfo[yLabel].min, v.metaInfo[yLabel].max]
            v.embeddingData[embed]['data'] = []
            Object.keys(v.metaDict).forEach(spot => {
                var x = v.metaDict[spot];
                x.color = v.metaInfo['c_seurat_clusters'].colorMap[x['c_seurat_clusters']]   
                v.embeddingData[embed]['data'].push(x)
            }) 
            v.embeddingData[embed]['dataHandler'] = {
                data: {
                    values: e => [],
                    pos: e => e[xLabel],
                    min: e => v.metaInfo[yLabel].min,
                    value: e => e[yLabel],
                }            
            }
        }
    });    
    console.log('---embed---', v)
}
export function processMain(v, d, filteredRows) {
    v.rowsLabel = d.columns[0];
    
    v.rows = [];
    v.rowFullMap = {};
    v.rowMap = {};
    v.colFullMap = {};
    v.colSum = {};

    d.forEach(x => {
        const full_temp = {};
        const temp = {};
        const rowsID = x[v.rowsLabel];
        if (filteredRows.length == 0 || filteredRows.includes(rowsID)){
            Object.keys(x).forEach(spot => {
                var value = parseFloat(x[spot])
                full_temp[spot] = value;
                if (v.filteredSpots.includes(spot)){
                    temp[spot] = value;
                    if (!Object.keys(v.colFullMap).includes(spot)){
                        v.colFullMap[spot] = {};
                        v.colSum[spot] = 0;   
                    }
                    v.colFullMap[spot][rowsID] = value; 
                    v.colSum[spot] += value;
                }
            });
            v.rowFullMap[rowsID] = full_temp;
            v.rowMap[rowsID] = temp;
            v.rows.push(rowsID)
        }
    });

    v.mainHeatmap = [];
    v.rows.forEach(s => {
        v.mainHeatmap.push(Object.values(v.rowMap[s]))
    });

    v.mainGradientFills = [];
    const [min, max] = minmax(v.mainHeatmap.flat());
    v.mainRange = [min, max];
    // const gradientColors = ["#041866", 
    //     "#435497", "#98c4d1",
    //     "#be3035", "#f3e567"];
    //const gradientColors = ["#041866", "#d4c1e6", "#ba1600"];
    const gradientColors = schemeRdYlBu[5].reverse();
    const valueDiv = (v.mainRange[1] - v.mainRange[0]) / 4;
    const values = [];
    for (let i = 0; i < 5; i++) {
        values.push(v.mainRange[0] + i * valueDiv);
    }
    const gradient = d3.scaleLinear()
        // .range([v.colors.start, this.colors.end])
        // .domain(this.mainRange);
        .range(gradientColors)
        // .domain([this.mainRange[0], (this.mainRange[1] + this.mainRange[0])/2
        //             , this.mainRange[1]]);
        .domain([0, v.mainRange[1] / 4, v.mainRange[1]*2 / 4,
            v.mainRange[1]*3 / 4,
            v.mainRange[1]]);
        // .domain(values);
        /* @debug
    this.debug.scale1 = (x) => d3.scaleLinear().range([0, 200])
                            .domain([-5, 2])(x);
    this.debug.scale2 = (x) => d3.scaleLinear().range([0, 200])
                .domain([min, max])(x);
    const debug = this.mainHeatmap.flat().filter(x => x > 0).sort();
    this.debug.data = [];
    for (let i = 0; i < debug.length; i += 6) {
        this.debug.data.push([computeLog(debug[i]), debug[i]]);
    }*/
    const div = (v.mainRange[1] - v.mainRange[0]) / 20;
    for (let i = 0; i <= 20; i ++) {
        v.mainGradientFills.push(gradient(v.mainRange[0] + i * div));
    }
    v.mainColorGetter = (d) => {
        if (d === 0)
            return v.colors.abd0;
        else {
            //return gradient(computeLog(d));
            return gradient(d);
        }
    };
    // this.mainColorGetter = (d) => gradient(computeLog(d + 1));
    v.valueRange = [0, max];

    console.log('----main---', v)
}

export function processPie(m){ // cell_type * spot fraction matrix
    var pieData = {}
    pieData['rows'] = []
    m.forEach(d => {
        var row = d[m.columns[0]];
        pieData['rows'].push(row);
        Object.keys(d).forEach((spot, col_i) => {
            if (col_i != 0){
                if (!Object.keys(pieData).includes(spot)){
                    pieData[spot] = []
                }
                pieData[spot].push({
                    name: row, // row name
                    value: parseFloat(d[spot])
                })
            }
        });
    });
    pieData['colorScheme'] = ColorSchemeCategory.create(pieData['rows'], 
    pieData['rows'].map((_, i) => interpolateSpectral(i/pieData['rows'].length)));
    //console.log(pieData, pieData['colorScheme'])
    return pieData
}

export function processHist(v, d){
    
    v.hist = {
        indexes: v.rows,
        result: {}
    };
    
    v.rows.forEach(k => {
        v.hist.result[k] = Object.keys(v.rowMap[k])
                            .map(x => [x, v.rowMap[k][x]]);
    });

    v.hist.colorMap = {};
    // const palette = scaleLinear().domain([0, 1, 2]).range(paletteColors);
    v.rows.forEach((x, i) => {
        // v.hist.colorMap[x] = rainbowL[i];
        v.hist.colorMap[x] = interpolateSpectral(i/v.rows.length)
    });

    console.log('----hist---', v)
}

export function processComparison(v) {

    v.category_key = 'c_seurat_clusters'
    const categories = v.metaInfo[v.category_key].values;

    // construct violin plot
    v.comparisonData = {}
    v.rows.forEach(marker => {
        var min = 0
        var max = 0
        const violins = [];
        const values = [];
        const scatterData = [];
        categories.forEach(cate => {
            const temp = [];
            Object.keys(v.rowFullMap[marker]).forEach(s => {
                if (v.filteredSpots.includes(s) && v.metaDict[s][v.category_key] === cate) {
                    temp.push(parseFloat(v.rowFullMap[marker][s]));
                }
            });
            if(temp.length > 0){
                //console.log(cate, temp.length)
                const stat = new Oviz.algo.Statistics(temp);
                const bins = new Oviz.algo.Histogram(temp, "count").getBins();
                const maxY = new Oviz.algo.Histogram(temp, "count").getMax();
                violins.push({bins, maxY, stat});
                values.push([stat.min(), stat.max()]);
        
                // calculate min max
                if (stat.min() < min){
                    min = stat.min()
                }
                if (stat.max() > max){
                    max = stat.max()
                }
                scatterData.push({key: cate, values: temp});
            }        
        });
        v.comparisonData[marker] = {
            valueRange: [min - max/5, max + max/5],
            violinData: {values, violins, categories},
            scatterData: scatterData
        }
    });
    console.log('---comparison---', v)
}

export function processGraph(v) {
    var d = v.graphData
    
    v.graphLabel = d.columns.slice(1, d.length + 1)

    var matrix = [];
    d.forEach(row => {
        var arr = [];
        Object.keys(row).forEach(k => {
            if(k != ''){
                arr.push(parseFloat(row[k]))
            }
        });        
        matrix.push(arr)
    });
    v.graphMatrix = matrix

    v.graphCord = d3.chord()
        .padAngle(0.05)     // padding between entities (black arc)
        .sortSubgroups(d3.descending)
        (matrix)

    v.graphMatrixScheme = ColorSchemeNumeric.create([schemeRdPu[3][0], schemeRdPu[3][2]], { type: "linear", domain: minmax(v.graphMatrix.flat())});

    v.nodeRadius = 10
    v.graphRadius = 150
    v.graphNode = {}
    v.graphCord.groups.forEach((x, i) => {
        var label = v.graphLabel[x.index]
        if (!Object.keys(v.graphNode).includes(label)){
            x.label = label
            x.innerRadius = v.graphRadius - v.nodeRadius
            x.outerRadius = v.graphRadius
            x.circleAngle = x.index * 360/v.graphLabel.length
            x.color = interpolateSpectral(i/v.graphLabel.length);

            v.graphNode[label] = x
        }
    });
    console.log('---graph---', v)
}

export function processLeftBox(v) {
        // compute left boxplot
        let categories = [...v.rows];
        const classifications = v.metaInfo["cluster_1"].values;
        const boxData = [{values: [], outliers: [], means: [], categories}, {values: [], outliers: [], means: [], categories}];
        const allValues = [];
        const meanDict = {};
    
        categories.forEach((c, i) => {
            const initialData = [];
            v.samples.forEach(s => {
                allValues.push(v.rowFullMap[c][s]);
                if (v.metaDict[s]['cluster_1'] === classifications[0]) {
                    initialData.push(v.rowFullMap[c][s]);
                }
            });
            const result = [];
            const stat1 = new Oviz.algo.Statistics(initialData);
            const interQuartileRange = stat1.Q3() - stat1.Q1();
            initialData.forEach(d => {
                if (!((d < stat1.Q3() - 1.5 * interQuartileRange) || (d > stat1.Q3() + 1.5 * interQuartileRange))) {
                    result.push(d);
                }
            });
            const stat2 = new Oviz.algo.Statistics(result);
            meanDict[c] = stat2.mean();
        });
    
        v.rows = categories = categories.sort((a, b) => meanDict[a] - meanDict[b]);
        categories.forEach((c, i) => {
            const initialData = [[], []];
            v.samples.forEach(s => {
                allValues.push(v.rowFullMap[c][s]);
                if (v.metaDict[s]['cluster_1'] === classifications[0]) {
                    initialData[0].push(v.rowFullMap[c][s]);
                    // initialData[0].push(Math.log10(parseFloat(rowFullMap[c][s])));
                } else {
                    initialData[1].push(v.rowFullMap[c][s]);
                    // initialData[1].push(Math.log10(parseFloat(rowFullMap[c][s])));
                }
            });
            classifications.forEach((_, j) => {
                const result = [];
                const stat1 = new Oviz.algo.Statistics(initialData[j]);
                const interQuartileRange = stat1.Q3() - stat1.Q1();
                initialData[j].forEach(d => {
                    if ((d < stat1.Q3() - 1.5 * interQuartileRange) || (d > stat1.Q3() + 1.5 * interQuartileRange))  {
                        boxData[j].outliers.push([i, d]);
                    } else {
                        result.push(d);
                    }
                });
                const stat2 = new Oviz.algo.Statistics(result);
                boxData[j].values.push([stat2.min(), stat2.Q1(), stat2.median(), stat2.Q3(), stat2.max()]);
                boxData[j].means.push(stat2.mean());
            });
        });
        
        const valueRange = findBoundsForValues(allValues, 2);
        v.boxplot = {categories: v.rows, classifications, boxData, valueRange};
}


export function processMarker(v) {
    const d = v.markerData
    const markerIdKey = d.columns[0];
    v.markers = d.map(x => x[markerIdKey]);
    v.markerFeatures = d.columns.slice(1, d.columns.length);

    v.filteredMarkers = []
    v.markerClusters = []
    v.markerVolcanoData = {}
    var APMin = 1;
    var APMax = 0;
    var FCMin = Number.MAX_VALUE;
    var FCMax = Number.MIN_VALUE;
    d.forEach(x => {
        x['avg_log2FC'] = parseFloat(x['avg_log2FC'])
        x['p_val_adj'] = parseFloat(x['p_val_adj'])
        x['p_val'] = parseFloat(x['p_val'])       
        if ((x['avg_log2FC'] >= v.markerFCCutOff ||
            x['avg_log2FC'] <= -v.markerFCCutOff) &&
            x['p_val_adj'] <= v.markerAPCutOff
        ){
            v.filteredMarkers.push(x[markerIdKey])
        }
        x.color = 'gray'
        if (x['avg_log2FC'] >= v.markerFCCutOff && x['p_val_adj'] <= v.markerAPCutOff){
            x.color = 'orange'
        }
        if (x['avg_log2FC'] <= -v.markerFCCutOff && x['p_val_adj'] <= v.markerAPCutOff){
            x.color = 'blue'
        }
        var c = x['seurat_clusters']
        if (!v.markerClusters.includes(c)){
            v.markerClusters.push(c)
            v.markerVolcanoData[c] = {data:[x]}
        }else{
            v.markerVolcanoData[c].data.push(x)
        }
        if (x['avg_log2FC'] > FCMax){
            FCMax = x['avg_log2FC']
        }
        if (x['avg_log2FC'] < FCMin){
            FCMin = x['avg_log2FC']
        }
        if (x['p_val_adj'] > APMax){
            APMax = x['p_val_adj']
        }
        if (x['p_val_adj'] < APMin){
            APMin = x['p_val_adj']
        }        
    });
    v.markerClusters.sort()
    v.volcanoNCol = Math.ceil(v.markerClusters.length / v.volcanoNRow)
    Object.keys(v.markerVolcanoData).forEach(c => {
        v.markerVolcanoData[c]['categoryRange'] = [prettyPrint(FCMin), prettyPrint(FCMax)]
        v.markerVolcanoData[c]['valueRange'] = [prettyPrint(-Math.log2(APMax)), prettyPrint(-Math.log2(APMin))]
        //v.markerVolcanoData[c]['xLabel'] = 'log2(FC)'
        //v.markerVolcanoData[c]['yLabel'] = '-log2(adjusted p)'
        v.markerVolcanoData[c]['dataHandler'] = {
			result: {
				values: e => [],
				pos: e => e['avg_log2FC'],
				min: e => APMin,
				value: e => e['p_val_adj'],
			}            
        }
    });
    console.log('----marker---', v)
}
export function prettyPrint(x: any){
    if (parseInt(x) == x){
        return x;
    }else{
        return x.toFixed(2);
    }
}
export function processMeta(v, filteredSpots = null) {
    // process MetaData
    const d = v.spotMetaData
    const sampleIdKey = d.columns[0];
    v.samples = d.map(x => x[sampleIdKey]);
    v.metaFeatures = d.columns.slice(1, d.columns.length);
    v.metaDict = {};
    d.forEach(x => {
        const sampleId = x[sampleIdKey];
        const new_x = [];
        Object.keys(x).forEach(k => {
            if (k != sampleIdKey){
                new_x[k] = x[k];
            }
        });
        v.metaDict[sampleId] = new_x;
    });

    if (filteredSpots === null){
        v.filteredSpots = filterSpot(v)
    }else{
        v.filteredSpots = filteredSpots
    }
    v.metaData = {};
    v.metaInfo = {};
    v.discaredFeatures = [];
    let curPos = 0;    
    v.metaFeatures.forEach((k, i) => {
        //if (!DataUtils.isDistcint(d, k)) {
        if (k.startsWith('n_') || k.startsWith('e_')) {
            const [min, max] = minmax(d.map(x => parseFloat(x[k]))
                .filter(x => !isNaN(x)));
            v.metaInfo[k] = new MetaInfo(k, true, min, max, []);
            v.metaData[k] = v.filteredSpots.map(x => v.metaDict[x][k]);
        } else {
            const values = d.map(x => x[k]).reduce((a, x) => {
                if (a.indexOf(x) < 0 && x !== "NA") a.push(x);
                return a;
            }, []);
            if (values.length > 15) {
                v.discaredFeatures.push(k);
                v.metaFeatures.splice(i, 1);
                // alert(`Meta info "${k}" contains more than 6 categories, will not be drawn`);
            } else {
                v.metaInfo[k] = new MetaInfo(k, false, null, null, values,
                    curPos + values.length <= brewPalette.length ?
                        brewPalette.slice(curPos, curPos + values.length) : null);
                v.metaData[k] = v.filteredSpots.map(x => v.metaDict[x][k]);
                curPos += values.length;
            }
        }
    });
    console.log('----meta---', v)
}

export function filterSpot(data){
    var imageWidth = data.spatialImageConfig.imageWidth;
    var windowWidth = data.spatialImageConfig.windowWidth;
    var windowHeight = data.spatialImageConfig.windowHeight;
    var windowX = imageWidth/2 + windowWidth/2;
    var windowY = imageWidth/2 + windowHeight/2;

    var filteredSpots = [];
    Object.keys(data.metaDict).forEach(spot => {
        var v = data.metaDict[spot];
        var x = parseInt(v['n_pxl_col_in_lowres'])*imageWidth/600;
        var y = parseInt(v['n_pxl_row_in_lowres'])*imageWidth/600;

        if (x >= windowX &&
            x <= windowX + windowWidth &&
            y >= windowY &&
            y <= windowY + windowHeight
            ){
                filteredSpots.push(spot)
            }
    });
    return filteredSpots;
}


class TreeNode {
    protected name: string;
    protected children?: TreeNode[];

    constructor(name: string) {
        this.name = name;
    }

    public addChild(node: TreeNode): TreeNode {
        if (!this.children) this.children = [];
        if (!this.children.find(x => x.name === node.name))
            this.children.push(node);
        return this.children[this.children.length - 1];
    }
    public getName() { return this.name; }
}

export class MetaInfo {
    public static keys = [
        "isNumber",
        "useNumber",
        "useGroup",
        "min",
        "max",
        "groupCount",
        "useThres",
        "thres",
        "minDistinct",
        "maxDistinct",
        "values",
        "colorMap",
        "colorStart",
        "colorEnd",
        "rangeMin",
        "rangeMax",
    ];
    public useNumber = false;
    public useGroup = false;
    public groupCount = 4;
    public useThres = false;
    public thres: number[] = [];
    public minDistinct = false;
    public maxDistinct = false;

    public rangeMin: number;
    public rangeMax: number;
    public colorStart = schemeRdPu[3][0]; //"#f7fcfd";
    // public colorEnd = "#006d2c";
    public colorEnd = schemeRdPu[3][2]; //"#0247FE";
    public colorMap: Record<string, string> = null;

    private simpleKey: string;
    private scheme: ColorScheme;
    private schemeSet;

    constructor(public key: string, public isNumber: boolean, public min: number,
                public max: number, public values: string[], schemeSet = defaultScheme) {
        this.useNumber = isNumber;
        this.rangeMin = min;
        this.rangeMax = max;
        this.simpleKey = key.replace(/\(|\)| /g, "_");
        this.schemeSet = schemeSet;
        this.updateColorGetter();
    }

    public updateColorGetter() {
        if (this.colorMap === null && this.values) {
            this.colorMap = ColorSchemeCategory.create(this.values, this.schemeSet as string[]).colors;
        }
        if (this.useNumber) {
            let opt;
            if (this.useGroup) {
                if (this.useThres) {
                    opt = {
                        type: "threshold",
                        thresholds: this.thres,
                        domain: [this.min, this.max],
                        minDistinct: this.minDistinct,
                        maxDistinct: this.maxDistinct,
                    };
                } else {
                    opt = {
                        type: "quantize",
                        groups: this.groupCount,
                        domain: [this.min, this.max],
                    };
                }
            } else {
                opt = { type: "linear", domain: [this.rangeMin, this.rangeMax] };
            }
            this.scheme = ColorSchemeGradient.create(this.colorStart, this.colorEnd, opt);
            // v.defineGradient(`md_${this.simpleKey}`, "horizontal", [this.colorStart, this.colorEnd]);
        } else {
            this.scheme = new ColorSchemeCategory(this.colorMap);
        }
    }

    public color(c: number | string) {
        if (c === "NA" || Number.isNaN(c) ) return NaN;
        return this.scheme.get(c);
    }

    public legendData() {
        if (this.useNumber && !this.useGroup) {
            return [null, `md_${this.simpleKey}`, this.rangeMin, this.rangeMax];
        }
        const lgData = this.scheme.legendData();
        return this.useNumber ? lgData : this.values.map(v => lgData.find(x => x.label === v));
    }

    public toObject() {
        const obj = { hasValues: !!this.values };
        for (const k of MetaInfo.keys) obj[k] = this[k];
        return obj;
    }

    public update(v: any, obj: any) {
        for (const k of MetaInfo.keys) {
            if (k === "thres" || (k === "valcolorues" && obj[k])) {
                v[k] = [...obj[k]];
            } else {
                v[k] = obj[k];
            }
        }
        v.updateColorGetter();
    }
}

export class GradientBar {
    public colors: string[];
    public stops: number[];
    private getColor: any;
    constructor(colors, stops) {
        this.colors = colors;
        this.stops = stops;
        this.getColor = scaleLinear().range(colors).domain(stops);
    }
}
