class CreateProjects < ActiveRecord::Migration[6.0]
  def change
    create_table :projects do |t|
      t.string :project_name
      t.string :cancer_name
      t.string :database
      t.string :preprocessed
      t.string :major_related_publications
      t.string :publication_link
      t.string :published_year 
      t.string :platform
      t.string :original_description
      t.string :resolution
      t.integer :num_of_samples
      t.integer :num_of_slices
      t.integer :num_of_spots
      t.integer :num_of_genes
      t.integer :num_of_cells
      # t.string :publication_link
      t.string :species

      t.timestamps
    end
  end
end
