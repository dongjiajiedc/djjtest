// export interface Viz {
//     vizOpts: any;
// }

import { registerEmbeddingMap2D } from "./embedding_map_2d";
import { registerEmbeddingMap3D } from "./embedding_map_3d";
import { registerSpotMap3D } from "./spot_map_3d";
import { registerSpotMap2D } from "./spot_map_2d";
import { registerSpatialDeconvolutionPieView } from "./spatial_deconvolution_pie_view";
import { registerSpatialDeconvolutionBarView } from "./spatial_deconvolution_bar_view";

import { registerSpatialCorrelation} from "./spatial_correlation";
// import { registerComparison} from "./comparison";
import { registerMarkerHeatmap } from "./marker_heatmap";
import { registerMarkerComparison } from "./marker_comparison";
import { registerSpatialInteraction } from "./spatial_interaction";

declare global {
    interface GonInfo {
        urls?: any;
        required_data?: any;
        module_name?: string;
        viz_mode?: string;
        analysis_name? :string;
        chosen_output?: number;
    }
    interface Window {
        gon: GonInfo;
        __BVD3_visualizers: Record<string, () => void>;
    }
}
export function registerViz(moduleName) {
    switch (moduleName) {
        case "embedding_map_2d":
            registerEmbeddingMap2D();
            break;
        case "embedding_map_3d":
            registerEmbeddingMap3D();
            break;
        case "spot_map_3d":
            registerSpotMap3D();
            break;
        case "spot_map_2d":
            registerSpotMap2D();
            break;            
        case "spatial_deconvolution_pie_view":
            registerSpatialDeconvolutionPieView();
            break;
        case "spatial_deconvolution_bar_view":
            registerSpatialDeconvolutionBarView();
            break;               
        case "spatial_correlation":
            registerSpatialCorrelation();
            break;
        case "marker_heatmap":
            registerMarkerHeatmap();
            break;
        case "marker_comparison":
            registerMarkerComparison();
            break;   
        case "spatial_interaction":
            registerSpatialInteraction();
            break;
    }

}
