import Crux from "crux";
import {createContinuousColorScheme} from "../../spatial-common/heatmap-data"
import { ColorSchemeNumeric, generateColors, str2int, interpolateTurbo} from "../../spatial-common/defs";
import { interpolateRgb } from "d3-interpolate";
import { ColorSchemeCategory } from "crux/dist/color";
import { createOnUndefined } from "../../spatial-common/support_func";
import { min, xml } from "d3";

const height = 200;
const width = 300;

const idIndex = 0;
var coorIndex = 0;
var typeIndex = 0;//default type

var keys = [];
var types = [];

var share, data;
var combinedData;
var methodKey;

//round max/min values of x and y axes
function setMax(num:number, fix:number){
    var i = fix, fraction = 1;
    while(i--) fraction *= 0.1;
    return (num+fraction).toFixed(fix);
}
function setMin(num:number, fix:number){
    var i = fix, fraction = 1;
    while(i--) fraction *= 0.1;
    return (num-fraction).toFixed(fix);
}

function sortNum(a , b){
    return a-b;
}

//manipulate data
export function process(this: any, result: any[]){
    if(this !=null){
        console.log("data", this);        
        // console.log("meta", this.data.metaData);
        //reverse the columns and rows
        let geneKey = this.data.expressionData.columns[0];
        let geneNames = this.data.expressionData.map(d => d[geneKey]);        
        let arr = []
        this.data.expressionData.columns.slice(1,).forEach(sampleID => {
            let temp = {};
            this.data.expressionData.forEach((d,i) => {
                temp[geneNames[i]] = d[sampleID];
            });
            arr.push(temp);
        });
        // arr.push({"columns":geneKey})
        this.data.expressionData = arr;
        console.log("expression data", this.data.expressionData)
    }
    console.log("result",result)
    keys = Object.keys(result[0]);
    
    var classes= keys.slice(1,).filter(k=>!k.startsWith("e_")&&!k.startsWith("n_"));
    var coorKeys= keys.slice(1,).filter(k=>(k.startsWith("e_")&&!k.endsWith("_3"))||k.startsWith("n_"));
    var idKey = keys[idIndex], classKey = this != null ?classes[typeIndex] : share.classKey;
    var xKey =  this != null ? coorKeys[coorIndex] : coorKeys[share.coorIndex], yKey = this != null ? coorKeys[coorIndex+1] : coorKeys[share.coorIndex+1];
    if (this != null && !this.data.hasOwnProperty("share")){
        data = this.data;
        data.meta = [];//for continuous colorScheme
        this.data.share = {};
        share = this.data.share;
        share.meta = this.data.expressionData;
        share.geneIdx = 0;
        share.coorIndex = coorIndex;
        share.coorKeys = null;
        share.classKey = classKey;
        share.classes = classes;
        share.hexMode = true;
        share.getMean = true;
        share.continuousValue = false;
        share.getDensity = false;
        share.height = height;
        share.width = width;
        share.pointRadius = 3;
        share.hexaWidth = 5+Math.ceil(result.length/100);//20-Math.ceil(result.length/100);
        if(share.hexaWidth>15) share.hexaWidth=15;
        share.radius = share.hexaWidth/Math.sqrt(3)*2;
        share.colorSchemes = {}, share.colors = {};
        share.colorSchemes['meta'] = {};
        share.metaValues = {};
        share.continuousMeta = [];
    }

    //chenge keys after user do selection
    if(share.classes != null){
        var temp = new Array(...share.classes), cValue = classes.filter(c=>c.startsWith("n_"));
        for(var i=0;i<cValue.length;i++)
            for(var j=0;j<temp.length;j++) if(cValue[i].slice(2,) == temp[j]) temp[j] = cValue[i];
        classes = classes.filter(c=>temp.includes(c));
    }
    if(share.coorKeys != null){

        var allMethods = keys.slice(1,).filter(k=>(k.startsWith("e_")&&!k.endsWith("_3"))||k.startsWith("n_")), temp = [];
        for(var idx=0;idx<allMethods.length;idx = idx+2){
            if(share.coorKeys.includes(allMethods[idx].slice(2,-1)) ||
                share.coorKeys.includes(allMethods[idx])){
                temp.push(allMethods[idx]);
                temp.push(allMethods[idx+1]);
            }
        }
        coorKeys = temp;
    }

    combinedData = result;
    if(share.meta != null){//process meta data        
        var meta = share.meta
        if(this!=null){
            this.data.optFiles = createOnUndefined(this.data, "optFiles");
            this.data.optFiles.meta = !(!meta);
        }
        combinedData  = combinedData.map((d, i)=>{
            var temp = {};
            Object.assign(temp, d);
            Object.assign(temp, meta[i]);
            return temp;
        });
        share.geneKey = Object.keys(share.meta[0])
        
        classes.unshift(share.geneKey[share.geneIdx]);
    }
    share.classes = classes;//reset share keys
    share.coorKeys = coorKeys; //share.width = 1200/(coorKeys.length/2*1.5);

    //iteration for multiple categories
    share.methodKeySet = [];
    var size = classes.length+1, numOfMethod = coorKeys.length/2;
    share.method = {};
    console.log("coorKey", coorKeys)
    for(let j=0;j<numOfMethod;j++){
        methodKey = coorKeys[2*j]
        share.methodKeySet.push(methodKey);
        share.method[methodKey] = {}
        xKey = coorKeys[2*j], yKey = coorKeys[2*j+1]
        share.rows = {};//data of categories
        for(let i=0; i<size;i++){
            if(i==size-1){
                share.getDensity = true;
            }
            else share.classKey = classKey = classes[i];
                //update x and y labels
            share.xLabel = xKey.slice(2,);
            share.yLabel = yKey.slice(2,);

            //create color categories for different types of data
            let keySet = [];
            if(classKey.startsWith("n_") || (share.hexMode&&classKey.startsWith("g_")))  share.continuousValue = true;
            else  share.continuousValue = false;
            combinedData.forEach(d=> {
                if(!(d[classKey]===undefined)) keySet.push(d[classKey]);
            });
            //keySet = [...new Set(keySet)];

            //color palette for categorical data
            share.metaValues[classKey] = keySet;
            if(share.colorSchemes['meta'][classKey]==null) {
                const min_color = "#B3E5FC";
                const middle_color = "#29B6F6"
                const max_color = "#01579B";
                share.colorSchemes['meta'][classKey] = i==0? new ColorSchemeNumeric([min_color, middle_color, max_color], { type: "linear", domain: [0,Math.max(...keySet)/2, Math.max(...keySet)]}):
                    ColorSchemeCategory.create(keySet, generateColors(Array.from(new Set(keySet)).length, classKey));
                share.colors[classKey] = {};
                keySet.forEach((key) => {
                    share.colors[classKey][key] = share.colorSchemes['meta'][classKey].get(key);
                })
            }
            // console.log(share.colors)
            // console.log("colorSchemes", share.colorSchemes)

            if(i==0) {
                share.color = new ColorSchemeNumeric(["#367FBA", "white", "#A4444C", "black"], { type: "linear", domain: [0, 2, 6, 10], numLegendPoints: 10 });
            }
            else share.color = share.colors[classKey];
          
            //set array of types for classification

            var xSet = [], ySet = [];
            var points = combinedData.map(function(d) {
                var x = parseFloat(d[xKey]), y = parseFloat(d[yKey]);
                var color = i==0?share.color.get(d[classKey]):share.colors[classKey][d[classKey]], id = d[idKey], value = d[classKey];
                xSet.push(x), ySet.push(y);
                return [x ,y,color,id,value];
            });
            share.points = points;

            var xMax = Math.max(...xSet)+share.hexaWidth,
            yMax = Math.max(...ySet)+share.radius,
            xMin = Math.min(...xSet)-share.hexaWidth,
            yMin = Math.min(...ySet)-share.radius;
            //var dx = (xMax-xMin)/5, dy = (yMax-yMin)/5;
            share.xMax = setMax(xMax,0), share.yMax = setMax(yMax,0);
            share.xMin = setMin(xMin,0), share.yMin = setMin(yMin,0);
            // share.xMax = (xMax+dx).toFixed(2), share.yMax = (yMax+dy).toFixed(2);
            // share.xMin = (xMin-dx).toFixed(2), share.yMin = (yMin-dy).toFixed(2);

            var bins = share.hexMode? hexbin(points) : null;
            share.data = bins;


            if(i==size-1){
                share.continuousValue = true;
                share.rows["density"] ={data:bins, points:share.points,category:"density", color:share.color, continuousValue:share.continuousValue, getDensity: share.getDensity};
                share.getDensity=false;
            }
            else share.rows[classKey] ={data:bins, points:share.points,category:classKey, color:share.color, 
                                                                        continuousValue:share.continuousValue, getDensity: share.getDensity, isGene: !i};
        }
        share.method[methodKey] = {row: share.rows, xLabel: share.xLabel, yLabel: share.yLabel, range:[share.xMin,share.xMax,share.yMin,share.yMax]}
    }
    if(share.colors.cont==null) {
        let contKey = "density";
        share.colors.cont = {};
        //deal with N/A in data.meta
        data.meta.forEach(m=>{
            if(m[contKey]==null) m[contKey]=0;
        })
        createContinuousColorScheme(data);
    }  
}

function hexbin(points) {
    var binsById = {}, bins = [],  n = points.length, xUnit = (share.xMax-share.xMin)/share.width, yUnit = (share.yMax-share.yMin)/share.height;
    var dx = (Math.sqrt(3)*share.radius)*xUnit, dy = (share.radius*3/2)*yUnit;
    for (var i = 0; i < n; ++i) {
        var point = points[i],
            px = point[0],
            py = point[1],
            pj = Math.round(py = py / dy),
            pi = Math.round(px = px / dx - (pj & 1) / 2),
            py1 = py - pj;

        if (Math.abs(py1) * 3 > 1) {
        var px1 = px - pi,
            pi2 = pi + (px < pi ? -1 : 1) / 2,
            pj2 = pj + (py < pj ? -1 : 1),
            px2 = px - pi2,
            py2 = py - pj2;
        if (px1 * px1 + py1 * py1 > px2 * px2 + py2 * py2) pi = pi2 + (pj & 1 ? 1 : -1) / 2, pj = pj2;
        }

        var id = pi + "-" + pj, bin = binsById[id];
        if (bin) {
            bin.push(point);
            bin.colors.push(point[2]); //color for each point
        }
        else {
        bins.push(bin = binsById[id] = [point]);
        bin.x = (pi + (pj & 1) / 2) * dx;
        bin.y = pj * dy;
        bin.colors = [point[2]]; //initial color set
        }
    }
    //density
    if(share.getDensity){
        let lenSet = [], contKey = "density"//methodKey.slice(2,-1);
        if(data.meta == null) data.meta = [];
        bins.forEach((b,i)=> {
           lenSet.push(b.length);
           if(data.meta[i]==null) data.meta[i]={};
           data.meta.push({density: b.length});
        });
        if(data.density == null) data.density = {};
        data.density[methodKey] = lenSet;
        //lenSet = Array.from(new Set(lenSet));
        if(share.colorSchemes['meta'][contKey]==null) {
            share.continuousMeta.push(contKey);
            share.colorSchemes['meta'][contKey] = {};
        }
        share.color = share.colorSchemes['meta'][contKey];
    }
    else if(share.continuousValue){
        let maxVal = 0
        if(share.getMean){//mean
            bins.forEach(b=>{
                var sum = 0;
                for(i=0;i<b.length;i = i+1) sum = sum +  parseFloat(b[i][4]);
                b.value = (sum/b.length).toFixed(4);
            });
            bins.forEach(b=> {
                if(b.value>maxVal) maxVal=b.value;
             });
            var binCol = new ColorSchemeNumeric(["#367FBA", "white", "#A4444C", "black"], { type: "linear", domain: [0, 2, 6, 10], numLegendPoints: 10 });
            if(share.classKey.startsWith("n_")){
                let lastColorPosition: number;
                let colorPosition: number;
                let str = share.classKey.slice(2,);
                do {
                    colorPosition = str2int(str);
                    str += "h";
                } while (lastColorPosition && Math.abs(lastColorPosition - colorPosition) < 0.3);
                lastColorPosition = colorPosition; // Update lastColorPosition
                const endColor = interpolateTurbo(colorPosition);
                // const endColor = generateColors(1, mk)[0];
                const startColor = interpolateRgb("white", endColor)(0.2);
                binCol = ColorSchemeNumeric.create([startColor, endColor], { type: "linear", domain: [0, maxVal] });
            }
            //new ColorSchemeNumeric(["yellow", "white",  "#98fb98"], { type: "linear", domain: [0, maxVal/4, maxVal/2, maxVal], numLegendPoints: 10});
            share.color = binCol;
            bins.forEach(b=> {
                b.color = binCol.get(b.value);
            });
        }
        else{//median
            bins.forEach(b=>{
                var values = [];
                for(i=0;i<b.length;i = i+1) values.push(parseFloat(b[i][4]));
                values.sort(sortNum);
                b.value = values[Math.floor(b.length/2)].toFixed(4);
            });
            bins.forEach(b=> {
                if(b.value>maxVal) maxVal=b.value;
             });
            var binCol = new ColorSchemeNumeric(["#367FBA", "white", "#A4444C", "black"], { type: "linear", domain: [0, 2, 6, 10], numLegendPoints: 10 });
            if(share.classKey.startsWith("n_")){
                let lastColorPosition: number;
                let colorPosition: number;
                let str = share.classKey.slice(2,);
                do {
                    colorPosition = str2int(str);
                    str += "h";
                } while (lastColorPosition && Math.abs(lastColorPosition - colorPosition) < 0.3);
                lastColorPosition = colorPosition; // Update lastColorPosition
                const endColor = interpolateTurbo(colorPosition);
                // const endColor = generateColors(1, mk)[0];
                const startColor = interpolateRgb("white", endColor)(0.2);
                binCol = ColorSchemeNumeric.create([startColor, endColor], { type: "linear", domain: [0, maxVal] });
            }
            share.color = binCol;
            bins.forEach(b=> {
                b.color = binCol.get(b.value);
            });
        }
    }
    else{//mode
        bins.forEach(b=>{
            var counter = {}, types = [], max = 0;
            for(i=0;i<b.length;i = i+1) counter[b[i][4]] = 0;//initialize the counter
            for(i=0;i<b.length;i = i+1) {
                types.push(b[i][4]);
                counter[b[i][4]] = counter[b[i][4]] + 1;
            }
            //types = [...new Set(types)];
            types.forEach(t=>{
                if(counter[t]>max) {
                    max = counter[t];
                    b.value = t;
                    b.color = share.color[t];
                }
            });
        });
    }
    return bins;
}

function setRange(){
    var range = [], xOffset, yOffset;
    if(share.hexMode){
        xOffset = share.hexaWidth.toFixed(2);
        yOffset = share.radius.toFixed(2);
    }
    else xOffset = yOffset = 1.5
    range = [share.xMin-xOffset,share.xMax+xOffset,share.yMin-yOffset,share.yMax+yOffset];
    return range;
}

function rgbToHex(a, b, c) {
    var r = /^\d{1,3}$/;
    if (!r.test(a) || !r.test(b) || !r.test(c)) 
        return window.alert("wrong input rgb");
    var hexs = [a.toString(16), b.toString(16), c.toString(16)];
    for (var i = 0; i < 3; i++) if (hexs[i].length == 1) hexs[i] = "0" + hexs[i];
    return "#" + hexs.join("");
}
