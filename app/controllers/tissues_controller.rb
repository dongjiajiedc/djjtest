class TissuesController < ApplicationController
    def index
    end

    def show
        @vis = ['id', 'section_name', 'sample_name', 'project_name', 'c_tissue_source',"tissue_name", 'c_tissue_type', "c_storage_type","c_metastasis"]

        @user = User.find(session[:user_id])
        @tissue = Tissue.find(params[:id])
        @attrs = Tissue.column_names
        @section_attrs = Section.column_names
        # @projects = @cancer.projects
        @invis = []
        @section_attrs.each_with_index do |attr, index|
            if !@vis.include?(attr)
                @invis.push(index+1)
            end
        end
        gon.push invis: @invis
        
        id = session[:user_id]
        @user = User.find(id) 
        @datasets = @user.datasets
        respond_to do |format|
            format.html
            # format.csv { send_data @cancer.projects.to_csv }
            format.json { render json: SectionDatatable.new(view_context, 'tissue', @tissue.tissue_name) }
        end
    end
end
