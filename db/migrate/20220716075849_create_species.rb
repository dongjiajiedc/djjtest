class CreateSpecies < ActiveRecord::Migration[6.0]
  def change
    create_table :species do |t|
      t.string :species_name
      t.string :data_source
      t.string :description
      
      t.timestamps
    end
  end
end
